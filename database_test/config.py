DB_NAME = 'vuiis_cest_2017.db'
TABLES = {
'Person': ('id', 'timeCreated', 'timeModified', 'name', 'email', 'affiliation',
    'pictureURL', 'bio'),
'SectionType': ('id', 'timeCreated', 'timeModified', 'title'),
'Section': ('id', 'timeCreated', 'timeModified', 'title', 'startTime',
    'endTime', 'sectionTypeID'),
'Moderator': ('id', 'timeCreated', 'timeModified', 'name', 'email',
    'affiliation', 'sectionID'),
'Presentation': ('id', 'timeCreated', 'timeModified', 'title', 'abstractURL',
    'startTime', 'lengthInMinutes', 'summary', 'stars', 'personID',
    'sectionID'),
'News': ('id', 'timeCreated', 'timeModified', 'title', 'startTime', 'endTime',
    'content'),
}

