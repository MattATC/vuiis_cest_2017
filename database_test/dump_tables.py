#!/usr/bin/env python
import argparse
import config
import pprint
import sqlite3

parser = argparse.ArgumentParser()
parser.add_argument('db_name', nargs='?', default='')
options = parser.parse_args()
if options.db_name:
    db_name = options.db_name
else:
    db_name = config.DB_NAME

with sqlite3.connect(db_name) as conn:
    for table, columns in config.TABLES.items():
        c = conn.cursor()
        c = conn.execute("select * from " + table)
        print(table)
        for row in c.fetchall():
            print(dict(zip(columns, row)))

