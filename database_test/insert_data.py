#!/usr/bin/env python
import argparse
import config
import datetime
import os
import sqlite3


parser = argparse.ArgumentParser()
parser.add_argument('db_name', nargs='?', default='')
options = parser.parse_args()
if options.db_name:
    db_name = options.db_name
else:
    db_name = config.DB_NAME

os.chdir(os.path.dirname(os.path.abspath(__file__)))

INSERT_PERSON_TABLE = """
insert into Person 
(id, timeCreated, timeModified, name, email, affiliation, pictureURL, bio)
values
(1, '{0}', '{1}', 'Person A', 'person.A@gmail.com', 'VU', 'PersonA',
'Person A is a person. He is named after the first letter in the alphabet'),
(2, '{0}', '{1}', 'Person B', 'person.B@gmail.com', 'VU', 'PersonB',
'Person B is a person. He is named after the second letter in the alphabet')
"""

INSERT_MODERATOR_TABLE = """
insert into Moderator
(id, timeCreated, timeModified, name, email, affiliation, sectionID)
values
(1, '{0}', '{1}', 'Moderator A', 'moderatorA@gmail.com', 'VU', {2}),
(2, '{0}', '{1}', 'Moderator B', 'moderatorB@gmail.com', 'VU', {3}),
(3, '{0}', '{1}', 'Moderator C', 'moderatorC@gmail.com', 'VU', {4}),
(4, '{0}', '{1}', 'Moderator D', 'moderatorD@gmail.com', 'VU', {5}),
(5, '{0}', '{1}', 'Moderator E', 'moderatorE@gmail.com', 'VU', {6})
"""


INSERT_SECTION_TYPE_TABLE = """
insert into SectionType
(id, timeCreated, timeModified, title)
values
(1, '{0}', '{1}', 'Oral'),
(2, '{0}', '{1}', 'Poster'),
(3, '{0}', '{1}', 'PowerPitch'),
(4, '{0}', '{1}', 'Break'),
(5, '{0}', '{1}', 'Event')
"""

INSERT_SECTION_TABLE = """
insert into Section
(id, timeCreated, timeModified, startTime, endTime, title, sectionTypeID)
values
(1, '{0}', '{1}', '2017-05-17 08:30:00', '2017-05-17 09:30:00', 'Oral Section', 1),
(2, '{0}', '{1}', '2017-05-17 09:30:00', '2017-05-17 10:30:00', 'Poster Section', 2),
(3, '{0}', '{1}', '2017-05-17 10:30:00', '2017-05-17 11:30:00', 'Power Pitch Section', 3),
(4, '{0}', '{1}', '2017-05-17 11:30:00', '2017-05-17 12:30:00', 'Break', 4),
(5, '{0}', '{1}', '2017-05-17 12:30:00', '2017-05-17 1:30:00', 'Event', 5)
"""

INSERT_PRESENTATION_TABLE = """
insert into Presentation
(id, timeCreated, timeModified, title, abstractURL, startTime, lengthInMinutes,
summary, stars, personID, sectionID)
values
(1, '{0}', '{1}', 'Oral Presentation', 'abstractURL A', '2017-05-17 08:30:00', 60,
    'This is an oral presentation.', 4, 1, 1),
(2, '{0}', '{1}', 'Poster Presentation', 'abstractURL B', '2017-05-17 09:30:00', 60,
    'This is a poster presentation.', 4, 2, 2),
(3, '{0}', '{1}', 'Power Pitch Presentation', 'abstractURL C', '2017-05-17 10:30:00', 60,
    'This is a power pitch presentation.', 4, 2, 3),
(4, '{0}', '{1}', 'Event', 'abstractURL D', '2017-05-17 11:30:00', 60,
    'This is an event.', 4, 2, 5)
"""

INSERT_NEWS_TABLE = """
insert into News
(id, timeCreated, timeModified, title, startTime, endTime, content, isRead)
values
(1, '{0}', '{1}', 'News 1', '{2}', '{3}', 'This is news article content 1', 0),
(2, '{0}', '{1}', 'News 2', '{4}', '{5}', 'This is news article content 2', 0)
"""

t = datetime.datetime.now()# + datetime.timedelta(days=7)
ct = t.strftime('%Y-%m-%d %H:%M:%S')
next_t = t + datetime.timedelta(days=1)
next_ct = next_t.strftime('%Y-%m-%d %H:%M:%S')
next_t2 = t + datetime.timedelta(days=2)
next_ct2 = next_t2.strftime('%Y-%m-%d %H:%M:%S')
with sqlite3.connect(db_name) as conn:
    conn.execute(INSERT_PERSON_TABLE.format(ct, ct))
    conn.execute(INSERT_MODERATOR_TABLE.format(ct, ct, 1, 2, 3, 4, 5))
    conn.execute(INSERT_SECTION_TYPE_TABLE.format(ct, ct))
    conn.execute(INSERT_SECTION_TABLE.format(ct, ct))
    conn.execute(INSERT_PRESENTATION_TABLE.format(ct, ct))
    conn.execute(INSERT_NEWS_TABLE.format(ct, ct, ct, next_ct, next_ct,
        next_ct2))
    conn.commit()

