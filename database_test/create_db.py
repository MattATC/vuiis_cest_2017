#!/usr/bin/env python
import argparse
import config
import os
import sqlite3


parser = argparse.ArgumentParser()
parser.add_argument('db_name', nargs='?', default='')
options = parser.parse_args()
if options.db_name:
    db_name = options.db_name
else:
    db_name = config.DB_NAME

os.chdir(os.path.dirname(os.path.abspath(__file__)))

CREATE_PERSON_TABLE = """
create table Person (id integer primary key autoincrement,
    timeCreated string not null, timeModified string not null,
    name string not null, email string null, affiliation string null,
    pictureURL string null, bio string null)
"""

CREATE_SECTION_TYPE_TABLE = """
create table SectionType (id integer primary key autoincrement,
    timeCreated string not null, timeModified string not null,
    title string not null)
"""

CREATE_SECTION_TABLE = """
create table Section (id integer primary key autoincrement,
    timeCreated string not null, timeModified string not null,
    title string null, startTime string not null, endTime string null,
    sectionTypeID int not null,
    foreign key(sectionTypeID) references SectionType(id) on delete set null)
"""

CREATE_MODERATOR_TABLE = """
create table Moderator (id integer primary key autoincrement,
    timeCreated string not null, timeModified string not null,
    name string null, email string null, affiliation string null,
    sectionID int not null,
    foreign key(sectionID) references Section(id) on delete set null)
"""

CREATE_PRESENTATION_TABLE = """
create table Presentation (id integer primary key autoincrement,
    timeCreated string not null, timeModified string not null,
    title string not null, abstractURL string null,
    startTime string not null, lengthInMinutes int null,
    summary string null, stars int null, personID int null,
    sectionID int not null,
    foreign key(personID) references Person(id) on delete set null,
    foreign key(sectionID) references Section(id) on delete set null)
"""

CREATE_PRESENTATION_RATING_TABLE = """
create table PresentationRating (id integer not null,
    timeModified string not null, stars int not null,
    unique(id),
    foreign key(id) references Presentation(id) on delete cascade)
"""

CREATE_NEWS_TABLE = """
create table News (id integer primary key autoincrement,
    timeCreated string not null, timeModified string not null,
    title string not null, startTime string not null, endTime string null,
    content string not null, isRead int not null default 0)
"""

try:
    os.remove(db_name)
except OSError as e:
    pass

with sqlite3.connect(db_name) as conn:
    conn.execute(CREATE_PERSON_TABLE)
    conn.execute(CREATE_SECTION_TYPE_TABLE)
    conn.execute(CREATE_SECTION_TABLE)
    conn.execute(CREATE_MODERATOR_TABLE)
    conn.execute(CREATE_PRESENTATION_TABLE)
    conn.execute(CREATE_PRESENTATION_RATING_TABLE)
    conn.execute(CREATE_NEWS_TABLE)
    conn.commit()

