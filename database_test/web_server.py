#!/usr/bin/env python
from __future__ import division
import config
import datetime
import json
import logging
import logging.handlers
import os
import sqlite3
import sys
import time
import traceback

from flask import Flask, make_response, render_template, request, session


app = Flask('vuiis_cest_web_server')
app.config['SECRET_KEY'] = open('websession.key', 'rb').read()
app.logger.setLevel(logging.DEBUG)
_formatter = logging.Formatter('%(levelname)s %(asctime)s: %(message)s')
if os.isatty(sys.stderr.fileno()):  # Log to terminal, if present
    _stream_handler = logging.StreamHandler()
    _stream_handler.setFormatter(_formatter)
    app.logger.addHandler(_stream_handler)


DB_NAME = 'vuiis_cest_2017_test.db'

#HTTP status codes
STAT_OK = 200
STAT_BAD_REQUEST = 400
STAT_UNAUTHORIZED = 401
STAT_NOT_FOUND = 404

#Content-Type values
TEXT_HTML_CONTENT = 'text/html; charset=utf-8'
JSON_CONTENT = 'application/json; charset=utf-8'

GET_NEW_TABLES_QUERY = """
select * from {0}
where strftime('%Y-%m-%d %H:%M:%S', timeModified) >
strftime('%Y-%m-%d %H:%M:%S', '{1}')
"""

GET_ENTITIES_QUERY = """
select id from {0}
order by id
"""

GET_NEW_ENTITIES_QUERY = """
select id from {0}
where strftime('%Y-%m-%d %H:%M:%S', timeModified) >
strftime('%Y-%m-%d %H:%M:%S', '{1}')
limit 1
"""

SUBMIT_RATING_QUERY = """
insert into Ratings(key, id, stars)
values (?, ?, ?)
"""

@app.route('/')
def index():
    headers = {'Content-Type': TEXT_HTML_CONTENT}
    response_body = 'OK\n'
    status = STAT_OK
    return make_response((response_body, status, headers))

@app.route('/update')
def get_updated():
    """ Gets the tables that are new than the specified date """
    app.logger.debug('headers: {0}'.format(request.headers))
    request_pairs = request.args.get('requestPairs')
    response_body = ''
    if request_pairs:
        try:
            app.logger.debug('request_pairs: {0}'.format(request_pairs))
            request_list = request_pairs.split(',')
            app.logger.debug('request_list: {0}'.format(request_list))
            req = {}
            while request_list:
                table = request_list.pop(0)
                timestamp = request_list.pop(0)
                if timestamp:
                    since = time.strptime(timestamp, '%Y-%m-%d %H:%M:%S')
                else:
                    timestamp = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                req[table] = timestamp
            updated = {}
            with sqlite3.connect(DB_NAME) as conn:
                c = conn.cursor()
                for table, timestamp in req.items():
                    query = GET_NEW_TABLES_QUERY.format(table, timestamp)
                    c.execute(query)
                    rows = []
                    columns = config.TABLES[table]
                    for row in c.fetchall():
                        rows.append(dict(zip(columns, row)))
                    if rows:
                        updated[table] = rows
            response_body = json.dumps(updated)
            status = STAT_OK
        except:
            response_body = ''
            app.logger.exception('')
            status = STAT_BAD_REQUEST
    else:
        response_body = ''
        status = STAT_BAD_REQUEST
    headers = {'Content-Type': JSON_CONTENT}
    return make_response((response_body, status, headers))

@app.route('/ids')
def get_ids():
    """ Gets the entities IDs in the given tables """
    entities = request.args.get('entities')
    if entities:
        try:
            request_list = entities.split(',')
            entities = {}
            with sqlite3.connect(DB_NAME) as conn:
                c = conn.cursor()
                for table in request_list:
                    query = GET_ENTITIES_QUERY.format(table)
                    c.execute(query)
                    rows = []
                    for row in c.fetchall():
                        rows.append(row[0])
                    entities[table] = rows
            response_body = json.dumps(entities)
            status = STAT_OK
        except:
            response_body = ''
            app.logger.exception('')
            status = STAT_BAD_REQUEST
    else:
        response_body = ''
        status = STAT_BAD_REQUEST
    headers = {'Content-Type': JSON_CONTENT}
    return make_response((response_body, status, headers))

@app.route('/update2')
def get_needs_update():
    """ Gets if new update is available after the given date """
    date = request.args.get('date')
    response_body = '0'
    status = STAT_BAD_REQUEST
    if date:
        try:
            result = 0
            with sqlite3.connect(DB_NAME) as conn:
                c = conn.cursor()
                for table in config.TABLES:
                    query = GET_NEW_TABLES_QUERY.format(table, date)
                    c.execute(query)
                    if c.fetchall():
                        result = 1
                        break
            response_body = str(result)
            status = STAT_OK
        except:
            status = STAT_BAD_REQUEST
    headers = {'Content-Type': TEXT_HTML_CONTENT}
    return make_response((response_body, status, headers))

@app.route('/rating', methods=['POST'])
def submit_rating():
    """ Submit a presentation rating """
    try:
        body = request.get_json()
        app.logger.debug('Got body: {0}'.format(body))
        response_body = json.dumps({'status': 1})
        status = STAT_OK
    except:
        response_body = json.dumps({'status': 0})
        status = STAT_BAD_REQUEST
    headers = {'Content-Type': JSON_CONTENT}
    return make_response((response_body, status, headers)) 

try:
    app.logger.info('vuiis_cest_web_server start')
    app.run(host='0.0.0.0', port=8888, debug=True)
except:
    msg = ''.join(traceback.format_exception(*sys.exc_info()))
    app.logger.error('Exception occurred:\n{0}'.format(msg))
    sys.exit(1)

