#!/usr/bin/env python
import config
import pprint
import sqlite3


"""
create table Presentation (id integer primary key autoincrement,
    title string not null, abstractURL string not null,
    startTime string not null, lengthInMinutes int null,
    summary string null, stars int null, personID int not null,
    sectionID int not null,
"""

SELECT_PRESENTATION_TABLE = """
select p.id, p.title, p.abstractURL, p.startTime, p.lengthInMinutes,
p.summary, p.stars, p.personID, p.sectionID
from Presentation p
order by p.id
"""

with sqlite3.connect(config.DB_NAME) as conn:
    c = conn.cursor()
    c.execute(SELECT_PRESENTATION_TABLE)
    columns = config.TABLES['Presentation']
    for row in c.fetchall():
        print(dict(zip(columns, row)))

