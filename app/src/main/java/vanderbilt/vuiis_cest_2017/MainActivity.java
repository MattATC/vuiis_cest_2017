package vanderbilt.vuiis_cest_2017;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.leakcanary.RefWatcher;

import java.util.HashMap;
import java.util.Map;

import vanderbilt.vuiis_cest_2017.Data.Abstract.AbstractManager;
import vanderbilt.vuiis_cest_2017.Data.Comment.UserCommentManager;
import vanderbilt.vuiis_cest_2017.Data.DatabaseMetaData;
import vanderbilt.vuiis_cest_2017.Data.News.NewsManager;
import vanderbilt.vuiis_cest_2017.Data.Portrait.PortraitManager;
import vanderbilt.vuiis_cest_2017.Data.Rating.PresentationRatingManager;
import vanderbilt.vuiis_cest_2017.Data.Rating.RatingBarTracker;
import vanderbilt.vuiis_cest_2017.DataManager.AppDataManager;
import vanderbilt.vuiis_cest_2017.DataManager.DataUpdateInfo;
import vanderbilt.vuiis_cest_2017.DataManager.DataUpdateListener;
import vanderbilt.vuiis_cest_2017.ViewController.ViewControllerFragment;
import vanderbilt.vuiis_cest_2017.ViewController.ViewControllerHandle;

public class MainActivity extends AppCompatActivity
    implements AppActivityListener, DataUpdateListener {

    private AppDataManager mDataManager;

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private FeatureViewPager mViewPager;
    private Map<Integer, ViewControllerFragment> mPageReferenceMap = new HashMap<>();
    private boolean mUpNavigationEnabled = false;
    private boolean mNewsEnabled = true;
    private TabLayout mFeatureTabLayout = null;
    private MenuItem mNewsItem = null;
    private Menu mMenu = null;
    private boolean mIsActive = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        RefWatcher refWatcher = ThisApplication.getRefWatcher();
        refWatcher.watch(this);

        //resetData();
        ThisApplication.initializeInstallationID(this);
        //ThisApplication.setServerAPIKey("");
        String serverAPIkey = ThisApplication.getServerAPIKey();
        if(serverAPIkey.isEmpty()) {
            OnBoardingActivity.startActivityForOnboarding(this);
        }
        mDataManager = new AppDataManager(this);

        PresentationRatingManager.setInstance(new PresentationRatingManager());
        RatingBarTracker.setInstance(new RatingBarTracker());
        UserCommentManager.setInstance(new UserCommentManager());
        NewsManager.setInstance(new NewsManager());
        AbstractManager.setInstance(new AbstractManager(this));
        AbstractManager.deleteAbstracts(this);
        mDataManager.subscribeToDataUpdate(DatabaseMetaData.TABLE_NEWS, this);
        PortraitManager.setInstance(new PortraitManager(this));
        PortraitManager.deletePortraits(this);

        setContentView(R.layout.activity_main);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (FeatureViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset,
                                       int positionOffsetPixels) {
            }
            @Override
            public void onPageSelected(int position) {
                ViewControllerFragment fragment = mPageReferenceMap.get(position);
                if(fragment != null) {
                    ViewControllerHandle viewController = fragment.getViewController();
                    if (viewController != null) {
                        viewController.restoreAppActivity();
                    }
                }
            }
            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        mViewPager.setCurrentItem(0);

        mFeatureTabLayout = (TabLayout) findViewById(R.id.tabs);
        mFeatureTabLayout.setupWithViewPager(mViewPager);
        //Set tab icons
        mFeatureTabLayout.getTabAt(0).setIcon(R.drawable.ic_presentation);
        mFeatureTabLayout.getTabAt(1).setIcon(R.drawable.ic_posters);
        mFeatureTabLayout.getTabAt(2).setIcon(R.drawable.ic_speakers);

        mDataManager.updateDataSources(null);
    }

    @Override
    public void onResume() {
        super.onResume();
        mIsActive = true;
        updateMenuOptions();
        if(ThisApplication.isServerAPIKeySet()) {
            mDataManager.checkDataUpdate();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mIsActive = false;
    }

    public boolean isActive() {
        return mIsActive;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        mMenu = menu;
        getMenuInflater().inflate(R.menu.menu_main, menu);
        mNewsItem = mMenu.findItem(R.id.action_view_news);
        updateMenuOptions();
        return super.onCreateOptionsMenu(menu);
    }

    private void updateMenuOptions() {
        if(mMenu != null) {
            MenuItem itemOnboarding = mMenu.findItem(R.id.action_onboarding);
            if (itemOnboarding != null) {
                itemOnboarding.setVisible(!ThisApplication.isServerAPIKeySet());
            }
            updateNewsButton();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if(id == R.id.action_onboarding) {
            OnBoardingActivity.startActivityForOnboarding(this);
            return true;
        }
        if(id == R.id.action_view_news) {
            String serverAPIkey = ThisApplication.getServerAPIKey();
            if(serverAPIkey.isEmpty()) {
                OnBoardingActivity.startActivityForOnboarding(this);
            }
            else {
                Intent intent = new Intent(this, NewsActivity.class);
                startActivityForResult(intent, NewsActivity.SHOW_NEWS);
            }
            return true;
        }
        if(id == R.id.action_show_current) {
            ViewControllerFragment fragment = mPageReferenceMap.get(mViewPager.getCurrentItem());
            if(fragment != null) {
                fragment.doCustomAction(AgendaFragment.CustomAction.SHOW_CURRENT_ITEM.ordinal(),
                        null);
                return true;
            }
        }
        if(id == android.R.id.home) {
            return navigateUp();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if(!navigateUp()) {
            moveTaskToBack(true);
        }
    }

    public boolean navigateUp() {
        if(mUpNavigationEnabled) {
            ViewControllerFragment fragment = mPageReferenceMap.get(mViewPager.getCurrentItem());
            if(fragment == null) {
                Log.d("navigateUp", "wtf!");
            }
            ViewControllerHandle viewController = fragment.getViewController();
            if(viewController != null) {
                viewController.onBackPressed();
                return true;
            }
        }
        return false;
    }

    public void enableUpNavigation() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        mUpNavigationEnabled = true;
    }

    public void disableUpNavigation() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setHomeButtonEnabled(false);
        mUpNavigationEnabled = false;
    }

    public void enableNewsButton() {
        if(mNewsItem != null) {
            mNewsItem.setVisible(true);
        }
        mNewsEnabled = true;
    }

    public void disableNewsButton() {
        if(mNewsItem != null) {
            mNewsItem.setVisible(false);
        }
        mNewsEnabled = false;
    }

    @Override
    public void enableFeatureTabs() {
        mFeatureTabLayout.setVisibility(View.VISIBLE);
        mViewPager.enableSwipe();
    }

    @Override
    public void disableFeatureTabs() {
        mFeatureTabLayout.setVisibility(View.GONE);
        mViewPager.disableSwipe();
    }

    public AppDataManager getAppDataManager() {
        return mDataManager;
    }

    @Override
    public void onDataUpdate(DataUpdateInfo dataUpdateInfo) {
        updateNewsButton();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        updateNewsButton();
    }

    private void updateNewsButton() {
        if (mNewsItem != null) {
            if (NewsManager.hasUnReadNews()) {
                mNewsItem.setIcon(R.drawable.ic_markunread_mailbox_black_24dp);
            }
            else {
                mNewsItem.setIcon(R.drawable.ic_mail_black_24dp);
            }
        }
    }

    private void resetData() {
        deleteDatabase(DatabaseMetaData.DATABASE_NAME);
        ThisApplication.deleteInstallationID(this);
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    private class SectionsPagerAdapter extends FragmentPagerAdapter {

        private SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            ViewControllerFragment viewControllerFragment = null;
            switch(position) {
                case 0:
                    viewControllerFragment = AgendaFragment.newInstance(position + 1);
                    break;
                case 1:
                    viewControllerFragment = PostersFragment.newInstance(position + 1);
                    break;
                case 2:
                    viewControllerFragment = SpeakersFragment.newInstance(position + 1);
                    break;
            }
            return viewControllerFragment;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            ViewControllerFragment fragment = (ViewControllerFragment)
                    super.instantiateItem(container, position);
            mPageReferenceMap.put(position, fragment);
            return fragment;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            mPageReferenceMap.remove(position);
            super.destroyItem(container, position, object);
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return AgendaFragment.TAB_TITLE;
                case 1:
                    return PostersFragment.TAB_TITLE;
                case 2:
                    return SpeakersFragment.TAB_TITLE;
            }
            return null;
        }
    }
}

