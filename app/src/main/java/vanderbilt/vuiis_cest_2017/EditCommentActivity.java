package vanderbilt.vuiis_cest_2017;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import vanderbilt.vuiis_cest_2017.Data.Comment.UserComment;
import vanderbilt.vuiis_cest_2017.Data.Comment.UserCommentManager;
import vanderbilt.vuiis_cest_2017.Data.DatabaseHelper;
import vanderbilt.vuiis_cest_2017.Utilities.GUI;

/**
 * Created by Matthew Christian on 4/3/17.
 */

public class EditCommentActivity extends AppCompatActivity {
    final public static int NEW_COMMENT = 0;

    final private static int CHARACTER_LIMIT = 500;
    final private static int CHARACTER_WARNING = 30;

    private boolean mIsChanged = false;
    private int mPresentationID;

    public static void startActivityToSubmitPresentationComment(Activity activity,
                                                                int presentationID, String topic) {
        Intent intent = new Intent(activity, EditCommentActivity.class);
        Bundle bundle = new Bundle();
        int requestCode = NEW_COMMENT;
        bundle.putString("topic", topic);
        bundle.putInt("presentationID", presentationID);
        intent.putExtras(bundle);
        activity.startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);

        //Get the passed presentation ID
        Bundle bundle = getIntent().getExtras();
        String topic = "";
        if(bundle != null) {
            topic = bundle.getString("topic", "");
            mPresentationID = bundle.getInt("presentationID", 0);
        }

        //Enable the up button and set the appropriate title
        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setTitle("");
        }

        //Populate the views in the layout
        final TextView tvTopic = (TextView)findViewById(R.id.label_comment_topic);
        final EditText etContent = (EditText) findViewById(R.id.et_comment);
        final TextView tvCharLimit = (TextView) findViewById(R.id.label_comment_char_limit);
        tvTopic.setText(topic);
        etContent.setText("");

        //Set the on focus change listeners so we can dismiss the soft keyboard
        View.OnFocusChangeListener onFocusChangeListener = new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus) {
                    GUI.lowerSoftKeyboard(v);
                }
            }
        };
        tvTopic.setOnFocusChangeListener(onFocusChangeListener);
        etContent.setOnFocusChangeListener(onFocusChangeListener);

        //Set text change listeners so we can prompt to save changes when the activity is exited
        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mIsChanged = true;
                int charactersLeft = CHARACTER_LIMIT - charSequence.length();
                if(charactersLeft <= CHARACTER_WARNING) {
                    tvCharLimit.setText(getString(R.string.status_characters_left, charactersLeft));
                    tvCharLimit.setVisibility(View.VISIBLE);
                }
                else {
                    tvCharLimit.setVisibility(View.GONE);
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {
            }
        };
        etContent.addTextChangedListener(textWatcher);
        etContent.setFilters(new InputFilter[] { new InputFilter.LengthFilter(CHARACTER_LIMIT) });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_edit_comment, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                checkChangesAndClose();
                return true;
            case R.id.action_submit_comment:
                saveAndClose();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        checkChangesAndClose();
    }

    //This method checks if the user made any changes to the comment and if so, prompt them to
    //continue editing. If not, cancel the activity
    private void checkChangesAndClose() {
        if(mIsChanged) {
            new AlertDialog.Builder(this)
                    .setTitle(getString(R.string.title_comment))
                    .setMessage(getString(R.string.dialog_ask_quit_comment))
                    .setCancelable(true)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            cancelActivity();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    }).show();
        }
        else {
            cancelActivity();
        }
    }

    private void cancelActivity() {
        Intent intent = getIntent();
        setResult(Activity.RESULT_CANCELED, intent);
        finish();
    }

    //This method validates the input, submits the comment, and closes the activity
    private void saveAndClose() {
        final EditText etContent = (EditText) findViewById(R.id.et_comment);
        String content = etContent.getText().toString();
        if(content.equals("")) {
            Toast.makeText(this, "Comment cannot be blank", Toast.LENGTH_SHORT).show();
        }
        else {
            DatabaseHelper dataHelper = new DatabaseHelper(this);
            UserComment comment = new UserComment(mPresentationID, content);
            UserCommentManager.submitComment(dataHelper, comment);
            Toast.makeText(this, "Hooray!  Your comment was submitted!", Toast.LENGTH_SHORT).show();
            setResult(Activity.RESULT_OK, getIntent());
            finish();
        }
    }
}
