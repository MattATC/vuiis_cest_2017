package vanderbilt.vuiis_cest_2017.DataManager;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import vanderbilt.vuiis_cest_2017.R;

/**
 * Created by Matthew Christian on 3/16/17.
 */

public class RetryUpdateFragment extends DialogFragment {
    private RetryUpdateFragmentListener mListener;

    public interface RetryUpdateFragmentListener {
        void onRetryUpdateRequest();
    }

    public void setListener(RetryUpdateFragmentListener listener) {
        mListener = listener;
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle args = getArguments();
        String errorMessage = args.getString("error_message", "") + " ";
        return new AlertDialog.Builder(getActivity())
                .setTitle("Update Failed")
                .setMessage(errorMessage + getString(R.string.dialog_retry_update_again))
                .setCancelable(false)
                .setPositiveButton("Try Again", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(mListener != null) {
                            mListener.onRetryUpdateRequest();
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Do Nothing
                    }
                }).show();
    }
}