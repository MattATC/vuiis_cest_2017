package vanderbilt.vuiis_cest_2017.DataManager;

import vanderbilt.vuiis_cest_2017.Utilities.NetworkTaskException;

/**
 * Created by Matthew Christian on 4/5/17.
 */

public interface RetrieveAPIKeyTaskDelegate {
    void onAPIKeyRetrieval(String apiKey);
    void onAPIKeyRetrievalFailed(NetworkTaskException networkTaskException);
    void onAPIKeyRetrievalCancelled();
}
