package vanderbilt.vuiis_cest_2017.DataManager;

/**
 * Created by Matthew Christian on 3/16/17.
 */

public interface DataUpdateListener {
    void onDataUpdate(DataUpdateInfo dataUpdateInfo);
}
