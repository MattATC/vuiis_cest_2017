package vanderbilt.vuiis_cest_2017.DataManager;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import vanderbilt.vuiis_cest_2017.Data.DatabaseHelper;
import vanderbilt.vuiis_cest_2017.Data.DatabaseMetaData;
import vanderbilt.vuiis_cest_2017.ThisApplication;
import vanderbilt.vuiis_cest_2017.Utilities.NetworkTaskCommon;
import vanderbilt.vuiis_cest_2017.Utilities.NetworkTaskException;

/**
 * Created by Matthew Christian on 3/21/17.
 */

public class DataUpdateCheckTask extends AsyncTask<String, Void, DataUpdateEntities> {
    private static final String UPDATE_SCRIPT_PATH = "update";
    private static final String ENTITY_ID_SCRIPT_PATH = "id";

    private DataUpdateCheckTaskDelegate mTaskDelegate;
    private NetworkTaskException mTaskException;

    public DataUpdateCheckTask(DataUpdateCheckTaskDelegate taskDelegate) {
        mTaskDelegate = taskDelegate;
    }

    /**
     * The entry point of the task
     * @param params
     * @return
     */
    @Override
    protected DataUpdateEntities doInBackground(String... params) {
        /* Start pseudocode
        //1. Call getNewAndUpdateEntities with all tables and the last modified date for each
        //2. For each entry in the JSON dict
        // 2a. If the entry's list is not empty, mark the row to be updated
        // 2b. If the entry's list is empty, do nothing
        //3. Call GetIDsInEntities with all tables
        //4. For each entry in the JSON dict
        // 4a. If the ID list is missing an ID from the table, mark the row to be deleted
        // 4b. If the ID list and the table IDs are the same, do nothing
        End pseudocode */
        JSONObject tableUpdateInfo = null;
        JSONObject rowIDsInfo = null;
        DataUpdateEntities dataUpdateEntities;
        try {
            tableUpdateInfo = getNewAndUpdateEntities(); //Steps 1, 2
            if (tableUpdateInfo != null) {
                rowIDsInfo = getEntityIDs(); //Steps 3, 4
            }
        }
        catch(NetworkTaskException e) {
            Log.d("DataUpdateTask", "NetworkTaskException occurred: " + e);
            mTaskException = e;
        }
        catch(Exception e) {
            Log.d("DataUpdateTask", "Exception occurred: " + e);
        }
        dataUpdateEntities = new DataUpdateEntities(tableUpdateInfo, rowIDsInfo);
        return dataUpdateEntities;
    }

    /**
     * Runs on the UI thread after doInBackground completes successfully
     * @param dataUpdateEntities
     */
    @Override
    protected void onPostExecute(DataUpdateEntities dataUpdateEntities) {
        if(mTaskDelegate != null) {
            if(mTaskException == null) {
                mTaskDelegate.onAppUpdateChecked(dataUpdateEntities);
            }
            else {
                mTaskDelegate.onAppUpdateCheckFailed(mTaskException);
            }
        }
    }

    /**
     * Runs on the UI thread after doInBackground does NOT complete successfully
     */
    @Override
    protected void onCancelled() {
        mTaskDelegate.onAppUpdateCheckCancelled();
    }

    public JSONObject getNewAndUpdateEntities()
            throws NetworkTaskException {
        try {
            Map<String, String> header = new HashMap<>();
            header.put("x-api-key", ThisApplication.getServerAPIKey());
            Map<String, String> paramMap = getDatesTablesWereLastModified();
            String urlParams = NetworkTaskCommon.encodeParameterMap(paramMap);
            String response = new String(NetworkTaskCommon.getServerHTTPData(UPDATE_SCRIPT_PATH,
                    urlParams, "", header), "UTF-8");
            JSONObject tableUpdateInfo = new JSONObject(response);
            //Make sure at least one list returned is not empty, else there is nothing to update
            Iterator<String> iterator = tableUpdateInfo.keys();
            while(iterator.hasNext()) {
                String table = iterator.next();
                JSONArray array = tableUpdateInfo.getJSONArray(table);
                if(array.length() > 0) {
                    return tableUpdateInfo;
                }
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        catch(NetworkTaskException e) {
            throw e;
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Call GetIDsInEntities with all tables and the last modified date for each
     * @return
     */
    private JSONObject getEntityIDs()
            throws NetworkTaskException {
        JSONObject rowIDsInfo = null;
        try {
            Map<String, String> header = new HashMap<>();
            header.put("x-api-key", ThisApplication.getServerAPIKey());
            String response = new String(NetworkTaskCommon.getServerHTTPData(ENTITY_ID_SCRIPT_PATH,
                    "", "",  header), "UTF-8");
            rowIDsInfo = new JSONObject(response);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        catch(NetworkTaskException e) {
            throw e;
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return rowIDsInfo;
    }

    private Map<String, String> getDatesTablesWereLastModified() {
        List<Date> lastModifiedList = new ArrayList<>(
                DatabaseMetaData.getDateTablesLastModified().values());
        Map<String, String> paramMap = new HashMap<>();

        lastModifiedList.removeAll(Arrays.asList(new Date[] { null }));
        if(!lastModifiedList.isEmpty()) {
            Collections.sort(lastModifiedList, Collections.<Date>reverseOrder());
            Date latestModified = lastModifiedList.get(0);
            paramMap.put("since", DatabaseHelper.convertDateToTimestamp(latestModified));
        }
        return paramMap;
    }
}
