package vanderbilt.vuiis_cest_2017.DataManager;

import vanderbilt.vuiis_cest_2017.Utilities.NetworkTaskException;

/**
 * Created by Matthew Christian on 3/21/17.
 */
public interface DataUpdateCheckTaskDelegate {
    void onAppUpdateChecked(DataUpdateEntities dataUpdateEntities);
    void onAppUpdateCheckFailed(NetworkTaskException networkTaskException);
    void onAppUpdateCheckCancelled();
}
