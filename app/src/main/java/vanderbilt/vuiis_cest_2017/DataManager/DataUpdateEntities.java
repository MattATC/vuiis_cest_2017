package vanderbilt.vuiis_cest_2017.DataManager;

import org.json.JSONObject;

/**
 * Created by Matthew Christian on 4/4/17.
 */

public class DataUpdateEntities {
    private JSONObject mTableUpdateInfo;
    private JSONObject mEntityIDs;

    public DataUpdateEntities(JSONObject tableUpdateInfo, JSONObject entityIDs) {
        mTableUpdateInfo = tableUpdateInfo;
        mEntityIDs = entityIDs;
    }

    public JSONObject getTableUpdateInfo() {
        return mTableUpdateInfo;
    }

    public JSONObject getEntityIDs() {
        return mEntityIDs;
    }

    public boolean hasEntities() {
        return mTableUpdateInfo != null && mTableUpdateInfo.length() > 0 &&
                mEntityIDs != null && mEntityIDs.length() > 0;
    }
}
