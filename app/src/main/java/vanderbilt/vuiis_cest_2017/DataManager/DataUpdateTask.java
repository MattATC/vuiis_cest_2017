package vanderbilt.vuiis_cest_2017.DataManager;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import vanderbilt.vuiis_cest_2017.Data.DatabaseHelper;

/**
 * Created by Matthew Christian on 3/15/17.
 */

public class DataUpdateTask extends AsyncTask<String, Void, DataUpdateInfo> {
    private DatabaseHelper mDatabaseHelper;
    private DataUpdateEntities mDataUpdateEntities;
    private DataUpdateTaskDelegate mTaskDelegate;

    public DataUpdateTask(DatabaseHelper dataHelper, DataUpdateEntities dataUpdateEntities,
                          DataUpdateTaskDelegate taskDelegate) {
        mDatabaseHelper = dataHelper;
        mDataUpdateEntities = dataUpdateEntities;
        mTaskDelegate = taskDelegate;
    }

    /**
     * The entry point of the task
     * @param params
     * @return
     */
    @Override
    protected DataUpdateInfo doInBackground(String... params) {
        DataUpdateInfo dataUpdateInfo = null;
        try {
            dataUpdateInfo = updateTables(mDataUpdateEntities.getTableUpdateInfo(),
                    mDataUpdateEntities.getEntityIDs()); //Steps 1, 2, 3, 4
        }
        catch(Exception e) {
            Log.d("DataUpdateTask", "Exception occurred: " + e);
        }
        return dataUpdateInfo;
    }

    /**
     * Runs on the UI thread after doInBackground completes successfully
     * @param dataUpdateInfo
     */
    @Override
    protected void onPostExecute(DataUpdateInfo dataUpdateInfo) {
        if(mTaskDelegate != null) {
            mTaskDelegate.onAppUpdated(dataUpdateInfo);
        }
    }

    /**
     * Runs on the UI thread after doInBackground does NOT complete successfully
     */
    @Override
    protected void onCancelled() {
        if(mTaskDelegate != null) {
            mTaskDelegate.onAppUpdateCancelled();
        }
    }

    /**
     * Updates a set of tables in the database
     * @param tableUpdateInfo
     * @param rowIDsInfo
     * @return
     */
    private DataUpdateInfo updateTables(JSONObject tableUpdateInfo, JSONObject rowIDsInfo) {
        /*  Start pseudocode
            1. Begin transaction
            2. For all rows marked for update, update them
            3. For all rows marked for deletion, delete them
            4. Commit transaction
            End pseudocode */
        int recordsUpdated = 0;
        HashSet<String> tablesAffected = new HashSet<>();
        SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();
        db.beginTransaction();
        try {
            Iterator<?> keys = tableUpdateInfo.keys();
            while (keys.hasNext()) {
                String tableName = (String) keys.next();
                JSONArray rowList = tableUpdateInfo.getJSONArray(tableName);
                recordsUpdated += updateTable(db, tableName, rowList);
                if(recordsUpdated > 0) {
                    tablesAffected.add(tableName);
                }
            }
            keys = rowIDsInfo.keys();
            while (keys.hasNext()) {
                String tableName = (String) keys.next();
                JSONArray idsList = rowIDsInfo.getJSONArray(tableName);
                recordsUpdated += deleteMissingIDsFromTable(db, tableName, idsList);
                if(recordsUpdated > 0) {
                    tablesAffected.add(tableName);
                }
            }
            db.setTransactionSuccessful();
        }
        catch (android.database.sqlite.SQLiteConstraintException e) {
            Log.e("updateTables", "Exception occurred while updating tables: " + e);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        finally {
            db.endTransaction();
        }
        db.close();
        return new DataUpdateInfo(recordsUpdated, tablesAffected, null);
    }

    /**
     * Updates a table in the database, returns the number of rows added + updated
     * @param db
     * @param tableName
     * @param rowList
     * @return
     * @throws JSONException
     */
    private int updateTable(SQLiteDatabase db, String tableName, JSONArray rowList)
            throws JSONException
    {
        List<String> columns = new ArrayList<>();
        List<String> values = new ArrayList<>();
        List<String> parameterSubstitutes = new ArrayList<>();
        int rowsN = rowList.length();
        for(int i = 0; i < rowsN; i++) {
            JSONObject row = rowList.getJSONObject(i);
            columns.clear();
            values.clear();
            parameterSubstitutes.clear();
            Iterator<?> keys = row.keys();
            while(keys.hasNext()) {
                String columnName = (String)keys.next();
                String columnValue = row.isNull(columnName) ? null : row.getString(columnName);
                values.add(columnValue);
                columns.add(columnName);
                parameterSubstitutes.add("?");
            }
            String valuePlaceholders = "(" + TextUtils.join(",", parameterSubstitutes) + ")";
            String columnNames = "(" + TextUtils.join(",", columns) + ")";
            String stmt = "insert or replace into " + tableName + " " + columnNames + " values "
                    + valuePlaceholders;
            db.execSQL(stmt, values.toArray(new String[values.size()]));
        }
        return rowsN;
    }

    /**
     * Deletes rows from a table that do not contain the id in the given list, returns the number
     * of rows deleted
     * @param db
     * @param tableName
     * @param idsList
     * @return
     * @throws JSONException
     */
    private int deleteMissingIDsFromTable(SQLiteDatabase db, String tableName, JSONArray idsList)
            throws JSONException
    {
        int records_deleted = 0;
        Set<Integer> idsInDb = new HashSet<>();
        Set<Integer> idsUpdated = new HashSet<>();
        Cursor cursor = db.query(tableName, new String[]{"id"}, null, null, null, null, null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            idsInDb.add(cursor.getInt(0));
            cursor.moveToNext();
        }
        cursor.close();
        int arrLength = idsList.length();
        for(int i = 0; i < arrLength; i++) {
            idsUpdated.add(idsList.getInt(i));
        }
        idsInDb.removeAll(idsUpdated);
        if(!idsInDb.isEmpty()) {
            String args = TextUtils.join(",", idsInDb);
            String stmt = String.format("delete from " + tableName + " where id in (%s)", args);
            db.execSQL(stmt);
            records_deleted = idsInDb.size();
        }
        return records_deleted;
    }
}
