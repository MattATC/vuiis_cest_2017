package vanderbilt.vuiis_cest_2017.DataManager;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import vanderbilt.vuiis_cest_2017.Data.Agenda.AgendaDataSource;
import vanderbilt.vuiis_cest_2017.Data.DatabaseHelper;
import vanderbilt.vuiis_cest_2017.Data.DatabaseMetaData;
import vanderbilt.vuiis_cest_2017.Data.News.NewsManager;
import vanderbilt.vuiis_cest_2017.Data.Rating.PresentationRatingManager;
import vanderbilt.vuiis_cest_2017.MainActivity;
import vanderbilt.vuiis_cest_2017.R;
import vanderbilt.vuiis_cest_2017.Utilities.NetworkTaskException;

/**
 * Created by Matthew Christian on 3/16/17.
 */

public class AppDataManager
        implements DataUpdateCheckTaskDelegate, DataUpdateTaskDelegate,
        RetryUpdateFragment.RetryUpdateFragmentListener {
    final private String UPDATE_FRAGMENT_TAG = "update";
    final private String RETRY_UPDATE_FRAGMENT_TAG = "retry_update";

    static private Map<String, Set<DataUpdateListener>> mUpdateSubscribers = new HashMap<>();

    private boolean mUpdateCheckInProgress;
    private boolean mUpdateInProgress;
    private DataUpdateEntities mDataUpdateEntities;

    private AppCompatActivity mActivity;

    public AppDataManager(AppCompatActivity activity) {
        mActivity = activity;
    }

    public void updateDataSources(DataUpdateInfo dataUpdateInfo) {
        mUpdateInProgress = true;
        if(dataUpdateInfo == null) {
            dataUpdateInfo = new DataUpdateInfo(-1,
                    new HashSet<>(Arrays.asList(DatabaseMetaData.PUBLIC_TABLES)), null);
        }
        DatabaseHelper dataHelper = new DatabaseHelper(mActivity);
        AgendaDataSource.updateAgenda(dataHelper, dataUpdateInfo);
        PresentationRatingManager.updateRatings(dataHelper, dataUpdateInfo);
        NewsManager.updateNews(dataHelper, dataUpdateInfo);
        //Notify subscribers that data was changed
        Set<DataUpdateListener> calledSubscribers = new HashSet<>();
        for(String topic: dataUpdateInfo.getTablesAffected()) {
            Set<DataUpdateListener> subscribers = mUpdateSubscribers.get(topic);
            if(subscribers != null) {
                for (DataUpdateListener subscriber : subscribers) {
                    if(!calledSubscribers.contains(subscriber)) {
                        subscriber.onDataUpdate(dataUpdateInfo);
                        calledSubscribers.add(subscriber);
                    }
                }
            }
        }
        DatabaseMetaData.updateDateTablesLastModified(new DatabaseHelper(mActivity));
        mUpdateInProgress = false;
    }

    public void subscribeToDataUpdate(String table, DataUpdateListener listener) {
        Set<DataUpdateListener> subscribers = mUpdateSubscribers.get(table);
        if(subscribers == null) {
            subscribers = new HashSet<>();
            mUpdateSubscribers.put(table, subscribers);
        }
        subscribers.add(listener);
    }

    public void subscribeToDataUpdate(Set<String> tables, DataUpdateListener listener) {
        for(String table: tables) {
            subscribeToDataUpdate(table, listener);
        }
    }

    public void unsubscribeFromDataUpdate(String table, DataUpdateListener listener) {
        Set<DataUpdateListener> subscribers = mUpdateSubscribers.get(table);
        if(subscribers != null) {
            subscribers.remove(listener);
        }
    }

    public void unsubscribeFromDataUpdate(Set<String> tables, DataUpdateListener listener) {
        for(String table: tables) {
            unsubscribeFromDataUpdate(table, listener);
        }
    }

    public void removeSubscriptions(DataUpdateListener listener) {
        for(Set<DataUpdateListener> subscribers: mUpdateSubscribers.values()) {
            subscribers.remove(listener);
        }
    }

    public void checkDataUpdate() {
        if(mDataUpdateEntities != null && mDataUpdateEntities.hasEntities()) {
            doDataUpdate(mDataUpdateEntities);
            mDataUpdateEntities = null;
        }
        else if(!mUpdateInProgress && !mUpdateCheckInProgress) {
            if(((MainActivity)mActivity).isActive()) {
                FragmentManager fragmentManager = mActivity.getSupportFragmentManager();
                UpdateAppDataFragment fragment = (UpdateAppDataFragment)
                        fragmentManager.findFragmentByTag(UPDATE_FRAGMENT_TAG);
                if (fragment != null) {
                    fragment.dismiss();
                }
            }
            mUpdateCheckInProgress = true;
            new DataUpdateCheckTask(this).execute("");
        }
    }

    /**
     * Show a dialog to the user while waiting for updates, if not already showing
     * Launch the update async taskShow a dialog to the user while waiting for updates,
     * if not already showing
     * @param dataUpdateEntities
     */
    public void doDataUpdate(DataUpdateEntities dataUpdateEntities) {
        if(!mUpdateInProgress) {
            mUpdateInProgress = true;
            FragmentManager fragmentManager = mActivity.getSupportFragmentManager();
            UpdateAppDataFragment fragment =
                    (UpdateAppDataFragment) fragmentManager.findFragmentByTag(UPDATE_FRAGMENT_TAG);
            if(fragment == null) {
                new UpdateAppDataFragment().show(fragmentManager, UPDATE_FRAGMENT_TAG);
            }
            DatabaseHelper dataHelper = new DatabaseHelper(mActivity);
            new DataUpdateTask(dataHelper, dataUpdateEntities, this).execute("");
        }
    }

    /**
     * Called from RetryUpdateFragment, which is opened by onAppUpdateCheckFailed
     */
    @Override
    public void onRetryUpdateRequest() {
        checkDataUpdate();
    }

    /**
     * This method and the other TaskDelegate methods will be called from an AsyncTask,
     * so anything that could try to commit FragmentTransaction (i.e. starting or dismissing a
     * fragment) could result in state loss/IllegalStateException. Instead of doing the update
     * immediately, we record that it needs to be done later and wait until we can commit
     * FragmentTransaction again (i.e. outside of an Android lifecycle method). Referred to the
     * following URL on ways to avoid this issue:
     *  http://www.androiddesignpatterns.com/2013/08/fragment-transaction-commit-state-loss.html
     * @param dataUpdateEntities
     */
    @Override
    public void onAppUpdateChecked(DataUpdateEntities dataUpdateEntities) {
        mUpdateCheckInProgress = false;
        if(dataUpdateEntities.hasEntities()) {
            if(((MainActivity)mActivity).isActive()) {
                doDataUpdate(dataUpdateEntities);
            }
            else {
                mDataUpdateEntities = dataUpdateEntities;
            }
        }
    }

    /**
     * If the update failed, tell the user why with a dialog and give them an option to retry
     * @param networkTaskException
     */
    @Override
    public void onAppUpdateCheckFailed(NetworkTaskException networkTaskException) {
        if(((MainActivity)mActivity).isActive()) {
            FragmentManager fragmentManager = mActivity.getSupportFragmentManager();

            UpdateAppDataFragment fragment =
                    (UpdateAppDataFragment) fragmentManager.findFragmentByTag(UPDATE_FRAGMENT_TAG);
            if(fragment == null) {
                new UpdateAppDataFragment().show(fragmentManager, UPDATE_FRAGMENT_TAG);
            }

            RetryUpdateFragment ruFragment = new RetryUpdateFragment();
            Bundle args = new Bundle();
            NetworkTaskException.ErrorCode errorCode = networkTaskException.getErrorCode();
            args.putInt("error_reason", errorCode.ordinal());
            String errMessage;
            switch (errorCode) {
                case SERVER_CONNECTION_ERROR:
                    errMessage = mActivity.getString(R.string.error_msg_server_connection);
                    break;
                case INTERNET_CONNECTION_ERROR:
                    errMessage = mActivity.getString(R.string.error_msg_internet_connection);
                    break;
                case STATUS_ERROR:
                    errMessage = mActivity.getString(R.string.error_msg_server_status);
                    break;
                case DECODING_ERROR:
                    errMessage = mActivity.getString(R.string.error_msg_server_decoding);
                    break;
                case UNKNOWN_ERROR:
                    errMessage = mActivity.getString(R.string.error_msg_server_unknown);
                    break;
                default:
                    errMessage = "";
                    break;
            }
            args.putString("error_message", errMessage);
            ruFragment.setArguments(args);
            ruFragment.setListener(this);
            ruFragment.show(fragmentManager, RETRY_UPDATE_FRAGMENT_TAG);
        }
        mUpdateCheckInProgress = false;
    }

    @Override
    public void onAppUpdateCheckCancelled() {
        mUpdateCheckInProgress = false;
    }

    /**
     * Update the app data
     * @param dataUpdateInfo
     */
    @Override
    public void onAppUpdated(DataUpdateInfo dataUpdateInfo) {
        if(((MainActivity)mActivity).isActive()) {
            FragmentManager fragmentManager = mActivity.getSupportFragmentManager();
            UpdateAppDataFragment fragment =
                    (UpdateAppDataFragment) fragmentManager.findFragmentByTag(UPDATE_FRAGMENT_TAG);
            if (fragment != null) {
                fragment.dismiss();
            }
        }
        if(dataUpdateInfo != null) {
            if(dataUpdateInfo.anyTablesAffected()) {
                updateDataSources(dataUpdateInfo);
            }
        }
    }

    @Override
    public void onAppUpdateCancelled() {
        if(((MainActivity)mActivity).isActive()) {
            FragmentManager fragmentManager = mActivity.getSupportFragmentManager();
            UpdateAppDataFragment fragment =
                    (UpdateAppDataFragment) fragmentManager.findFragmentByTag(UPDATE_FRAGMENT_TAG);
            if (fragment != null) {
                fragment.dismiss();
            }
        }
        mUpdateInProgress = false;
    }
}
