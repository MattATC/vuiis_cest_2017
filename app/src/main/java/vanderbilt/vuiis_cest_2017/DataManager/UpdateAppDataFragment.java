package vanderbilt.vuiis_cest_2017.DataManager;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import vanderbilt.vuiis_cest_2017.R;

/**
 * Created by Matthew Christian on 3/16/17.
 */

public class UpdateAppDataFragment extends DialogFragment {
    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //Show a dialog to the user while waiting for updates
        String message = String.format(getString(R.string.dialog_updating),
                getString(R.string.app_name));
        ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("Please wait...");
        progressDialog.setMessage(message);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }
}
