package vanderbilt.vuiis_cest_2017.DataManager;

/**
 * Created by Matthew Christian on 3/15/17.
 */

public interface DataUpdateTaskDelegate {
    void onAppUpdated(DataUpdateInfo dataUpdateInfo);
    void onAppUpdateCancelled();
}
