package vanderbilt.vuiis_cest_2017.DataManager;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import vanderbilt.vuiis_cest_2017.Utilities.NetworkTaskCommon;
import vanderbilt.vuiis_cest_2017.Utilities.NetworkTaskException;

/**
 * Created by Matthew Christian on 4/5/17.
 */

public class RetrieveAPIKeyTask extends AsyncTask<Integer, Void, Boolean> {
    private static final String KEY_SCRIPT_PATH = "key";

    private RetrieveAPIKeyTaskDelegate mTaskDelegate;
    private String mPasscode;
    private NetworkTaskException mTaskException;
    private String mAPIKey;

    public RetrieveAPIKeyTask(RetrieveAPIKeyTaskDelegate taskDelegate, String passcode) {
        mTaskDelegate = taskDelegate;
        mPasscode = passcode;
        mAPIKey = "";
    }

    @Override
    protected Boolean doInBackground(Integer... integers) {
        boolean result = false;
        try {
            mAPIKey = retrieveKey();
            if(!mAPIKey.isEmpty()) {
                result = true;
            }
        }
        catch (NetworkTaskException e) {
            mTaskException = e;
        }
        return result;
    }

    private String retrieveKey() throws NetworkTaskException {
        String apiKey = "";
        try {
            Map<String, String> header = new HashMap<>();
            Map<String, String> paramMap = new HashMap<>();
            paramMap.put("key", mPasscode);
            String urlParams = NetworkTaskCommon.encodeParameterMap(paramMap);
            String response = new String(NetworkTaskCommon.getServerHTTPData(KEY_SCRIPT_PATH,
                    urlParams, "", header), "UTF-8");
            JSONObject keyObject = new JSONObject(response);
            if(keyObject.has("message") && keyObject.getString("message").equals("Access Denied")) {
                throw new NetworkTaskException(NetworkTaskException.ErrorCode.INVLAID_CREDENTIALS);
            }
            apiKey = keyObject.getString("key");
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        catch(NetworkTaskException e) {
            throw e;
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return apiKey;
    }

    @Override
    protected void onPostExecute(Boolean success) {
        if(mTaskDelegate != null) {
            if(success) {
                mTaskDelegate.onAPIKeyRetrieval(mAPIKey);
            }
            else {
                mTaskDelegate.onAPIKeyRetrievalFailed(mTaskException);
            }
        }
    }

    @Override
    protected void onCancelled() {
        if(mTaskDelegate != null) {
            mTaskDelegate.onAPIKeyRetrievalCancelled();
        }
    }
}
