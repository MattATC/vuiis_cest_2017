package vanderbilt.vuiis_cest_2017.DataManager;

import java.util.Set;

import vanderbilt.vuiis_cest_2017.Data.NewsBundle;

/**
 * Created by Matthew Christian on 3/15/17.
 */

public class DataUpdateInfo {
    private int mRecordsUpdated;
    private Set<String> mTablesAffected;
    private NewsBundle mNewsBundle;

    public DataUpdateInfo(int recordsUpdated, Set<String> tablesAffected, NewsBundle newsBundle) {
        mRecordsUpdated = recordsUpdated;
        mTablesAffected = tablesAffected;
        mNewsBundle = newsBundle;
    }

    public int getRecordsUpdated() {
        return mRecordsUpdated;
    }

    public Set<String> getTablesAffected() {
        return mTablesAffected;
    }

    public boolean anyTablesAffected() { return !mTablesAffected.isEmpty(); }

    public boolean checkTablesAffected(Set<String> checkTables) {
        for(String checkTable: checkTables) {
            if(mTablesAffected.contains(checkTable)) {
                return true;
            }
        }
        return false;
    }

    public NewsBundle getNewsBundle() { return mNewsBundle; }
}
