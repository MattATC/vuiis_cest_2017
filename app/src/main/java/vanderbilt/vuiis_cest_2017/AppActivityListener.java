package vanderbilt.vuiis_cest_2017;

/**
 * Created by Matthew Christian on 3/14/17.
 */

public interface AppActivityListener {

    void enableUpNavigation();

    void disableUpNavigation();

    void enableNewsButton();

    void disableNewsButton();

    void enableFeatureTabs();

    void disableFeatureTabs();
}
