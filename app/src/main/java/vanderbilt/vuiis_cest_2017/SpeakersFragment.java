package vanderbilt.vuiis_cest_2017;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ListAdapter;

import com.squareup.leakcanary.RefWatcher;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import vanderbilt.vuiis_cest_2017.Data.Agenda.AgendaDataSource;
import vanderbilt.vuiis_cest_2017.Data.Agenda.AgendaDetailViewBuilder;
import vanderbilt.vuiis_cest_2017.Data.Agenda.PersonData;
import vanderbilt.vuiis_cest_2017.Data.Agenda.PresentationData;
import vanderbilt.vuiis_cest_2017.Data.Agenda.SectionData;
import vanderbilt.vuiis_cest_2017.Data.Agenda.SectionTypeData;
import vanderbilt.vuiis_cest_2017.Data.Agenda.SpeakerItemAdapter;
import vanderbilt.vuiis_cest_2017.Data.Agenda.SpeakerListItem;
import vanderbilt.vuiis_cest_2017.Data.Agenda.SpeakerPerson;
import vanderbilt.vuiis_cest_2017.Data.DatabaseHelper;
import vanderbilt.vuiis_cest_2017.Data.Rating.RatingBarTracker;
import vanderbilt.vuiis_cest_2017.DataManager.AppDataManager;
import vanderbilt.vuiis_cest_2017.DataManager.DataUpdateInfo;
import vanderbilt.vuiis_cest_2017.DataManager.DataUpdateListener;
import vanderbilt.vuiis_cest_2017.ViewController.MasterDetailController.MasterDetailController;
import vanderbilt.vuiis_cest_2017.ViewController.MasterDetailController.MasterDetailControllerListener;
import vanderbilt.vuiis_cest_2017.ViewController.ViewControllerFragment;
import vanderbilt.vuiis_cest_2017.ViewController.ViewControllerHandle;

/**
 * Created by Matthew Christian on 3/3/17.
 */
public class SpeakersFragment extends ViewControllerFragment
        implements MasterDetailControllerListener, DataUpdateListener {

    public static final String TAB_TITLE = "Presenters";

    private MasterDetailController mController;
    private LayoutInflater mLayoutInflater;
    private List<SpeakerListItem> mSpeakerListItems = new ArrayList<>();
    private SpeakerItemAdapter mAdapter;

    /**
     * Returns a new instance of this fragment
     */
    public static SpeakersFragment newInstance(int sectionNumber) {
        return new SpeakersFragment();
    }

    public SpeakersFragment() {
        mController = new MasterDetailController(this, null, this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        RefWatcher refWatcher = ThisApplication.getRefWatcher();
        refWatcher.watch(this);
    }

    @Override
    public ViewControllerHandle getViewController() {
        return mController;
    }

    @Override
    public void doCustomAction(int action, Bundle params) {}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        MainActivity mainActivity = (MainActivity)getActivity();
        mController.setAppActivityListener(mainActivity);
        AppDataManager appDataManager = mainActivity.getAppDataManager();
        if(appDataManager != null) {
            appDataManager.subscribeToDataUpdate(AgendaDataSource.getTables(), this);
        }
    }

    @Override
    public void onDetach() {
        MainActivity mainActivity = (MainActivity)getActivity();
        AppDataManager appDataManager = mainActivity.getAppDataManager();
        if(appDataManager != null) {
            appDataManager.unsubscribeFromDataUpdate(AgendaDataSource.getTables(), this);
        }
        super.onDetach();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mController.onConfigurationChanged(newConfig);
    }

    @Override
    public final View onCreateView(LayoutInflater inflater, ViewGroup container,
                                   Bundle savedInstanceState) {
        mLayoutInflater = inflater;
        return mController.onCreateView(inflater, container, savedInstanceState);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        mController.onCreateOptionsMenu(menu, menuInflater);
        MenuItem itemShowCurrent = menu.findItem(R.id.action_show_current);
        if(itemShowCurrent != null) {
            itemShowCurrent.setVisible(false);
        }
    }

    @Override
    public ListAdapter getListAdapter() {
        mAdapter = new SpeakerItemAdapter(getContext(), mSpeakerListItems);
        updateItems(AgendaDataSource.getPeople().values());
        return mAdapter;
    }

    @Override
    public void buildDetailView(FrameLayout detailPanel, int position, int detailPanelWidth) {
        Log.d("SpeakersFragment", "buildDetailView");
        SpeakerPerson item = (SpeakerPerson)mSpeakerListItems.get(position);
        PersonData person = item.getPerson();
        PresentationData presentation = AgendaDataSource.getPresentation(
                person.getPresentationID());
        MainActivity mainActivity = (MainActivity)getActivity();
        DatabaseHelper dataHelper = new DatabaseHelper(mainActivity);
        AgendaDetailViewBuilder.buildDetailView(detailPanel, detailPanelWidth, mLayoutInflater,
                presentation, person, RatingBarTracker.getInstance(),
                AgendaDetailViewBuilder.ViewTag.SPEAKERS, dataHelper, mainActivity);
    }

    @Override
    public void updateOptionMenuOnNoneSelected() {

    }

    @Override
    public void updateOptionMenuOnSelected(int position) {

    }

    @Override
    public void onDataUpdate(DataUpdateInfo dataUpdateInfo) {
        updateItems(AgendaDataSource.getPeople().values());
        mController.restoreState();
    }

    private void updateItems(Collection<PersonData> people) {
        mSpeakerListItems.clear();
        Map<PersonData, SpeakerListItem> speakerMap = new HashMap<>();

        //Only show presenters for oral presentation and power pitch
        for(SectionData section: AgendaDataSource.getSections().values()) {
            SectionTypeData sectionType = AgendaDataSource.getSectionType(
                    section.getSectionTypeID());
            switch(sectionType.getTitle()) {
                case SectionTypeData.TYPE_ORAL_PRESENTATION:
                case SectionTypeData.TYPE_POWER_PITCH:
                    for (PresentationData presentation : section.getPresentations()) {
                        PersonData person = AgendaDataSource.getPerson(presentation.getPersonID());
                        if(person != null && !speakerMap.containsKey(person)) {
                            SpeakerPerson speakerPerson = new SpeakerPerson(person);
                            speakerMap.put(person, speakerPerson);
                        }
                    }
                    break;
            }
        }
        List<SpeakerListItem> speakers = new ArrayList<>(speakerMap.values());
        Collections.sort(speakers);
        mSpeakerListItems.addAll(speakers);
        mAdapter.notifyDataSetChanged();
    }
}
