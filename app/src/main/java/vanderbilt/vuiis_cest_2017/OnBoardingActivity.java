package vanderbilt.vuiis_cest_2017;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import vanderbilt.vuiis_cest_2017.DataManager.RetrieveAPIKeyTask;
import vanderbilt.vuiis_cest_2017.DataManager.RetrieveAPIKeyTaskDelegate;
import vanderbilt.vuiis_cest_2017.Utilities.NetworkTaskException;

/**
 * Created by Matthew Christian on 4/5/17.
 */
public class OnBoardingActivity extends AppCompatActivity
        implements RetrieveAPIKeyTaskDelegate {
    private Button mContinueButton;
    private Button mSkipButton;

    public static void startActivityForOnboarding(Activity activity) {
        Intent intent = new Intent(activity, OnBoardingActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboarding);

        //Disable the up button
        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(false);
        }

        final OnBoardingActivity onBoardingActivity = this;
        final EditText etPasscode = (EditText)findViewById(R.id.et_passcode);
        mContinueButton = (Button)findViewById(R.id.btn_continue);
        mContinueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mContinueButton.setEnabled(false);
                mSkipButton.setEnabled(false);
                new RetrieveAPIKeyTask(onBoardingActivity,
                        etPasscode.getText().toString()).execute(0);
            }
        });

        mSkipButton = (Button)findViewById(R.id.btn_skip);
        mSkipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mContinueButton.setEnabled(false);
                mSkipButton.setEnabled(false);
                onBoardingActivity.skipOnBoard();
            }
        });
    }

    @Override
    public void onAPIKeyRetrieval(String apiKey) {
        ThisApplication.setServerAPIKey(apiKey);
        //Toast.makeText(this, "Successfully retrieved API key: " + apiKey,
        //        Toast.LENGTH_LONG).show();
        setResult(Activity.RESULT_OK, getIntent());
        finish();
    }

    @Override
    public void onAPIKeyRetrievalFailed(NetworkTaskException networkTaskException) {
        String errMessage = getString(R.string.error_msg_server_invalid);
        if(networkTaskException != null) {
            NetworkTaskException.ErrorCode errorCode = networkTaskException.getErrorCode();
            switch (errorCode) {
                case SERVER_CONNECTION_ERROR:
                    errMessage = getString(R.string.error_msg_server_connection);
                    break;
                case INTERNET_CONNECTION_ERROR:
                    errMessage = getString(R.string.error_msg_internet_connection);
                    break;
                case STATUS_ERROR:
                    errMessage = getString(R.string.error_msg_server_status);
                    break;
                case DECODING_ERROR:
                    errMessage = getString(R.string.error_msg_server_decoding);
                    break;
                case INVLAID_CREDENTIALS:
                    errMessage = getString(R.string.error_msg_server_invalid);
                    break;
                case UNKNOWN_ERROR:
                    errMessage = getString(R.string.error_msg_server_unknown);
                    break;
                default:
                    errMessage = "";
                    break;
            }
        }
        Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show();
        mContinueButton.setEnabled(true);
        mSkipButton.setEnabled(true);
    }

    @Override
    public void onAPIKeyRetrievalCancelled() {
        skipOnBoard();
    }

    public void skipOnBoard() {
        setResult(Activity.RESULT_CANCELED, getIntent());
        finish();
    }
}
