package vanderbilt.vuiis_cest_2017.ViewController.SimpleController;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import vanderbilt.vuiis_cest_2017.AppActivityListener;
import vanderbilt.vuiis_cest_2017.ViewController.ViewControllerHandle;

/**
 * Created by Matthew Christian on 3/20/17.
 */
public class SimpleController extends ViewControllerHandle {
    private SimpleControllerListener mSCListener;
    private Menu mMenu;
    private LayoutInflater mLayoutInflater;
    private ViewGroup mContainerView;

    public SimpleController(Fragment fragment,
                            AppActivityListener appActivityListener,
                            SimpleControllerListener scListener)
    {
        super(fragment, appActivityListener);
        mSCListener = scListener;
    }

    public Fragment getFragment() {
        return mFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mLayoutInflater = inflater;
        mContainerView = container;
        return mSCListener.buildView(inflater, container);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        mMenu = menu;
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void restoreAppActivity() {

    }

    @Override
    public void restoreState() {
        mSCListener.buildView(mLayoutInflater, mContainerView);
    }

    @Override
    public Menu getMenu() {
        return mMenu;
    }
}
