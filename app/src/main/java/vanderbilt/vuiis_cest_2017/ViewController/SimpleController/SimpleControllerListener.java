package vanderbilt.vuiis_cest_2017.ViewController.SimpleController;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Matthew Christian on 3/20/17.
 */

public interface SimpleControllerListener {
    View buildView(LayoutInflater inflater, ViewGroup container);
}
