package vanderbilt.vuiis_cest_2017.ViewController.MasterDetailController;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import vanderbilt.vuiis_cest_2017.AppActivityListener;
import vanderbilt.vuiis_cest_2017.R;
import vanderbilt.vuiis_cest_2017.Utilities.GUI;

/**
 * Created by Matthew Christian on 3/13/17.
 * Portrait state of Master Detail Controller
 */
public final class MasterDetailStatePortrait extends MasterDetailControllerState {

    public MasterDetailStatePortrait(MasterDetailController controller,
                                     MasterDetailControllerListener viewControllerListener)
    {
        super(controller, viewControllerListener);
    }

    @Override
    final protected View updateLayout(LayoutInflater inflater, ViewGroup container)
    {
        mRootLayout = (LinearLayout)inflater.inflate(R.layout.fragment_master_detail, container, false);
        mMasterFrame = (FrameLayout) mRootLayout.findViewById(R.id.frame_master);
        mDetailFrame = (FrameLayout) mRootLayout.findViewById(R.id.frame_detail);
        setUpListView(inflater);
        return mRootLayout;
    }

    @Override
    final public void showDetailView(int position)
    {
        mMasterFrame.setVisibility(View.GONE);
        mDetailFrame.removeAllViews();
        mDetailFrame.setVisibility(View.VISIBLE);
        mController.setShowDetail(true);
        restoreAppActivity();
        int detailPanelWidth = mRootLayout.getWidth();
        mListener.buildDetailView(mDetailFrame, position, detailPanelWidth);
        mController.getControllerListener().updateOptionMenuOnSelected(position);
    }

    @Override
    final public void onBackPressed()
    {
        mDetailFrame.setVisibility(View.GONE);
        mMasterFrame.setVisibility(View.VISIBLE);
        mController.setShowDetail(false);
        restoreAppActivity();
        mController.getControllerListener().updateOptionMenuOnNoneSelected();
    }

    @Override
    final public void restoreSharedState()
    {
        int selection = mController.getSelection();
        if(selection > -1) {
            mDummy.requestFocus();
            mListView.setSelection(selection); //Scroll the view to the active selection
            mListView.requestFocusFromTouch();
            GUI.centerScrollListView(mListView, selection);
            if(mController.getShowDetail()) {
                mListView.performItemClick(mListView.getChildAt(selection), selection,
                        mListView.getItemIdAtPosition(selection));
            }
            else {
                mListView.setItemChecked(selection, true);
            }
            mListener.updateOptionMenuOnSelected(selection);
        }
        else {
            mListener.updateOptionMenuOnNoneSelected();
        }
    }

    @Override
    final public void restoreAppActivity() {
        AppActivityListener appActivityListener = mController.getAppActivityListener();
        if (mController.getShowDetail()) {
            appActivityListener.enableUpNavigation();
            appActivityListener.disableNewsButton();
            appActivityListener.disableFeatureTabs();
        }
        else {
            appActivityListener.disableUpNavigation();
            appActivityListener.enableNewsButton();
            appActivityListener.enableFeatureTabs();
        }
    }
}
