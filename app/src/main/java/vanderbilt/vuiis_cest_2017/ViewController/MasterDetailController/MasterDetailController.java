package vanderbilt.vuiis_cest_2017.ViewController.MasterDetailController;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import vanderbilt.vuiis_cest_2017.AppActivityListener;
import vanderbilt.vuiis_cest_2017.ViewController.ViewControllerHandle;

/**
 * Created by Matthew Christian on 3/13/17.
 * Controller class for Master Detail Controller, to be used by fragment to handle the layout
 */
public class MasterDetailController extends ViewControllerHandle
{
    final private static int TABLET_WIDTH_THRESHOLD = 700;

    private MasterDetailControllerListener mMDCListener;
    private boolean mShowDetail;
    private int mSelection;
    private MasterDetailControllerState mLayoutState;
    private Menu mMenu;
    private LayoutInflater mLayoutInflater;
    private ViewGroup mContainerView;
    private boolean mScreenIsTabletWidth;

    public MasterDetailController(Fragment fragment,
                                  AppActivityListener appActivityListener,
                                  MasterDetailControllerListener mdcListener)
    {
        super(fragment, appActivityListener);
        mMDCListener = mdcListener;
        mShowDetail = false;
        mSelection = -1;
        mLayoutState = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mLayoutInflater = inflater;
        mContainerView = container;
        updateLayoutState();
        return mLayoutState.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        updateLayoutState();
        ViewGroup fragmentView = (ViewGroup)mFragment.getView();
        mLayoutState.onConfigurationChanged(fragmentView, mLayoutInflater);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        mMenu = menu;
        mMDCListener.updateOptionMenuOnNoneSelected();
    }

    @Override
    public void onBackPressed() {
        mLayoutState.onBackPressed();
    }

    @Override
    public void restoreAppActivity() {
        mLayoutState.restoreAppActivity();
    }

    @Override
    public void restoreState() {
        if(mSelection != -1) {
            mLayoutState.restoreSharedState();
        }
        else {
            mMDCListener.updateOptionMenuOnNoneSelected();
        }
    }

    @Override
    public Menu getMenu() {
        return mMenu;
    }

    public Fragment getFragment() {
        return mFragment;
    }

    public MasterDetailControllerListener getControllerListener() {
        return mMDCListener;
    }

    public int getSelection() {
        return mSelection;
    }

    public void setSelection(int position) {
        mSelection = position;
    }

    public boolean getShowDetail() {
        return mShowDetail;
    }

    public void setShowDetail(boolean value) {
        mShowDetail = value;
    }

    public boolean isScreenTabletModeWidth() {
        DisplayMetrics displayMetrics = mFragment.getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        return (dpWidth >= TABLET_WIDTH_THRESHOLD);
    }

    protected void updateLayoutState()
    {
        //On large screens, use the landscape layout, else use the portrait layout
        if(isScreenTabletModeWidth()) {
            if(!mScreenIsTabletWidth || mLayoutState == null) {
                mLayoutState = new MasterDetailStateLandscape(this, mMDCListener);
                mScreenIsTabletWidth = true;
            }
        }
        else {
            if(mScreenIsTabletWidth || mLayoutState == null) {
                mLayoutState = new MasterDetailStatePortrait(this, mMDCListener);
                mScreenIsTabletWidth = false;
            }
        }
    }

    public void restoreActionBar() {
        mLayoutState.restoreActionBar();
    }

    protected void restoreSelection(int position) {
        if(position != -1) {
            setSelection(position);
            mLayoutState.restoreSharedState();
        }
        else {
            mMDCListener.updateOptionMenuOnNoneSelected();
        }
    }
}
