package vanderbilt.vuiis_cest_2017.ViewController.MasterDetailController;

import android.widget.FrameLayout;
import android.widget.ListAdapter;


/**
 * Created by Matthew Christian on 3/13/17.
 * View Controller Listener class for Master Detail Controller, used by View Controller states to
 * communicate with users of View Controller
 */
public interface MasterDetailControllerListener {

    ListAdapter getListAdapter();

    void buildDetailView(FrameLayout detailPanel, int position, int detailPanelWidth);

    void updateOptionMenuOnNoneSelected();

    void updateOptionMenuOnSelected(int position);
}
