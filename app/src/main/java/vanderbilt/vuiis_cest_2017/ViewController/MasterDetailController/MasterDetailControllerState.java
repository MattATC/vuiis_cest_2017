package vanderbilt.vuiis_cest_2017.ViewController.MasterDetailController;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;

import vanderbilt.vuiis_cest_2017.R;

/**
 * Created by Matthew Christian on 3/13/17.
 * Master Detail Controller State base class
 * Holds shared state information between two layout states: Single and Multi
 */
public abstract class MasterDetailControllerState implements AbsListView.OnItemClickListener
{
    protected MasterDetailController mController;
    protected MasterDetailControllerListener mListener;
    protected FrameLayout mMasterFrame;
    protected FrameLayout mDetailFrame;
    protected LinearLayout mRootLayout;
    protected ListView mListView;
    protected View mDummy;
    private boolean mLayoutUpdatePending;

    public abstract void onBackPressed();

    protected abstract View updateLayout(LayoutInflater inflater, ViewGroup container);

    protected abstract void showDetailView(int position);

    public abstract void restoreSharedState();

    public abstract void restoreAppActivity();

    public MasterDetailControllerState(MasterDetailController controller,
                                       MasterDetailControllerListener viewControllerListener)
    {
        mController = controller;
        mListener = viewControllerListener;
        mMasterFrame = null;
        mDetailFrame = null;
        mListView = null;
    }

    final public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        mController.getFragment().setHasOptionsMenu(true);
        //container.removeAllViewsInLayout();
        return updateLayout(inflater, container);
    }

    final protected void setUpListView(LayoutInflater inflater)
    {
        View view = inflater.inflate(R.layout.agenda_master, mMasterFrame, true);
        mListView = (ListView) view.findViewById(R.id.list);
        mListView.setEmptyView(view.findViewById(R.id.empty));
        mListView.setAdapter(mListener.getListAdapter());
        mListView.setOnItemClickListener(this);
        mDummy = view.findViewById(R.id.dummy);
    }

    final public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        mController.setSelection(position);
        showDetailView(position);
    }

    final public void onConfigurationChanged(final ViewGroup fragmentView,
                                             final LayoutInflater inflater)
    {
        final int oldWidth = fragmentView.getWidth();
        final int oldHeight = fragmentView.getHeight();
        fragmentView.removeAllViewsInLayout();
        final View contentView = updateLayout(inflater, fragmentView);
        fragmentView.addView(contentView);

        if(!mLayoutUpdatePending) {
            mLayoutUpdatePending = true;
            ViewTreeObserver vto = fragmentView.getViewTreeObserver();
            vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    if ((fragmentView.getWidth() != oldWidth) ||
                            (fragmentView.getHeight() != oldHeight)) {
                        mLayoutUpdatePending = false;
                        fragmentView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        restoreSharedState();
                    }
                }
            });
        }
    }

    final public void refreshListView() {
        if(mListView != null) {
            mListView.invalidateViews();
        }
    }

    public void restoreActionBar() {
        if(mController.getShowDetail()) {
            mListener.updateOptionMenuOnSelected(mController.getSelection());
        }
        else {
            mListener.updateOptionMenuOnNoneSelected();
        }
    }
}

