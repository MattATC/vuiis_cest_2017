package vanderbilt.vuiis_cest_2017.ViewController.MasterDetailController;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import vanderbilt.vuiis_cest_2017.AppActivityListener;
import vanderbilt.vuiis_cest_2017.R;
import vanderbilt.vuiis_cest_2017.Utilities.GUI;

/**
 * Created by Matthew Christian on 3/13/17.
 * Landscape state of Master Detail Controller
 */
public final class MasterDetailStateLandscape extends MasterDetailControllerState {

    public MasterDetailStateLandscape(MasterDetailController controller,
                                      MasterDetailControllerListener viewControllerListener) {
        super(controller, viewControllerListener);
    }

    @Override
    final protected View updateLayout(final LayoutInflater inflater, final ViewGroup container) {
        mRootLayout = (LinearLayout)inflater.inflate(R.layout.fragment_split_master_detail,
                container, false);
        mMasterFrame = (FrameLayout) mRootLayout.findViewById(R.id.frame_split_master);
        mDetailFrame = (FrameLayout) mRootLayout.findViewById(R.id.frame_split_detail);
        setUpListView(inflater);
        return mRootLayout;
    }

    @Override
    final public void showDetailView(int position) {
        mDetailFrame.removeAllViews();
        mDetailFrame.setVisibility(View.VISIBLE);
        if(position != -1) {
            int detailPanelWidth = mDetailFrame.getWidth();
            mListener.buildDetailView(mDetailFrame, position, detailPanelWidth);
            mController.getControllerListener().updateOptionMenuOnSelected(position);
        }
        else {
            mController.getControllerListener().updateOptionMenuOnNoneSelected();
        }
    }

    @Override
    final public void onBackPressed() {
    }

    @Override
    final public void restoreSharedState() {
        restoreAppActivity();
        int selection = mController.getSelection();
        if(selection > -1) {
            mDummy.requestFocus();
            mListView.setSelection(selection); //Scroll the view to the active selection
            mListView.requestFocusFromTouch();
            GUI.centerScrollListView(mListView, selection);
            mListView.performItemClick(mListView.getChildAt(selection), selection,
                    mListView.getItemIdAtPosition(selection));
            mListView.setItemChecked(selection, true);
            mListener.updateOptionMenuOnSelected(selection);
        }
        else {
            mListener.updateOptionMenuOnNoneSelected();
        }
    }

    @Override
    final public void restoreAppActivity() {
        AppActivityListener appActivityListener = mController.getAppActivityListener();
        appActivityListener.disableUpNavigation();
        appActivityListener.enableNewsButton();
        appActivityListener.enableFeatureTabs();
    }
}
