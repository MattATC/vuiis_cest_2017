package vanderbilt.vuiis_cest_2017.ViewController;

import android.os.Bundle;
import android.support.v4.app.Fragment;

/**
 * Created by Matthew Christian on 3/20/17.
 */

public abstract class ViewControllerFragment extends Fragment {
    public abstract ViewControllerHandle getViewController();
    public abstract void doCustomAction(int action, Bundle params);
}
