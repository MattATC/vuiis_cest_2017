package vanderbilt.vuiis_cest_2017.ViewController;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import vanderbilt.vuiis_cest_2017.AppActivityListener;

/**
 * Created by Matthew Christian on 3/13/17.
 */

public abstract class ViewControllerHandle {
    protected Fragment mFragment;
    protected AppActivityListener mAppActivityListener;

    public ViewControllerHandle(Fragment fragment, AppActivityListener appActivityListener) {
        mFragment = fragment;
        mAppActivityListener = appActivityListener;
    }

    public abstract View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState);
    public abstract void onConfigurationChanged(Configuration newConfig);
    public abstract void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater);
    public abstract void onBackPressed();
    public abstract void restoreAppActivity();
    public abstract void restoreState();
    public abstract Menu getMenu();

    final public void setAppActivityListener(AppActivityListener appActivityListener) {
        mAppActivityListener = appActivityListener;
    }

    final public AppActivityListener getAppActivityListener() {
        return mAppActivityListener;
    }
}
