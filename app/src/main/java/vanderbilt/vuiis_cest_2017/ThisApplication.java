package vanderbilt.vuiis_cest_2017;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.UUID;

import vanderbilt.vuiis_cest_2017.Data.DatabaseHelper;

/**
 * Created by Matthew Christian on 3/13/17.
 */
public class ThisApplication extends Application {
    private static final String CONFIG_FILE = "VUIIS_CEST_2017_CONFIG";
    private static final String INSTALLATION_FILE = "INSTALLATION";
    private static final String SERVER_API_KEY_CONFIG = "API";

    private static String mInstallationID = "";
    private static ThisApplication mInstance;

    private static RefWatcher mRefWatcher;

    public ThisApplication() {
        mInstance = this;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        mRefWatcher = LeakCanary.install(this);
    }

    public static String getInstallationID() {
        return mInstallationID;
    }

    public static Context getContext() { return mInstance; }

    public static ThisApplication get() {
        return mInstance;
    }

    public static RefWatcher getRefWatcher() {
        return mRefWatcher;
    }

    public static String getConfigValue(String key, String defaultValue) {
        String value = "";
        try {
            FileInputStream fis = mInstance.openFileInput(ThisApplication.CONFIG_FILE);
            Properties prop = new Properties();
            prop.load(fis);
            fis.close();
            value = prop.getProperty(key, defaultValue);
            //Log.d("ThisApplication", "Successfully read config file: " + key + " = " +
            //    value);
        }
        catch (IOException e) {
            Log.e("ThisApplication", "Failed to read config file: " + e.getMessage());
        }
        return value;
    }

    public static void saveConfigValue(String key, String value) {
        Properties prop = new Properties();
        try {
            FileInputStream fis = mInstance.openFileInput(ThisApplication.CONFIG_FILE);
            prop.load(fis);
            fis.close();
        }
        catch (IOException e) {
            Log.e("ThisApplication", "Failed to read config file: " + e.getMessage());
        }
        try {
            FileOutputStream fos = mInstance.openFileOutput(ThisApplication.CONFIG_FILE,
                    Context.MODE_PRIVATE);
            prop.setProperty(key, value);
            prop.store(fos, null);
            fos.flush();
            fos.close();
            //Log.d("ThisApplication", "Successfully wrote config file: " + key + " = " +
            //    value);
        }
        catch (IOException e) {
            Log.e("ThisApplication", "Failed to write config file: " + e.getMessage());
        }
    }

    public static boolean deleteInstallationID(Activity activity) {
        File installationFile = new File(activity.getFilesDir(), INSTALLATION_FILE);
        return installationFile.delete();
    }

    public static void initializeInstallationID(Activity activity) {
        File installationFile = new File(activity.getFilesDir(), INSTALLATION_FILE);
        boolean installationFileExists = installationFile.exists();
        try {
            String installationID = "";
            if(installationFileExists) {
                FileInputStream fis = mInstance.openFileInput(installationFile.getName());
                int c;
                while ((c = fis.read()) != -1) {
                    installationID += c;
                }
                Log.d("ThisApplication", "Installation ID was: " + installationID);
            }
            else {
                FileOutputStream fos = mInstance.openFileOutput(installationFile.getName(),
                        Context.MODE_PRIVATE);
                installationID = UUID.randomUUID().toString();
                fos.write(installationID.getBytes());
                fos.close();
                Log.d("ThisApplication", "Wrote new Installation ID: " + installationID);
                //Copy the assets database
                DatabaseHelper dbHelper = new DatabaseHelper(activity);
                SQLiteDatabase db = dbHelper.getWritableDatabase(); // Initialize the database
                db.close();
                //DatabaseHelper.copyAppDatabaseAsset(activity);
            }
            mInstallationID = installationID;
        }
        catch(FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getServerAPIKey() {
        return getConfigValue(SERVER_API_KEY_CONFIG, "");
    }

    public static void setServerAPIKey(String value) {
        saveConfigValue(SERVER_API_KEY_CONFIG, value);
    }

    public static boolean isServerAPIKeySet() {
        return !getConfigValue(SERVER_API_KEY_CONFIG, "").isEmpty();
    }
}
