package vanderbilt.vuiis_cest_2017.Utilities;

import android.graphics.Color;
import android.text.TextPaint;
import android.view.View;

/**
 * Created by matthew on 3/13/17.
 */

public class EmailSpan extends LinkableSpan {
    private String mEmail;
    private LinkOpener mLinkOpener;

    public EmailSpan(String email, int beginPos, int endPos, LinkOpener linkOpener) {
        super(beginPos, endPos);
        mEmail = email;
        mLinkOpener = linkOpener;
    }

    @Override
    public void onClick(View widget) {
        if(mLinkOpener != null) {
            mLinkOpener.composeEmail(mEmail, "Reservation", "");
        }
    }

    @Override
    public void updateDrawState(TextPaint ds){
        ds.setUnderlineText(false);
        ds.setColor(Color.rgb(20, 20, 200));
    }
}
