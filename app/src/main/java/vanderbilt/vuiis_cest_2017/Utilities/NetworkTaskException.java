package vanderbilt.vuiis_cest_2017.Utilities;

import java.io.IOException;

/**
 * Created by Matthew Christian on 3/15/17.
 */

public class NetworkTaskException extends IOException {
    public enum ErrorCode {
        SERVER_CONNECTION_ERROR, INTERNET_CONNECTION_ERROR, STATUS_ERROR, DECODING_ERROR,
        INVLAID_CREDENTIALS, UNKNOWN_ERROR
    }
    private ErrorCode mErrorCode;

    public NetworkTaskException(ErrorCode errorCode) {
        super();
        mErrorCode = errorCode;
    }

    public ErrorCode getErrorCode() {
        return mErrorCode;
    }
}