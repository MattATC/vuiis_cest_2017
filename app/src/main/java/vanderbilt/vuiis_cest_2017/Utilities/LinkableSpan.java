package vanderbilt.vuiis_cest_2017.Utilities;

import android.text.style.ClickableSpan;

/**
 * Created by matthew on 3/13/17.
 */

public abstract class LinkableSpan extends ClickableSpan {
    protected int mBeginPos;
    protected int mEndPos;

    public LinkableSpan(int beginPos, int endPos) {
        mBeginPos = beginPos;
        mEndPos = endPos;
    }

    public int getBeginPos() {
        return mBeginPos;
    }

    public int getEndPos() {
        return mEndPos;
    }
}
