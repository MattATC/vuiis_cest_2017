package vanderbilt.vuiis_cest_2017.Utilities;

/**
 * Created by Matthew Christian on 3/13/17.
 */

public interface LinkOpener {
    void openURL(String url);
    void dialPhoneNumber(String phoneNumber);
    void composeEmail(String emailAddress, String emailSubject, String emailBody);
}

