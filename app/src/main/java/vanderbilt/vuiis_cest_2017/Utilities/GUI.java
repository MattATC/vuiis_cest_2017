package vanderbilt.vuiis_cest_2017.Utilities;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import vanderbilt.vuiis_cest_2017.ThisApplication;

/**
 * Created by Matthew Christian on 4/24/15.
 */
public class GUI {
    final private static String URL_LINK_TYPE = "url";
    final private static String PHONE_LINK_TYPE = "phone";
    final private static String EMAIL_LINK_TYPE = "email";
    final private static Pattern mMarkupPattern = Pattern.compile("\\[\\[([^|]*)\\|([^|]*)\\|([^|]*)\\]\\]");

    /**
     * Converts the markdown that appears in a given text into clickable links and attaches
     * them to the given TextView.
     * An example of the format of the markdown: [[url|www.google.com|Google]]
     * @param textView
     * @param text
     * @param linkOpener
     */
    public static void processTextViewMarkdown(TextView textView, String text, LinkOpener linkOpener)
    {
        StringBuffer stringBuffer = new StringBuffer();
        Matcher matcher = mMarkupPattern.matcher(text);
        List<LinkableSpan> spans = new ArrayList<>();
        int matchLengthDiff = 0;

        while(matcher.find()) {
            final String linkType = matcher.group(1);
            final String link = matcher.group(2);
            final String textReplacement = matcher.group(3);
            final int linkBegin = matcher.start() - matchLengthDiff;
            matcher.appendReplacement(stringBuffer, textReplacement);
            final int linkEnd = linkBegin + textReplacement.length();
            matchLengthDiff = matcher.end() - linkEnd;
            if(linkOpener == null) continue;
            LinkableSpan newSpan = null;
            if(linkType.equals(URL_LINK_TYPE)) {
                newSpan = new HyperlinkSpan(link, linkBegin, linkEnd, linkOpener);
            }
            else if(linkType.equals(PHONE_LINK_TYPE)) {
                newSpan = new PhoneNumberSpan(link, linkBegin, linkEnd, linkOpener);
            }
            else if(linkType.equals(EMAIL_LINK_TYPE)) {
                newSpan = new EmailSpan(link, linkBegin, linkEnd, linkOpener);
            }
            if(newSpan != null) {
                spans.add(newSpan);
            }
        }
        matcher.appendTail(stringBuffer);

        SpannableString spannableText = new SpannableString(stringBuffer.toString());
        for(LinkableSpan span: spans) {
            spannableText.setSpan(span, span.getBeginPos(), span.getEndPos(),
                    Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        }

        textView.setText(spannableText);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    /**
     * Lowers the software keyboard
     * @param view
     */
    public static void lowerSoftKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager)
                ThisApplication.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /**
     * Scrolls a ListView up halfway so that the selected view is in the center of the screen
     * @param listView
     * @param selection
     */
    public static void centerScrollListView(final ListView listView, final int selection) {
        DisplayMetrics displayMetrics = ThisApplication.getContext().getResources()
                .getDisplayMetrics();
        float height = displayMetrics.heightPixels;  // Height of the screen
        listView.setSelectionFromTop(selection, (int)(height / 2));
        View selectedView = listView.getChildAt(selection);
        if(selectedView != null) {
            int h1 = listView.getHeight();
            int h2 = selectedView.getHeight();
            listView.setSelectionFromTop(selection, h1 / 2 - h2 / 2);
        }
    }
}

