package vanderbilt.vuiis_cest_2017.Utilities;

import android.graphics.Color;
import android.text.TextPaint;
import android.view.View;

/**
 * Created by matthew on 3/13/17.
 * This class determines the style of the hyperlinks that appear in TextViews and triggers an
 * event to the given URL opener when clicked
 */

public class HyperlinkSpan extends LinkableSpan {
    private String mURL;
    private LinkOpener mLinkOpener;

    public HyperlinkSpan(String url, int beginPos, int endPos, LinkOpener linkOpener) {
        super(beginPos, endPos);
        mURL = url;
        mLinkOpener = linkOpener;
    }

    @Override
    public void onClick(View widget) {
        if(mLinkOpener != null) {
            mLinkOpener.openURL(mURL);
        }
    }

    @Override
    public void updateDrawState(TextPaint ds){
        ds.setUnderlineText(false);
        ds.setColor(Color.rgb(20, 20, 200));
    }
}
