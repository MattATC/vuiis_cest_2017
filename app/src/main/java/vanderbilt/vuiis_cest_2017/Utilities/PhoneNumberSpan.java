package vanderbilt.vuiis_cest_2017.Utilities;

import android.graphics.Color;
import android.text.TextPaint;
import android.view.View;

/**
 * Created by matthew on 3/13/17.
 */

public class PhoneNumberSpan extends LinkableSpan {
    private String mNumber;
    private LinkOpener mLinkOpener;

    public PhoneNumberSpan(String url, int beginPos, int endPos, LinkOpener linkOpener) {
        super(beginPos, endPos);
        mNumber = url;
        mLinkOpener = linkOpener;
    }

    @Override
    public void onClick(View widget) {
        if(mLinkOpener != null) {
            mLinkOpener.dialPhoneNumber(mNumber);
        }
    }

    @Override
    public void updateDrawState(TextPaint ds){
        ds.setUnderlineText(false);
        ds.setColor(Color.rgb(20, 20, 200));
    }
}
