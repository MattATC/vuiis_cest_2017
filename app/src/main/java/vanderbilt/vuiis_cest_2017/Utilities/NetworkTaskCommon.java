package vanderbilt.vuiis_cest_2017.Utilities;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import vanderbilt.vuiis_cest_2017.ThisApplication;

/**
 * Created by Matthew Christian on 3/15/17.
 */

public class NetworkTaskCommon
{
    private static final String mServerURL =
            "xmzwd5v1za.execute-api.us-east-2.amazonaws.com/cest";
    private static final String GOOGLE_URL = "google.com";
    private static final String HTTP_PROTOCOL = "http://";
    private static final String HTTPS_PROTOCOL = "https://";
    private static final int HTTPS_PORT = 443;

    /**
     * Transforms the parameter map into an HTML form encoding
     * @param paramMap
     * @return
     */
    public static String encodeParameterMap(Map<String, String> paramMap) {
        String urlParams = "";
        try {
            List<String> parameterList = new ArrayList<>();
            if (paramMap != null) {
                Iterator<?> itr = paramMap.entrySet().iterator();
                while (itr.hasNext()) {
                    Map.Entry pair = (Map.Entry) itr.next();
                    String value = URLEncoder.encode((String) pair.getValue(), "UTF-8");
                    parameterList.add(pair.getKey() + "=" + value);
                }
            }
            urlParams = TextUtils.join("&", parameterList);
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return urlParams;
    }

    /**
     * Get the response from the app server script via HTTP GET request with data
     * @param scriptPath
     * @param urlParams
     * @param data
     * @param header
     * @return
     * @throws IOException, NetworkTaskException
     */
    public static byte[] getServerHTTPData(String scriptPath, String urlParams, final String data,
                                           final Map<String, String> header)
            throws IOException {
        return retrieveHTTPData(scriptPath, urlParams, new HttpURLConnectionCreator() {
            @Override
            public HttpURLConnection create(String url) throws IOException {
                URL urlObj = new URL(url);
                HttpURLConnection connection = (HttpURLConnection) urlObj.openConnection();
                connection.setRequestMethod("GET");
                for(Map.Entry<String, String> keyValuePair: header.entrySet()) {
                    connection.setRequestProperty(keyValuePair.getKey(), keyValuePair.getValue());
                }
                connection.setUseCaches(false);
                if(!data.isEmpty()) {
                    OutputStream os = connection.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                    writer.write(data);
                    writer.flush();
                    writer.close();
                    os.close();
                }
                return connection;
            }
        });
    }

    /**
     * Gets the response from the app server script via HTTP POST request
     * @param scriptPath
     * @param urlParams
     * @param data
     * @param header
     * @return
     * @throws IOException, NetworkTaskException
     */
    public static byte[] postServerHTTPData(String scriptPath, String urlParams, final String data,
                                            final Map<String, String> header)
            throws IOException {
        return retrieveHTTPData(scriptPath, urlParams, new HttpURLConnectionCreator() {
            @Override
            public HttpURLConnection create(String url) throws IOException {
                URL urlObj = new URL(url);
                HttpURLConnection connection = (HttpURLConnection) urlObj.openConnection();
                connection.setRequestMethod("POST");
                for(Map.Entry<String, String> keyValuePair: header.entrySet()) {
                    connection.setRequestProperty(keyValuePair.getKey(), keyValuePair.getValue());
                }
                connection.setUseCaches(false);
                if(!data.isEmpty()) {
                    OutputStream os = connection.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                    writer.write(data);
                    writer.flush();
                    writer.close();
                    os.close();
                }
                return connection;
            }
        });
    }

    /**
     * Retrieves the response from the app server script and handles error conditions
     * @param scriptPath
     * @param urlParams
     * @param connectionCreator
     * @return
     * @throws NetworkTaskException
     */
    private static byte[] retrieveHTTPData(String scriptPath, String urlParams,
                                           HttpURLConnectionCreator connectionCreator)
            throws NetworkTaskException
    {
        int responseCode = 0;
        if (!urlParams.isEmpty()) {
            urlParams = "?" + urlParams;
        }
        try {
            String url = HTTPS_PROTOCOL + mServerURL + "/" + scriptPath + urlParams;
            Log.d("retrieveHTTPData", "sending URL " + url);
            HttpURLConnection connection = connectionCreator.create(url);
            responseCode = connection.getResponseCode();
            if(responseCode == 200) {
                BufferedInputStream inputStream = new BufferedInputStream(
                        connection.getInputStream());
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                int c;
                while ((c = inputStream.read()) != -1) {
                    outputStream.write((char) c);
                }
                return outputStream.toByteArray();
            }
        }
        catch (UnsupportedEncodingException e) {
            Log.d("NetworkTaskCommon", "Encoding exception: " + e);
            e.printStackTrace();
        }
        catch (IOException e) {
            Log.d("NetworkTaskCommon", "IO exception: " + e);
            e.printStackTrace();
        }

        //If we reach this point, we were unable to connect to one of the script hosts
        if(responseCode == 403) {
            throw new NetworkTaskException(NetworkTaskException.ErrorCode.INVLAID_CREDENTIALS);
        }

        //We now check network connectivity to provide a more detailed error message
        ConnectivityManager cm = (ConnectivityManager) ThisApplication.getContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if(netInfo != null && netInfo.isConnectedOrConnecting()) {
            try {
                SocketAddress sockAddress = new InetSocketAddress(GOOGLE_URL, HTTPS_PORT);
                Socket sock = new Socket();
                sock.connect(sockAddress, 2000);
            }
            catch (IOException e) {
                //Could not reach Google, it is likely that there is no internet connection
                Log.d("NetworkTaskCommon", "Error connecting to Google: " + e);
                e.printStackTrace();
                throw new NetworkTaskException(
                        NetworkTaskException.ErrorCode.INTERNET_CONNECTION_ERROR);
            }
            //Google was reachable, therefore the script's host was not available
            throw new NetworkTaskException(NetworkTaskException.ErrorCode.SERVER_CONNECTION_ERROR);
        }
        else {
            //No network was reachable at all, maybe wifi is off?
            throw new NetworkTaskException(
                    NetworkTaskException.ErrorCode.INTERNET_CONNECTION_ERROR);
        }
    }

    private interface HttpURLConnectionCreator {
        HttpURLConnection create(String url) throws IOException;
    }

}
