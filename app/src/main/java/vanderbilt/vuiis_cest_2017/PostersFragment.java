package vanderbilt.vuiis_cest_2017;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ListAdapter;

import com.squareup.leakcanary.RefWatcher;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import vanderbilt.vuiis_cest_2017.Data.Agenda.AgendaDataSource;
import vanderbilt.vuiis_cest_2017.Data.Agenda.AgendaDetailViewBuilder;
import vanderbilt.vuiis_cest_2017.Data.Agenda.AgendaItemAdapter;
import vanderbilt.vuiis_cest_2017.Data.Agenda.AgendaListItem;
import vanderbilt.vuiis_cest_2017.Data.Agenda.AgendaSection;
import vanderbilt.vuiis_cest_2017.Data.Agenda.AgendaTimeSlot;
import vanderbilt.vuiis_cest_2017.Data.Agenda.PersonData;
import vanderbilt.vuiis_cest_2017.Data.Agenda.PresentationData;
import vanderbilt.vuiis_cest_2017.Data.Agenda.SectionData;
import vanderbilt.vuiis_cest_2017.Data.Agenda.SectionTypeData;
import vanderbilt.vuiis_cest_2017.Data.DatabaseHelper;
import vanderbilt.vuiis_cest_2017.Data.Rating.RatingBarTracker;
import vanderbilt.vuiis_cest_2017.DataManager.AppDataManager;
import vanderbilt.vuiis_cest_2017.DataManager.DataUpdateInfo;
import vanderbilt.vuiis_cest_2017.DataManager.DataUpdateListener;
import vanderbilt.vuiis_cest_2017.ViewController.MasterDetailController.MasterDetailController;
import vanderbilt.vuiis_cest_2017.ViewController.MasterDetailController.MasterDetailControllerListener;
import vanderbilt.vuiis_cest_2017.ViewController.ViewControllerFragment;
import vanderbilt.vuiis_cest_2017.ViewController.ViewControllerHandle;

/**
 * Created by Matthew Christian on 3/3/17.
 */
public class PostersFragment extends ViewControllerFragment
        implements MasterDetailControllerListener, DataUpdateListener {

    public static final String TAB_TITLE = "Posters";

    private MasterDetailController mController;
    private LayoutInflater mLayoutInflater;
    private List<AgendaListItem> mAgendaListItems = new ArrayList<>();
    private AgendaItemAdapter mAdapter;
    private RatingBarTracker mRatingBarTracker = new RatingBarTracker();

    /**
     * Returns a new instance of this fragment
     */
    public static PostersFragment newInstance(int sectionNumber) {
        return new PostersFragment();
    }

    public PostersFragment() {
        mController = new MasterDetailController(this, null, this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        RefWatcher refWatcher = ThisApplication.getRefWatcher();
        refWatcher.watch(this);
    }

    @Override
    public ViewControllerHandle getViewController() {
        return mController;
    }

    @Override
    public void doCustomAction(int action, Bundle params) {}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        MainActivity mainActivity = (MainActivity)getActivity();
        mController.setAppActivityListener(mainActivity);
        AppDataManager appDataManager = mainActivity.getAppDataManager();
        if(appDataManager != null) {
            appDataManager.subscribeToDataUpdate(AgendaDataSource.getTables(), this);
        }
    }

    @Override
    public void onDetach() {
        MainActivity mainActivity = (MainActivity)getActivity();
        AppDataManager appDataManager = mainActivity.getAppDataManager();
        if(appDataManager != null) {
            appDataManager.unsubscribeFromDataUpdate(AgendaDataSource.getTables(), this);
        }
        super.onDetach();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mController.onConfigurationChanged(newConfig);
    }

    @Override
    public final View onCreateView(LayoutInflater inflater, ViewGroup container,
                                   Bundle savedInstanceState) {
        mLayoutInflater = inflater;
        return mController.onCreateView(inflater, container, savedInstanceState);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        mController.onCreateOptionsMenu(menu, menuInflater);
        MenuItem itemShowCurrent = menu.findItem(R.id.action_show_current);
        if(itemShowCurrent != null) {
            itemShowCurrent.setVisible(false);
        }
    }

    @Override
    public ListAdapter getListAdapter() {
        mAdapter = new AgendaItemAdapter(getContext(), mAgendaListItems);
        updateItems();
        return mAdapter;
    }

    @Override
    public void buildDetailView(FrameLayout detailPanel, int position, int detailPanelWidth) {
        Log.d("PostersFragment", "buildDetailView");
        AgendaTimeSlot item = (AgendaTimeSlot)mAgendaListItems.get(position);
        PresentationData presentation = item.getPresentation();
        PersonData person = AgendaDataSource.getPerson(presentation.getPersonID());
        MainActivity mainActivity = (MainActivity)getActivity();
        DatabaseHelper dataHelper = new DatabaseHelper(mainActivity);
        AgendaDetailViewBuilder.buildDetailView(detailPanel, detailPanelWidth, mLayoutInflater,
                presentation, person, mRatingBarTracker, AgendaDetailViewBuilder.ViewTag.POSTERS,
                dataHelper, mainActivity);
    }

    @Override
    public void updateOptionMenuOnNoneSelected() {

    }

    @Override
    public void updateOptionMenuOnSelected(int position) {

    }

    @Override
    public void onDataUpdate(DataUpdateInfo dataUpdateInfo) {
        //Log.d("PostersFragment", "onDataUpdate called");
        updateItems();
        mController.restoreState();
    }

    private void updateItems() {
        //Log.d("PostersFragment", "updateItems called");
        mAgendaListItems.clear();

        //Get all poster sections
        MainActivity mainActivity = (MainActivity)getActivity();
        DatabaseHelper dataHelper = new DatabaseHelper(mainActivity);
        Map<Integer, AgendaSection> agendaSections = new HashMap<>();

        for(SectionData section: AgendaDataSource.getSections().values()) {
            int sectionID = section.getID();
            SectionTypeData sectionType = AgendaDataSource.getSectionType(
                    section.getSectionTypeID());
            if(sectionType.getTitle().equals(SectionTypeData.TYPE_POSTER)) {
                AgendaSection agendaSection = new AgendaSection(section);
                agendaSections.put(sectionID, agendaSection);
                for (PresentationData presentation : section.getPresentations()) {
                    AgendaTimeSlot agendaTimeSlot = new AgendaTimeSlot(mainActivity, agendaSection,
                            presentation, mRatingBarTracker, dataHelper);
                    agendaSection.addTimeSlot(agendaTimeSlot);
                }
            }
        }
        //Add the sections and timeSlot items to the list
        List<AgendaSection> sectionList = new ArrayList<>(agendaSections.values());
        Collections.sort(sectionList);  // Sort sections by date/time
        for (AgendaSection agendaSection : sectionList) {
            mAgendaListItems.add(agendaSection);
            List<AgendaTimeSlot> timeSlotSection = agendaSection.getTimeSlots();
            Collections.sort(timeSlotSection);  // Sort timeSlots by date/time
            for (AgendaTimeSlot timeSlot : timeSlotSection) {
                mAgendaListItems.add(timeSlot);
            }
        }
        mAdapter.notifyDataSetChanged();
    }
}