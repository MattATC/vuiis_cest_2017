package vanderbilt.vuiis_cest_2017;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import vanderbilt.vuiis_cest_2017.Data.DatabaseHelper;
import vanderbilt.vuiis_cest_2017.Data.News.NewsArticle;
import vanderbilt.vuiis_cest_2017.Data.News.NewsItem;
import vanderbilt.vuiis_cest_2017.Data.News.NewsItemAdapter;
import vanderbilt.vuiis_cest_2017.Data.News.NewsListItem;
import vanderbilt.vuiis_cest_2017.Data.News.NewsManager;

/**
 * Created by Matthew Christian on 3/30/17.
 */
public class NewsActivity extends AppCompatActivity implements AbsListView.OnItemClickListener {
    static public int SHOW_NEWS = 0;

    private List<NewsListItem> mNewsItems = new ArrayList<>();
    private NewsItemAdapter mNewsItemAdapter;
    private DatabaseHelper mDataHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_news);
        ListView listView = (ListView)findViewById(R.id.list);
        updateNewsItems();
        mNewsItemAdapter = new NewsItemAdapter(getApplicationContext(), mNewsItems);
        listView.setAdapter(mNewsItemAdapter);
        listView.setOnItemClickListener(this);

        mDataHelper = new DatabaseHelper(this);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == android.R.id.home) {
            saveAndClose();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        saveAndClose();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        NewsItem newsItem = (NewsItem)mNewsItems.get(i);
        newsItem.markRead(mDataHelper);
        mNewsItemAdapter.notifyDataSetChanged();
    }

    private void updateNewsItems() {
        Date currentTime = new Date();
        //Only add news items when the current time is between the start and end times
        mNewsItems.clear();
        for(NewsArticle article: NewsManager.getArticles()) {
            if(!currentTime.before(article.getStartTime()) &&
                    !currentTime.after(article.getEndTime())) {
                NewsItem newsItem = new NewsItem(article);
                mNewsItems.add(newsItem);
            }
        }
        Collections.sort(mNewsItems);
    }

    private void saveAndClose() {
        Intent intent = getIntent();
        intent.putExtra("items_unread", NewsManager.numberOfUnReadNews());
        setResult(Activity.RESULT_OK, intent);
        finish();
    }
}
