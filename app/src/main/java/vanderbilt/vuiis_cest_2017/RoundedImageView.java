package vanderbilt.vuiis_cest_2017;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.ViewTreeObserver;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Matthew Christian on 5/9/17.
 */

public class RoundedImageView extends AppCompatImageView
    implements ViewTreeObserver.OnPreDrawListener {
    static private Paint mShapePaint;
    static private Paint mDrawablePaint;
    static private Map<String, Bitmap> mDrawableCache = new HashMap<>();
    static private int IMAGE_CACHE_LIMIT = 16;

    private int mRadius;
    private int mWidth;
    private int mHeight;
    private int mCenterX;
    private int mCenterY;
    private String mDrawKey;
    private Bitmap mDrawableBitmap;
    private Bitmap mBitmap;
    private Canvas mCanvas;
    private Rect mRect = new Rect(0, 0, 1, 1);

    static {
        mShapePaint = new Paint();
        mShapePaint.setAntiAlias(true);
        mShapePaint.setFilterBitmap(true);
        mShapePaint.setDither(true);
        mShapePaint.setColor(Color.BLACK);
        mDrawablePaint = new Paint(mShapePaint);
        mDrawablePaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
    }

    public RoundedImageView(Context context) {
        super(context);
        init(context, null, 0);
    }

    public RoundedImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public RoundedImageView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs, defStyle);
    }

    private void init(Context context, @Nullable AttributeSet attrs, int defStyle) {
        setWillNotDraw(false);
        mWidth = 1;
        mHeight = 1;
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        getViewTreeObserver().addOnPreDrawListener(this);
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        getViewTreeObserver().removeOnPreDrawListener(this);
    }

    public void setImageDrawable(String name, Drawable drawable) {
        setImageDrawable(drawable);
        //Create a key so we can retrieve the rounded image from a cache later
        mDrawKey = String.format("%s %s %s", name, mWidth, mHeight);
        updateRoundedImage();
    }

    @Override
    public boolean onPreDraw() {
        int width = Math.max(getWidth(), 1);
        int height = Math.max(getHeight(), 1);
        if((width != mWidth) || (height != mHeight)) {
            //Create another blank bitmap for drawing
            mWidth = width;
            mHeight = height;
            mCenterX = width / 2;
            mCenterY = height / 2;
            mRadius = mCenterX;
            mRect = new Rect(0, 0, width, height);
            mBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            mCanvas = new Canvas(mBitmap);
            updateRoundedImage();
        }
        return true;
    }

    private void updateRoundedImage() {
        if(mDrawKey == null || mWidth == 1 || mHeight == 1 || getDrawable() == null) {
            return;
        }
        //The rounded image is retrieved from a cache, else generated on the fly and saved
        //Don't allow more than a certain number of images in the cache
        if(mDrawableCache.size() > IMAGE_CACHE_LIMIT) {
            Log.d("RoundedImageView", "cleared the cache");
            mDrawableCache.clear();
        }
        if(mDrawableCache.containsKey(mDrawKey)) {
            mDrawableBitmap = mDrawableCache.get(mDrawKey);
            //Log.d("RoundedImageView", "retrieved bitmap for " + this);
        }
        else {
            Bitmap bitmap = ((BitmapDrawable) getDrawable()).getBitmap();
            Bitmap bitmapCopy = bitmap.copy(Bitmap.Config.ARGB_8888, true);
            int bitmapWidth = Math.max(bitmapCopy.getWidth(), 1);
            int bitmapHeight = Math.max(bitmapCopy.getHeight(), 1);
            if (mWidth > 0 && ((bitmapWidth != mWidth) || (bitmapHeight != mHeight))) {
                float factor = Math.min(bitmapWidth, bitmapHeight) / (float)mWidth;
                //Log.d("RoundedImageView", hashCode + " Radius = " + mRadius + " factor = " + factor +
                //        " diameter " + width);
                mDrawableBitmap = Bitmap.createScaledBitmap(bitmapCopy, (int)(bitmapWidth / factor),
                        (int)(bitmapHeight / factor), false);
            }
            else {
                mDrawableBitmap = bitmapCopy;
            }
            mDrawableCache.put(mDrawKey, mDrawableBitmap);
            //Log.d("RoundedImageView", "create new bitmap for " + this + " cache size " +
            //        mDrawableCache.size());
        }
    }

    @Override
    public void onDraw(Canvas canvas) {
        //We first draw a circle, and then use that as a mask to draw the image
        if(mDrawableBitmap != null && mCanvas != null) {
            mCanvas.drawARGB(0, 0, 0, 0);
            mCanvas.drawCircle(mCenterX, mCenterY, mRadius, mShapePaint);
            mCanvas.drawBitmap(mDrawableBitmap, mRect, mRect, mDrawablePaint);
            canvas.drawBitmap(mBitmap, 0, 0, null);
        }
    }
}
