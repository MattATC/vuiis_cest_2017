package vanderbilt.vuiis_cest_2017.Data.Agenda;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;

import java.util.Date;

import vanderbilt.vuiis_cest_2017.Data.CustomListItem;

/**
 * Created by Matthew Christian on 3/3/17.
 */

public abstract class AgendaListItem implements CustomListItem, Comparable<AgendaListItem> {
    public enum RowType {
        SECTION_ITEM, TIMESLOT_ITEM
    }

    @Override
    public abstract int getViewType();

    @Override
    public abstract View getView(LayoutInflater inflater, View view);

    @Override
    public abstract boolean isEnabled();

    @Override
    public int compareTo(@NonNull AgendaListItem other) {
        return getTime().compareTo(other.getTime());
    }

    public abstract Date getTime();
}
