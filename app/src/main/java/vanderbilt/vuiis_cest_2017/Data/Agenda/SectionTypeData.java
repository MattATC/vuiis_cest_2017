package vanderbilt.vuiis_cest_2017.Data.Agenda;

/**
 * Created by Matthew Christian on 3/16/17.
 */

public class SectionTypeData {
    private int mID;
    private String mTitle;

    static public final String TYPE_ORAL_PRESENTATION = "Oral Session";
    static public final String TYPE_POSTER = "Poster Session";
    static public final String TYPE_POWER_PITCH = "Power Pitches";
    static public final String TYPE_LUNCH = "Lunch";
    static public final String TYPE_RECEPTION = "Reception";
    static public final String TYPE_CHECKIN = "Check-in";
    static public final String TYPE_WELCOME = "Welcome";

    public SectionTypeData(int id, String title) {
        mID = id;
        mTitle = title;
    }

    public int getID() { return mID; }
    public String getTitle() { return mTitle; }
}
