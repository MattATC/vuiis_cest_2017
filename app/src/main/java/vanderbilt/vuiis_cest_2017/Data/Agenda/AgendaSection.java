package vanderbilt.vuiis_cest_2017.Data.Agenda;

import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import vanderbilt.vuiis_cest_2017.R;

/**
 * Created by Matthew Christian on 3/3/17.
 */

public class AgendaSection extends AgendaListItem
{
    private SectionData mSection;
    private List<AgendaTimeSlot> mTimeSlots = new ArrayList<>();

    public AgendaSection(SectionData section) {
        mSection = section;
    }

    public SectionData getSection() { return mSection; }

    public void addTimeSlot(AgendaTimeSlot timeSlot) {
        mTimeSlots.add(timeSlot);
    }

    public Date getTime() { return mSection.getStartTime(); }

    public List<AgendaTimeSlot> getTimeSlots() {
        return mTimeSlots;
    }

    @Override
    public int getViewType() {
        return AgendaListItem.RowType.SECTION_ITEM.ordinal();
    }

    @Override
    public View getView(LayoutInflater inflater, View convertView) {
        View view;
        if(convertView == null) {
            view = inflater.inflate(R.layout.agenda_section, null);
        }
        else {
            view = convertView;
        }
        RelativeLayout frContent = (RelativeLayout)view.findViewById(R.id.frame_content);
        TextView tvHeader = (TextView)view.findViewById(R.id.label_section_header);
        TextView tvSubHeader = (TextView)view.findViewById(R.id.label_section_subheader);
        TextView tvDate = (TextView)view.findViewById(R.id.label_section_date);
        TextView tvTime = (TextView)view.findViewById(R.id.label_section_time);

        int backgroundColor = R.color.section_oral_presentation;
        SectionTypeData sectionType = AgendaDataSource.getSectionType(mSection.getSectionTypeID());
        /*switch(sectionType.getTitle()) {
            case SectionTypeData.TYPE_ORAL_PRESENTATION:
                backgroundColor = R.color.section_oral_presentation;
                break;
            case SectionTypeData.TYPE_POSTER:
                backgroundColor = R.color.section_poster;
                break;
            case SectionTypeData.TYPE_POWER_PITCH:
                backgroundColor = R.color.section_power_pitch;
                break;
            case SectionTypeData.TYPE_BREAK:
            case SectionTypeData.TYPE_LUNCH:
            case SectionTypeData.TYPE_RECEPTION:
                backgroundColor = R.color.section_break;
                break;
            case SectionTypeData.TYPE_CHECKIN:
                backgroundColor = R.color.section_checkin;
                break;
        }*/
        frContent.setBackground(ContextCompat.getDrawable(inflater.getContext(), backgroundColor));

        List<String> moderators = new ArrayList<>();
        for(ModeratorData moderator: mSection.getModerators()) {
            moderators.add(moderator.getName());
        }
        Date startDate = mSection.getStartTime();
        Date endDate = mSection.getEndTime();
        String dateFormat = new SimpleDateFormat("EEEE, MMMM d").format(startDate);
        String timeFormat = new SimpleDateFormat("h:mm a").format(startDate) + " - " +
                new SimpleDateFormat("h:mm a").format(endDate);
        tvHeader.setText(mSection.getTitle());
        if(!moderators.isEmpty()) {
            tvSubHeader.setVisibility(View.VISIBLE);
            tvSubHeader.setText("Moderated by " + TextUtils.join(", ", moderators));
        }
        else {
            tvSubHeader.setVisibility(View.GONE);
        }
        tvDate.setText(dateFormat);
        tvTime.setText(timeFormat);
        return view;
    }

    @Override
    public boolean isEnabled() {
        return false;
    }
}
