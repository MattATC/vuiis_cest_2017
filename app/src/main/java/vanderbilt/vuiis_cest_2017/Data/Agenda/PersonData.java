package vanderbilt.vuiis_cest_2017.Data.Agenda;

/**
 * Created by Matthew Christian on 3/8/17.
 */

public class PersonData {
    private int mID;
    private String mName;
    private String mEmail;
    private String mAffiliation;
    private String mPictureURL;
    private String mBio;
    private int mPresentationID;

    public PersonData(int id, String name, String email, String affiliation, String pictureURL,
                      String bio, int presentationID)
    {
        mID = id;
        mName = name;
        mEmail = email;
        mAffiliation = affiliation;
        mPictureURL = pictureURL;
        mBio = bio;
        mPresentationID = presentationID;
    }

    public int getID() { return mID; }
    public String getName() { return mName; }
    public String getEmail() { return mEmail; }
    public String getAffiliation() { return mAffiliation; }
    public String getPictureURL() { return mPictureURL; }
    public String getBio() { return mBio; }
    public int getPresentationID() { return mPresentationID; }

}
