package vanderbilt.vuiis_cest_2017.Data.Agenda;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

import vanderbilt.vuiis_cest_2017.Data.Abstract.AbstractManager;
import vanderbilt.vuiis_cest_2017.Data.DatabaseHelper;
import vanderbilt.vuiis_cest_2017.Data.Portrait.PortraitManager;
import vanderbilt.vuiis_cest_2017.Data.Rating.PresentationRating;
import vanderbilt.vuiis_cest_2017.Data.Rating.PresentationRatingManager;
import vanderbilt.vuiis_cest_2017.Data.Rating.RatingBarTrackerDelegate;
import vanderbilt.vuiis_cest_2017.EditCommentActivity;
import vanderbilt.vuiis_cest_2017.OnBoardingActivity;
import vanderbilt.vuiis_cest_2017.R;
import vanderbilt.vuiis_cest_2017.RoundedImageView;
import vanderbilt.vuiis_cest_2017.ThisApplication;

/**
 * Created by Matthew Christian on 3/27/17.
 */

public class AgendaDetailViewBuilder {
    /**
     * Everywhere a rating bar can appear, a unique tag is required to make sure the number of
     * stars can be updated. The tag is passed as the 'viewTag' argument.
     */
    public enum ViewTag{
        AGENDA,
        SPEAKERS,
        POSTERS,
        TIMESLOT
    }

    public static void buildDetailView(FrameLayout detailPanel, int detailPanelWidth,
                                       LayoutInflater layoutInflater,
                                       final PresentationData presentation, final PersonData person,
                                       RatingBarTrackerDelegate trackerDelegate, ViewTag viewTag,
                                       DatabaseHelper dataHelper, final Activity activity) {
        View rootView = layoutInflater.inflate(R.layout.agenda_detail, detailPanel, true);
        TextView tvTitle = (TextView) rootView.findViewById(R.id.tv_title);
        LinearLayout llAddCommentCntnr = (LinearLayout) rootView.findViewById(R.id.container_fab);
        ImageButton ibAddComment = (ImageButton) rootView.findViewById(R.id.button_add_comment);
        TextView tvTime = (TextView) rootView.findViewById(R.id.tv_time);
        TextView tvSpeaker = (TextView) rootView.findViewById(R.id.tv_speaker);
        //TextView tvSummary = (TextView) rootView.findViewById(R.id.tv_summary);
        final RoundedImageView ivImage = (RoundedImageView)rootView.findViewById(R.id.image_presentation);
        //RatingBar rbStars = (RatingBar) rootView.findViewById(R.id.rb_presentation);
        TextView tvTimeSlotType = (TextView) rootView.findViewById(R.id.label_timeslot_type);
        ImageView ivAbstractDetail = (ImageView) rootView.findViewById(R.id.iv_abstract_preview);
        View vAbstractButton = rootView.findViewById(R.id.abstract_button);

        final String title = presentation != null ? presentation.getTitle() : "[No Title]";
        Date presentationDate = presentation.getStartTime();
        final String timeAndDate = new SimpleDateFormat("h:mm a").format(presentationDate) +
                " on " + new SimpleDateFormat("MMMM d").format(presentationDate);
        if(title != null) {
            tvTitle.setText(title);
        }
        else {
            tvTitle.setVisibility(View.GONE);
        }

        //Change the layout height by the API level
        if(Build.VERSION.SDK_INT < 24) {
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams)
                    llAddCommentCntnr.getLayoutParams();
            layoutParams.height = FrameLayout.LayoutParams.WRAP_CONTENT;
            llAddCommentCntnr.setLayoutParams(layoutParams);
        }

        ibAddComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ThisApplication.isServerAPIKeySet()) {
                    EditCommentActivity.startActivityToSubmitPresentationComment(
                            activity, presentation.getID(), presentation.getTitle());
                }
                else {
                    OnBoardingActivity.startActivityForOnboarding(activity);
                }
            }
        });
        tvTime.setText(timeAndDate);
        if(person != null) {
            tvSpeaker.setText(person.getName());
            //Load the picture when it is available
            PortraitManager.requestPortrait(activity, person.getPictureURL(),
                    new PortraitManager.PortraitLoadObserver() {
                        @Override
                        public void onPortraitLoad(String pictureURL, Drawable portrait) {
                            ivImage.setImageDrawable(pictureURL, portrait);
                        }
                        @Override
                        public void onPortraitLoadFailed(String pictureURL) {
                            ivImage.setImageDrawable("",
                                    PortraitManager.getPlaceholderPortrait());
                        }
                    });
        }
        else {
            tvSpeaker.setVisibility(View.GONE);
        }
        SectionData section = AgendaDataSource.getSection(presentation.getSectionID());
        SectionTypeData sectionType = AgendaDataSource.getSectionType(section.getSectionTypeID());
        //PresentationRating rating;

        final String sectionTypeTitle = sectionType.getTitle();
        tvTimeSlotType.setText("");
        switch(sectionTypeTitle) {
            case SectionTypeData.TYPE_ORAL_PRESENTATION:
            case SectionTypeData.TYPE_POWER_PITCH:
                /*rbStars.setVisibility(View.VISIBLE);
                rating = PresentationRatingManager.getRating(presentation.getID());
                rbStars.setRating(rating != null ? rating.getStars() : 0);
                trackerDelegate.registerRatingBar(presentation, viewTag.ordinal(), rbStars,
                        dataHelper);*/
                tvTime.setVisibility(View.VISIBLE);
                tvTimeSlotType.setVisibility(View.VISIBLE);
                if (sectionTypeTitle.equals(SectionTypeData.TYPE_POWER_PITCH)) {
                    tvTimeSlotType.setText("power pitch!");
                }
            case SectionTypeData.TYPE_POSTER:
                tvTime.setVisibility(View.VISIBLE);
                tvTimeSlotType.setVisibility(View.GONE);
                break;
            default:
                tvTime.setVisibility(View.VISIBLE);
                //rbStars.setVisibility(View.GONE);
                tvTimeSlotType.setVisibility(View.GONE);
                break;
        }
        final String abstractURL = presentation.getAbstractURL();
        if(person != null) {
            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (ThisApplication.isServerAPIKeySet()) {
                        AbstractManager.openAbstract(activity, presentation.getAbstractURL());
                    }
                    else {
                        OnBoardingActivity.startActivityForOnboarding(activity);
                    }
                }
            };
            if(abstractURL != null && AbstractManager.isAbstractPreviewSupported()) {
                ivAbstractDetail.setVisibility(View.VISIBLE);
                ivAbstractDetail.setOnClickListener(clickListener);
                AbstractManager.previewAbstract(activity, presentation.getAbstractURL(),
                         ivAbstractDetail, detailPanelWidth);
                vAbstractButton.setVisibility(View.GONE);
            }
            else {
                vAbstractButton.setVisibility(View.VISIBLE);
                vAbstractButton.setOnClickListener(clickListener);
                ivAbstractDetail.setVisibility(View.GONE);
            }
            ivImage.setVisibility(View.VISIBLE);
        }
        else {
            tvTimeSlotType.setVisibility(View.GONE);
            ivImage.setVisibility(View.GONE);
            //rbStars.setVisibility(View.GONE);
        }
        if(abstractURL == null) {
            vAbstractButton.setVisibility(View.GONE);
            vAbstractButton.setOnClickListener(null);
            ivAbstractDetail.setVisibility(View.GONE);
            ivAbstractDetail.setOnClickListener(null);
        }

        //tvSummary.setText(presentation.getSummary());
    }
}
