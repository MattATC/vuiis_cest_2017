package vanderbilt.vuiis_cest_2017.Data.Agenda;

/**
 * Created by matthew on 3/16/17.
 */

public class ModeratorData {
    private int mID;
    private String mName;
    private String mEmail;
    private String mAffiliation;
    private int mSectionID;

    public ModeratorData(int id, String name, String email,
                         String affiliation, int sectionID) {
        mID = id;
        mName = name;
        mEmail = email;
        mAffiliation = affiliation;
        mSectionID = sectionID;
    }

    public int getID() { return mID; }
    public String getName() { return mName; }
    public String getEmail() { return mEmail; }
    public String getAffiliation() { return mAffiliation; }
    public int getSectionID() { return mSectionID; }
}
