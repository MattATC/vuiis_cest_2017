package vanderbilt.vuiis_cest_2017.Data.Agenda;

import android.content.Context;

import java.util.List;

import vanderbilt.vuiis_cest_2017.Data.CustomItemAdapter;

/**
 * Created by Matthew Christian on 3/27/17.
 */

public class SpeakerItemAdapter extends CustomItemAdapter<SpeakerListItem> {
    public SpeakerItemAdapter(Context context, List<SpeakerListItem> items) {
        super(context, android.R.layout.simple_list_item_1, items);
    }

    @Override
    public int getViewTypeCount() {
        return SpeakerListItem.RowType.values().length;
    }
}
