package vanderbilt.vuiis_cest_2017.Data.Agenda;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;

import vanderbilt.vuiis_cest_2017.Data.CustomListItem;

/**
 * Created by Matthew Christian on 3/27/17.
 */

public abstract class SpeakerListItem implements CustomListItem, Comparable<SpeakerListItem> {
    public enum RowType {
        SECTION_ITEM, PERSON_ITEM
    }

    @Override
    public abstract int getViewType();

    @Override
    public abstract View getView(LayoutInflater inflater, View view);

    @Override
    public abstract boolean isEnabled();

    @Override
    public int compareTo(@NonNull SpeakerListItem other) {
        return getName().compareTo(other.getName());
    }

    public abstract String getName();
}
