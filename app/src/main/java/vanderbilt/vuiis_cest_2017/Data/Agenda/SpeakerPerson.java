package vanderbilt.vuiis_cest_2017.Data.Agenda;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import vanderbilt.vuiis_cest_2017.Data.Portrait.PortraitManager;
import vanderbilt.vuiis_cest_2017.R;
import vanderbilt.vuiis_cest_2017.RoundedImageView;

/**
 * Created by matthew on 3/27/17.
 */

public class SpeakerPerson extends  SpeakerListItem {
    private PersonData mPerson;

    public SpeakerPerson(PersonData person) {
        mPerson = person;
    }

    public PersonData getPerson() {
        return mPerson;
    }

    @Override
    public int getViewType() {
        return RowType.PERSON_ITEM.ordinal();
    }

    @Override
    public View getView(LayoutInflater inflater, View convertView) {
        View view;
        if(convertView == null) {
            view = inflater.inflate(R.layout.speaker_person, null);
            view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
        else {
            view = convertView;
        }
        if (mPerson != null) {
            final Context context = inflater.getContext();
            final RoundedImageView ivSpeaker = (RoundedImageView)
                    view.findViewById(R.id.image_speaker);
            //Load the picture when it is available
            PortraitManager.requestPortrait(context, mPerson.getPictureURL(),
                    new PortraitManager.PortraitLoadObserver() {
                        @Override
                        public void onPortraitLoad(String pictureURL, Drawable portrait) {
                            ivSpeaker.setImageDrawable(pictureURL, portrait);
                        }
                        @Override
                        public void onPortraitLoadFailed(String pictureURL) {
                            ivSpeaker.setImageDrawable("",
                                    PortraitManager.getPlaceholderPortrait());
                        }
                    });
            TextView tvSpeaker = (TextView) view.findViewById(R.id.label_speaker_name);
            tvSpeaker.setText(mPerson.getName());
            TextView tvAffiliation = (TextView)view.findViewById(R.id.label_affiliation);
            tvAffiliation.setText(mPerson.getAffiliation());
            PresentationData presentation = AgendaDataSource.getPresentation(
                    mPerson.getPresentationID());
            TextView tvTitle = (TextView)view.findViewById(R.id.label_presentation_title);
            tvTitle.setText(presentation.getTitle());
        }
        return view;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public String getName() {
        return mPerson.getName();
    }
}
