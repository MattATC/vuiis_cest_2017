package vanderbilt.vuiis_cest_2017.Data.Agenda;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

import vanderbilt.vuiis_cest_2017.Data.DatabaseHelper;
import vanderbilt.vuiis_cest_2017.Data.Portrait.PortraitManager;
import vanderbilt.vuiis_cest_2017.Data.Rating.PresentationRating;
import vanderbilt.vuiis_cest_2017.Data.Rating.PresentationRatingManager;
import vanderbilt.vuiis_cest_2017.Data.Rating.RatingBarTrackerDelegate;
import vanderbilt.vuiis_cest_2017.R;
import vanderbilt.vuiis_cest_2017.RoundedImageView;

/**
 * Created by Matthew Christian on 3/3/17.
 */

public class AgendaTimeSlot extends AgendaListItem {


    private Activity mActivity;
    private AgendaSection mSection;
    private PresentationData mPresentation;
    private RatingBarTrackerDelegate mTrackerDelegate;
    private DatabaseHelper mDataHelper;

    public AgendaTimeSlot(Activity activity, AgendaSection section, PresentationData presentation,
                          RatingBarTrackerDelegate trackerDelegate, DatabaseHelper dataHelper) {
        mActivity = activity;
        mSection = section;
        mPresentation = presentation;
        mTrackerDelegate = trackerDelegate;
        mDataHelper = dataHelper;
    }

    public AgendaSection getSection() {
        return mSection;
    }

    public PresentationData getPresentation() {
        return mPresentation;
    }

    @Override
    public Date getTime() {
        return mPresentation.getStartTime();
    }

    @Override
    public int getViewType() {
        return RowType.TIMESLOT_ITEM.ordinal();
    }

    @Override
    public View getView(LayoutInflater inflater, View convertView) {
        View view;
        if(convertView == null) {
            view = inflater.inflate(R.layout.agenda_timeslot, null);
        }
        else {
            view = convertView;
        }
        if(mPresentation != null) {
            TextView tvDate = (TextView) view.findViewById(R.id.label_agenda_date);
            TextView tvSpeaker = (TextView) view.findViewById(R.id.label_agenda_speaker);
            TextView tvTitle = (TextView) view.findViewById(R.id.label_agenda_title);
            final RoundedImageView ivPerson = (RoundedImageView)
                    view.findViewById(R.id.image_presentation);
            //RatingBar rbStars = (RatingBar) view.findViewById(R.id.rating_presentation);
            TextView tvTimeSlotType = (TextView) view.findViewById(R.id.label_timeslot_type);
            final String dateString = new SimpleDateFormat("h:mm a").format(
                    mPresentation.getStartTime());
            tvDate.setText(dateString);
            tvTitle.setText(mPresentation.getTitle());
            PersonData person = AgendaDataSource.getPerson(mPresentation.getPersonID());
            if(person != null) {
                ivPerson.setVisibility(View.VISIBLE);
                tvSpeaker.setText(person.getName());
                //Load the picture when it is available
                PortraitManager.requestPortrait(mActivity, person.getPictureURL(),
                        new PortraitManager.PortraitLoadObserver() {
                            @Override
                            public void onPortraitLoad(String pictureURL, Drawable portrait) {
                                ivPerson.setImageDrawable(pictureURL, portrait);
                            }
                            @Override
                            public void onPortraitLoadFailed(String pictureURL) {
                                ivPerson.setImageDrawable("",
                                        PortraitManager.getPlaceholderPortrait());
                            }
                        });
            }
            else {
                ivPerson.setVisibility(View.GONE);
                tvSpeaker.setText("");
            }

            SectionData section = AgendaDataSource.getSection(mPresentation.getSectionID());
            SectionTypeData sectionType = AgendaDataSource.getSectionType(
                    section.getSectionTypeID());
            final String sectionTypeTitle = sectionType.getTitle();
            //PresentationRating rating;
            tvTimeSlotType.setText("");
            if(sectionTypeTitle.equals(SectionTypeData.TYPE_POWER_PITCH) ||
                    sectionTypeTitle.equals(SectionTypeData.TYPE_ORAL_PRESENTATION)) {
                /*rbStars.setVisibility(View.VISIBLE);
                rating = PresentationRatingManager.getRating(mPresentation.getID());
                rbStars.setRating(rating != null ? rating.getStars() : 0);
                mTrackerDelegate.registerRatingBar(mPresentation,
                    AgendaDetailViewBuilder.ViewTag.TIMESLOT.ordinal(), rbStars, mDataHelper);*/
                tvDate.setVisibility(View.VISIBLE);
                tvTimeSlotType.setVisibility(View.VISIBLE);
                if (sectionTypeTitle.equals(SectionTypeData.TYPE_POWER_PITCH)) {
                    tvTimeSlotType.setText("power pitch!");
                }
            }
            else if(sectionTypeTitle.equals(SectionTypeData.TYPE_POSTER)) {
                tvDate.setVisibility(View.GONE);
                tvTimeSlotType.setVisibility(View.GONE);
            }
            else {
                tvDate.setVisibility(View.VISIBLE);
                //rbStars.setVisibility(View.GONE);
                tvTimeSlotType.setVisibility(View.GONE);
            }
            if(person == null) {
                //rbStars.setVisibility(View.GONE);
                tvTimeSlotType.setVisibility(View.GONE);
            }
        }
        return view;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
