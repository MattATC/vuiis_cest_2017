package vanderbilt.vuiis_cest_2017.Data.Agenda;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Matthew Christian on 3/8/17.
 */

public class SectionData {
    private int mID;
    private String mTitle;
    private Date mStartTime;
    private Date mEndTime;
    private int mSectionTypeID;
    List<ModeratorData> mModerators;
    List<PresentationData> mPresentations;

    public SectionData(int id, String title, Date startTime, Date endTime, int sectionTypeID,
                       List<ModeratorData> moderators, List<PresentationData> presentations) {
        mID = id;
        mTitle = title;
        mStartTime = startTime;
        mEndTime = endTime;
        mSectionTypeID = sectionTypeID;
        mModerators = moderators;
        mPresentations = presentations;
    }

    public int getID() { return mID; }
    public String getTitle() { return mTitle; }
    public Date getStartTime() { return mStartTime; }
    public Date getEndTime() { return mEndTime; }
    public int getSectionTypeID() { return mSectionTypeID; }

    public String getStartTimeDateString() {
        return new SimpleDateFormat("EEE, MMM dd").format(mStartTime);
    }

    public String getStartTimeString() {
        return new SimpleDateFormat("h:mm a").format(mStartTime);
    }

    public String getEndTimeDateString() {
        return new SimpleDateFormat("EEE, MMM dd").format(mEndTime);
    }

    public String getEndTimeString() {
        return new SimpleDateFormat("h:mm a").format(mEndTime);
    }

    public List<ModeratorData> getModerators() { return mModerators; }
    public List<PresentationData> getPresentations() { return mPresentations; }
}
