package vanderbilt.vuiis_cest_2017.Data.Agenda;

import android.content.Context;

import java.util.List;

import vanderbilt.vuiis_cest_2017.Data.CustomItemAdapter;

/**
 * Created by Matthew Christian on 3/3/17.
 */

public class AgendaItemAdapter extends CustomItemAdapter<AgendaListItem> {

    public AgendaItemAdapter(Context context, List<AgendaListItem> items) {
        super(context, android.R.layout.simple_list_item_1, items);
    }

    @Override
    public int getViewTypeCount() {
        return AgendaListItem.RowType.values().length;
    }
}
