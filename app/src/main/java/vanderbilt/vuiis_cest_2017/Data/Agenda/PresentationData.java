package vanderbilt.vuiis_cest_2017.Data.Agenda;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Matthew Christian on 3/8/17.
 */

public class PresentationData {
    private int mID;
    private String mTitle;
    private String mAbstractURL;
    private Date mStartTime;
    private Date mEndTime;
    private int mLengthInMinutes;
    private String mSummary;
    private int mPersonID;
    private int mSectionID;

    public PresentationData(int id, String title, String abstractURL, Date startTime,
                            int lengthInMinutes, String summary, int personID, int sectionID)
    {
        mID = id;
        mTitle = title;
        mAbstractURL = abstractURL;
        mStartTime = startTime;
        mLengthInMinutes = lengthInMinutes;
        mSummary = summary;
        mPersonID = personID;
        mSectionID = sectionID;
        //Calculate the end time
        Calendar cal = Calendar.getInstance();
        cal.setTime(mStartTime);
        cal.add(Calendar.MINUTE, mLengthInMinutes);
        mEndTime = cal.getTime();
    }

    public int getID() { return mID; }
    public String getTitle() { return mTitle; }
    public String getAbstractURL() {
        return mAbstractURL;
    }
    public Date getStartTime() { return mStartTime; }
    public int getLengthInMinutes() { return mLengthInMinutes; }
    public String getSummary() {
        return mSummary;
    }
    public int getPersonID() {
        return mPersonID;
    }
    public int getSectionID() { return mSectionID; }

    public String getStartTimeDateString() {
        return new SimpleDateFormat("EEE, MMM dd").format(mStartTime);
    }

    public String getStartTimeString() {
        return new SimpleDateFormat("h:mm a").format(mStartTime);
    }

    public String getEndTimeDateString() {
        return new SimpleDateFormat("EEE, MMM dd").format(mEndTime);
    }

    public String getEndTimeString() {
        return new SimpleDateFormat("h:mm a").format(mEndTime);
    }
}
