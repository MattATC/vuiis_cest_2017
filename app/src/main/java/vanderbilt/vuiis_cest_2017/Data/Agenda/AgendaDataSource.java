package vanderbilt.vuiis_cest_2017.Data.Agenda;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import vanderbilt.vuiis_cest_2017.Data.DatabaseHelper;
import vanderbilt.vuiis_cest_2017.Data.DatabaseMetaData;
import vanderbilt.vuiis_cest_2017.DataManager.DataUpdateInfo;

/**
 * Created by Matthew Christian on 3/8/17.
 */

public class AgendaDataSource {
    private static final String[] AGENDA_TABLES = { "Presentation", "Person", "Section",
        "SectionType", "Moderator"};
    private static final Set<String> mTables = new HashSet<>(Arrays.asList(AGENDA_TABLES));

    private static Map<Integer, SectionData> mSections = new HashMap<>();
    private static Map<Integer, PersonData> mPeople = new HashMap<>();
    private static Map<Integer, ModeratorData> mModerators = new HashMap<>();
    private static Map<Integer, PresentationData> mPresentations = new HashMap<>();
    private static Map<Integer, SectionTypeData> mSectionTypes = new HashMap<>();

    public static Map<Integer, SectionData> getSections() {
        return Collections.unmodifiableMap(mSections);
    }

    public static SectionData getSection(int sectionID) {
        return mSections.get(sectionID);
    }

    public static Map<Integer, PresentationData> getPresentations() {
        return Collections.unmodifiableMap(mPresentations);
    }

    public static PresentationData getPresentation(int presentationID) {
        return mPresentations.get(presentationID);
    }

    public static Map<Integer, PersonData> getPeople() {
        return Collections.unmodifiableMap(mPeople);
    }

    public static PersonData getPerson(int personID) {
        return mPeople.get(personID);
    }

    public static SectionTypeData getSectionType(int sectionTypeID) {
        return mSectionTypes.get(sectionTypeID);
    }

    public static Set<String> getTables() {
        return mTables;
    }

    public static void updateAgenda(DatabaseHelper dataHelper, DataUpdateInfo dataUpdateInfo) {
        SQLiteDatabase db = dataHelper.getReadableDatabase();
        String query, query2;
        Cursor cursor, cursor2;
        Map<Integer, List<ModeratorData>> sectionModeratorMap = new HashMap<>();
        Map<Integer, List<PresentationData>> sectionPresentationMap = new HashMap<>();
        List<ModeratorData> sectionModerators;
        List<PresentationData> sectionPresentations;

        Set<String> tablesAffected = dataUpdateInfo.getTablesAffected();
        boolean personAffected = tablesAffected.contains(DatabaseMetaData.TABLE_PERSON);
        boolean sectionTypeAffected = tablesAffected.contains(DatabaseMetaData.TABLE_SECTION_TYPE);
        boolean sectionAffected = sectionTypeAffected ||
                tablesAffected.contains(DatabaseMetaData.TABLE_SECTION);
        boolean moderatorAffected = sectionAffected ||
                tablesAffected.contains(DatabaseMetaData.TABLE_MODERATOR);
        boolean presentationAffected = personAffected || sectionAffected ||
                tablesAffected.contains(DatabaseMetaData.TABLE_PRESENTATION);

        if(moderatorAffected) { // Read all moderators
            mModerators.clear();
            query = "select id, name, email, affiliation, sectionID from Moderator order by id";
            cursor = db.rawQuery(query, null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                final int modID = cursor.getInt(0);
                final String modName = cursor.getString(1);
                final String modEmail = cursor.getString(2);
                final String modAffiliation = cursor.getString(3);
                final int sectionID = cursor.getInt(4);
                ModeratorData moderator = new ModeratorData(modID, modName, modEmail,
                        modAffiliation, sectionID);
                mModerators.put(modID, moderator);
                sectionModerators = sectionModeratorMap.get(sectionID);
                if (sectionModerators == null) {
                    sectionModerators = new ArrayList<>();
                    sectionModeratorMap.put(sectionID, sectionModerators);
                }
                sectionModerators.add(moderator);
                cursor.moveToNext();
            }
            cursor.close();
        }

        if(presentationAffected) { // Read all presentations
            query = "select p.id, p.title, p.abstractURL, p.startTime, p.lengthInMinutes, " +
                    "p.summary, p.personID, p.sectionID from Presentation p " +
                    "left join PresentationRating pr on p.id = pr.id order by p.id";
            cursor = db.rawQuery(query, null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                final int presID = cursor.getInt(0);
                final String presTitle = cursor.getString(1);
                final String presAbstractURL = cursor.getString(2);
                final String presStartTime = cursor.getString(3);
                final int presLengthInMinutes = cursor.getInt(4);
                final String presSummary = cursor.getString(5);
                final int presPersonID = cursor.getInt(6);
                final int presSectionID = cursor.getInt(7);
                try {
                    PresentationData presentation = new PresentationData(presID, presTitle,
                            presAbstractURL, DatabaseHelper.convertTimestampToDate(presStartTime),
                            presLengthInMinutes, presSummary, presPersonID,
                            presSectionID);
                    mPresentations.put(presID, presentation);
                    sectionPresentations = sectionPresentationMap.get(presSectionID);
                    if (sectionPresentations == null) {
                        sectionPresentations = new ArrayList<>();
                        sectionPresentationMap.put(presSectionID, sectionPresentations);
                    }
                    sectionPresentations.add(presentation);
                }
                catch (ParseException e) {
                    e.printStackTrace();
                }
                cursor.moveToNext();
            }
            cursor.close();
        }

        if(personAffected) { // Read all people
            //query = "select s.id, s.name, s.email, s.affiliation, s.pictureURL, s.bio, p.id " +
            //        "from Person s join Presentation p on p.personID == s.id order by s.id, p.id desc";
            query = "select s.id, s.name, s.email, s.affiliation, s.pictureURL, s.bio, p.id as " +
                    "presID, st.id as secTypeID from Person s join Presentation p on " +
                    "p.personID=s.id join Section sec on p.sectionID=sec.id join SectionType st " +
                    "on st.id=sec.sectionTypeID where st.id!=6 order by s.id;";
            cursor = db.rawQuery(query, null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                final int personID = cursor.getInt(0);
                final String personName = cursor.getString(1);
                final String personEmail = cursor.getString(2);
                final String personAffiliation = cursor.getString(3);
                final String personPictureURL = cursor.getString(4);
                final String personBio = cursor.isNull(5) ? "" : cursor.getString(5);
                final int perPresentationID = cursor.getInt(6);
                PersonData person = new PersonData(personID, personName, personEmail,
                        personAffiliation, personPictureURL, personBio, perPresentationID);
                mPeople.put(personID, person);
                cursor.moveToNext();
            }
            cursor.close();
        }

        if(sectionAffected) { // Read all sections
            query = "select s.id, s.title, s.startTime, s.endTime, st.id from Section s " +
                    "join SectionType st on s.sectionTypeID == st.id order by s.id";
            cursor = db.rawQuery(query, null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                final int sectionID = cursor.getInt(0);
                final String sectionTitle = cursor.getString(1);
                final String sectionStartTime = cursor.getString(2);
                final String sectionEndTime = cursor.getString(3);
                final int sectionTypeID = cursor.getInt(4);
                try {
                    sectionModerators = sectionModeratorMap.containsKey(sectionID) ?
                            sectionModeratorMap.get(sectionID) : new ArrayList<ModeratorData>();
                    sectionPresentations = sectionPresentationMap.containsKey(sectionID) ?
                            sectionPresentationMap.get(sectionID) : new ArrayList<PresentationData>();
                    SectionData section = new SectionData(sectionID, sectionTitle,
                            DatabaseHelper.convertTimestampToDate(sectionStartTime),
                            DatabaseHelper.convertTimestampToDate(sectionEndTime),
                            sectionTypeID, sectionModerators, sectionPresentations);
                    mSections.put(sectionID, section);
                    SectionTypeData sectionType = mSectionTypes.get(sectionTypeID);
                    if (sectionType == null) {
                        query2 = "select title from SectionType st where id == " + sectionTypeID;
                        cursor2 = db.rawQuery(query2, null);
                        cursor2.moveToFirst();
                        final String sectionTypeTitle = cursor2.getString(0);
                        sectionType = new SectionTypeData(sectionTypeID, sectionTypeTitle);
                        Log.d("AgendaDataSource", "Added section type " + sectionTypeTitle);
                        mSectionTypes.put(sectionTypeID, sectionType);
                        cursor2.close();
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                cursor.moveToNext();
            }
            cursor.close();
        }

        db.close();
    }
}
