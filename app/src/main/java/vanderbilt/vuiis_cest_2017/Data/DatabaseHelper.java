package vanderbilt.vuiis_cest_2017.Data;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.AssetManager;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Matthew Christian on 3/8/17.
 */

public class DatabaseHelper extends SQLiteOpenHelper {
    public static DateFormat mDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    static {
        mDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    public DatabaseHelper(Activity activity) {
        super(activity, DatabaseMetaData.DATABASE_NAME, null, DatabaseMetaData.DATABASE_VERSION);
    }

    public static Date convertTimestampToDate(String timestamp) throws java.text.ParseException {
        return mDateFormat.parse(timestamp);
    }

    public static String convertDateToTimestamp(Date date) {
        return mDateFormat.format(date);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d("onCreate", "DatabaseHelper called");
        db.execSQL("create table Person (id integer primary key autoincrement, " +
                "timeCreated string not null, timeModified string not null, " +
                "name string not null, email string null, affiliation string null, " +
                "pictureURL string null, bio string null)");
        db.execSQL("create table SectionType (id integer primary key autoincrement, " +
                "timeCreated string not null, timeModified string not null, " +
                "title string not null)");
        db.execSQL("create table Section (id integer primary key autoincrement, " +
                "timeCreated string not null, timeModified string not null, " +
                "title string null, startTime string not null, endTime string null, " +
                "sectionTypeID int not null, " +
                "foreign key(sectionTypeID) references SectionType(id) on delete set null)");
        db.execSQL("create table Moderator (id integer primary key autoincrement, " +
                "timeCreated string not null, timeModified string not null, " +
                "name string null, email string null, affiliation string null, " +
                "sectionID int not null, " +
                "foreign key(sectionID) references Section(id) on delete set null)");
        db.execSQL("create table Presentation (id integer primary key autoincrement, " +
                "timeCreated string not null, timeModified string not null, " +
                "title string not null, abstractURL string null, " +
                "startTime string not null, lengthInMinutes int null, " +
                "summary string null, stars int null, " +
                "personID int null, sectionID int not null, " +
                "foreign key(personID) references Person(id) on delete set null, " +
                "foreign key(sectionID) references Section(id) on delete set null)");
        db.execSQL("create table PresentationRating (id integer primary key autoincrement, " +
                "timeModified string not null, stars int not null, unique(id), " +
                "foreign key(id) references Presentation(id) on delete cascade)");
        db.execSQL("create table News (id integer primary key autoincrement, " +
                "timeCreated string not null, timeModified string not null, " +
                "title string not null, startTime string not null, endTime string null, " +
                "content string not null, isRead int not null default 0)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d("onUpgrade", "called oldVersion = " + oldVersion + " newVersion = " + newVersion);
        db.execSQL("drop table if exists Person");
        db.execSQL("drop table if exists SectionType");
        db.execSQL("drop table if exists Section");
        db.execSQL("drop table if exists Moderator");
        db.execSQL("drop table if exists Presentation");
        db.execSQL("drop table if exists News");
        db.execSQL("drop table if exists PresentationRating");
        onCreate(db);
    }

    /**
     * Copies the assets database
     */
    static public void copyAppDatabaseAsset(Context context) {
        try {
            ContextWrapper contextWrapped = new ContextWrapper(context);
            AssetManager assetManager = contextWrapped.getAssets();
            String databaseDir = contextWrapped.getApplicationInfo().dataDir;
            File dbFile = new File(databaseDir + "//databases//",
                    DatabaseMetaData.DATABASE_NAME);
            InputStream inStream = assetManager.open(DatabaseMetaData.DATABASE_NAME);
            OutputStream outStream = new FileOutputStream(dbFile.getAbsolutePath());
            byte[] buffer = new byte[4096];
            while (inStream.read(buffer) != -1) {
                outStream.write(buffer);
            }
            inStream.close();
            outStream.flush();
            outStream.close();
            Log.d("copyAppDB", "Success");
        }
        catch (IOException e) {
            //e.printStackTrace();
            Log.e("copyAppDB", "Failed " + e.getMessage());
        }
    }

}
