package vanderbilt.vuiis_cest_2017.Data;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.List;

/**
 * Created by matthew on 3/3/17.
 */

public abstract class CustomItemAdapter<T extends CustomListItem> extends ArrayAdapter<T>
{
    protected LayoutInflater mInflater;

    public CustomItemAdapter(Context context, int resource, List<T> items) {
        super(context, resource, items);
        mInflater = LayoutInflater.from(context);
    }

    public abstract int getViewTypeCount();

    @Override
    final public int getItemViewType(int position) {
        return getItem(position).getViewType();
    }

    @Override
    final public View getView(int position, View view, ViewGroup parent) {
        return getItem(position).getView(mInflater, view);
    }

    @Override
    final public boolean isEnabled(int position) {
        return getItem(position).isEnabled();
    }
}
