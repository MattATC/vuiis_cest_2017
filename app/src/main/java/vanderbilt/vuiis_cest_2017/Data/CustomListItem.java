package vanderbilt.vuiis_cest_2017.Data;

import android.view.LayoutInflater;
import android.view.View;

/**
 * Created by matthew on 3/3/17.
 */

public interface CustomListItem
{
    int getViewType();
    View getView(LayoutInflater inflater, View view);
    boolean isEnabled();
}
