package vanderbilt.vuiis_cest_2017.Data.News;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

import vanderbilt.vuiis_cest_2017.Data.DatabaseHelper;
import vanderbilt.vuiis_cest_2017.R;

/**
 * Created by Matthew Christian on 3/30/17.
 */

public class NewsItem extends NewsListItem {
    private NewsArticle mArticle;

    public NewsItem(NewsArticle article) {
        mArticle = article;
    }

    public NewsArticle getArticle() { return mArticle; }

    @Override
    public int getViewType() {
        return RowType.NEWS_ITEM.ordinal();
    }

    @Override
    public View getView(LayoutInflater inflater, View convertView) {
        View view;
        if(convertView == null) {
            view = inflater.inflate(R.layout.news_item, null);
            view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
        else {
            view = convertView;
        }
        TextView tvDate = (TextView)view.findViewById(R.id.label_news_date);
        TextView tvTitle = (TextView)view.findViewById(R.id.label_news_title);
        TextView tvSummary = (TextView)view.findViewById(R.id.label_news_summary);
        ImageView ivUnread = (ImageView)view.findViewById(R.id.image_unread);
        Date startTime = mArticle.getStartTime();
        String date = new SimpleDateFormat("EEE, MMM dd").format(startTime);
        String time = new SimpleDateFormat("h:mm a").format(startTime);
        tvDate.setText(date);
        tvTitle.setText(mArticle.getTitle());
        tvSummary.setText(mArticle.getContent());
        if(mArticle.isRead()) {
            ivUnread.setVisibility(View.GONE);
        }
        else {
            ivUnread.setVisibility(View.VISIBLE);
        }
        return view;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public Date getStartTime() {
        return mArticle.getStartTime();
    }

    public void markRead(DatabaseHelper dataHelper) {
        NewsManager.markRead(dataHelper, mArticle);
    }
}
