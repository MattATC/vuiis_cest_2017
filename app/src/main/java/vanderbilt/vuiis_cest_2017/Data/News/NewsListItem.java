package vanderbilt.vuiis_cest_2017.Data.News;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;

import java.util.Date;

import vanderbilt.vuiis_cest_2017.Data.CustomListItem;

/**
 * Created by Matthew Christian on 3/30/17.
 */

public abstract class NewsListItem implements CustomListItem, Comparable<NewsListItem> {

    public enum RowType {
        NEWS_ITEM
    }

    @Override
    public abstract int getViewType();

    @Override
    public abstract View getView(LayoutInflater inflater, View view);

    @Override
    public abstract boolean isEnabled();

    @Override
    public int compareTo(@NonNull NewsListItem other) {
        return getStartTime().compareTo(other.getStartTime());
    }

    public abstract Date getStartTime();
}
