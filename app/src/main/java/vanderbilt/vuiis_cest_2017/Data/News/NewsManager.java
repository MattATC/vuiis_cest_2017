package vanderbilt.vuiis_cest_2017.Data.News;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import vanderbilt.vuiis_cest_2017.Data.DatabaseHelper;
import vanderbilt.vuiis_cest_2017.Data.DatabaseMetaData;
import vanderbilt.vuiis_cest_2017.DataManager.DataUpdateInfo;

/**
 * Created by Matthew Christian on 3/30/17.
 */

public class NewsManager {
    static private NewsManager mNewsManager;
    static private Map<Integer, NewsArticle> mArticles = new HashMap<>();
    static private int mNumberUnreadArticles = 0;

    static public void setInstance(NewsManager newsManager) {
        mNewsManager = newsManager;
    }

    static public NewsManager getInstance() {
        return mNewsManager;
    }

    public NewsManager() {
    }

    static public void updateNews(DatabaseHelper dataHelper, DataUpdateInfo dataUpdateInfo) {
        Set<String> tablesAffected = dataUpdateInfo.getTablesAffected();
        if(tablesAffected.contains(DatabaseMetaData.TABLE_NEWS)) {
            mArticles.clear();
            mNumberUnreadArticles = 0;
            Date currentTime = new Date();
            SQLiteDatabase db = dataHelper.getReadableDatabase();
            String query;
            Cursor cursor;
            query = "select id, title, startTime, endTime, content, isRead from News order by id";
            cursor = db.rawQuery(query, null);
            cursor.moveToFirst();
            while(!cursor.isAfterLast()) {
                final int articleID = cursor.getInt(0);
                final String title = cursor.getString(1);
                final String startTime = cursor.getString(2);
                final String endTime = cursor.getString(3);
                final String content = cursor.getString(4);
                final boolean isRead = cursor.getInt(5) > 0;
                try {
                    NewsArticle article = new NewsArticle(articleID, title,
                            DatabaseHelper.convertTimestampToDate(startTime),
                            DatabaseHelper.convertTimestampToDate(endTime),
                            content, isRead);
                    if(!currentTime.before(article.getStartTime()) &&
                            !currentTime.after(article.getEndTime())) { //Only get current news
                        if (!isRead) {
                            mNumberUnreadArticles++;
                        }
                        mArticles.put(articleID, article);
                    }
                    cursor.moveToNext();
                }
                catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            cursor.close();
            db.close();
        }
    }

    static public Collection<NewsArticle> getArticles() {
        return mArticles.values();
    }

    static public void markRead(DatabaseHelper dataHelper, NewsArticle article) {
        if(!article.isRead()) {
            SQLiteDatabase db = dataHelper.getWritableDatabase();
            db.beginTransaction();
            try {
                String stmt = "update News set isRead = 1 where id = ?";
                ArrayList<String> values = new ArrayList<>();
                values.add(Integer.toString(article.getID()));
                db.execSQL(stmt, values.toArray(new String[values.size()]));
                db.setTransactionSuccessful();
                article.markRead();
                mNumberUnreadArticles--;
            }
            catch (SQLiteException e) {
                Log.e("markRead", "Exception occurred while marking News " + e);
            }
            finally {
                db.endTransaction();
            }
            db.close();
        }
    }

    static public boolean hasUnReadNews() {
        return mNumberUnreadArticles > 0;
    }

    static public int numberOfUnReadNews() {
        return mNumberUnreadArticles;
    }
}
