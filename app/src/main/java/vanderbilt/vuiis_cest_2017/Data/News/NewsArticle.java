package vanderbilt.vuiis_cest_2017.Data.News;

import android.support.annotation.NonNull;

import java.util.Date;

/**
 * Created by Matthew Christian on 3/30/17.
 */

public class NewsArticle implements Comparable<NewsArticle> {
    private int mID;
    private String mTitle;
    private Date mStartTime;
    private Date mEndTime;
    private String mContent;
    private boolean mIsRead;

    public NewsArticle(int id, String title, Date startTime, Date endTime, String content,
                       boolean isRead) {
        mID = id;
        mTitle = title;
        mStartTime = startTime;
        mEndTime = endTime;
        mContent = content;
        mIsRead = isRead;
    }

    public int getID() { return mID; }
    public String getTitle() { return mTitle; }
    public Date getStartTime() { return mStartTime; }
    public Date getEndTime() { return mEndTime; }
    public String getContent() { return mContent; }
    public boolean isRead() { return mIsRead; }

    @Override
    public int compareTo(@NonNull NewsArticle other) {
        return mStartTime.compareTo(other.mStartTime);
    }

    public void markRead() {
        mIsRead = true;
    }
}
