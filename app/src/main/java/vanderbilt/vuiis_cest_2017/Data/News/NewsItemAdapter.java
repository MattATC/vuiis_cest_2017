package vanderbilt.vuiis_cest_2017.Data.News;

import android.content.Context;

import java.util.List;

import vanderbilt.vuiis_cest_2017.Data.CustomItemAdapter;

/**
 * Created by Matthew Christian on 3/30/17.
 */

public class NewsItemAdapter extends CustomItemAdapter<NewsListItem> {
    public NewsItemAdapter(Context context, List<NewsListItem> items) {
        super(context, android.R.layout.simple_list_item_1, items);
    }

    @Override
    public int getViewTypeCount() { return NewsListItem.RowType.values().length; }
}
