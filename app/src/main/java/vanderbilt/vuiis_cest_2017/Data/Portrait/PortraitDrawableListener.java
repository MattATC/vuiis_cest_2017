package vanderbilt.vuiis_cest_2017.Data.Portrait;

import android.graphics.drawable.Drawable;

import java.io.File;

/**
 * Created by matthew on 5/12/17.
 */

public interface PortraitDrawableListener {
    void onPortraitReady(String pictureURL, File portraitFile, Drawable portrait);
    void onPortraitNotFound(String pictureURL);
}
