package vanderbilt.vuiis_cest_2017.Data.Portrait;

import android.graphics.drawable.Drawable;
import android.os.AsyncTask;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import vanderbilt.vuiis_cest_2017.ThisApplication;
import vanderbilt.vuiis_cest_2017.Utilities.NetworkTaskCommon;

/**
 * Created by matthew on 5/12/17.
 */

public class PortraitRetrievalTask extends AsyncTask<Integer, Void, Boolean> {
    private String mPictureURL;
    private File mExternalFile;
    private Drawable mDrawable;
    private PortraitDrawableListener mPortraitListener;

    public PortraitRetrievalTask(String pictureURL, File externalFile,
                                 PortraitDrawableListener pictureDrawableListener) {
        mPictureURL = pictureURL;
        mExternalFile = externalFile;
        mPortraitListener = pictureDrawableListener;
    }

    @Override
    protected Boolean doInBackground(Integer... integers) {
        boolean success = false;
        try {
            Map<String, String> header = new HashMap<>();
            header.put("Content-Type", "application/pdf");
            header.put("x-api-key", ThisApplication.getServerAPIKey());
            byte[] data = NetworkTaskCommon.getServerHTTPData(mPictureURL, "", "", header);
            copyDataToFile(data);
            success = true;
        }
        catch(IllegalArgumentException e) {
            e.printStackTrace();
        }
        catch(ClassCastException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return success;
    }

    private void copyDataToFile(byte[] data) throws IOException {
        InputStream inStream = new ByteArrayInputStream(data);
        OutputStream outStream = new FileOutputStream(mExternalFile.getAbsolutePath());
        //Copy bytes to the file
        byte[] buffer = new byte[4096];
        while(inStream.read(buffer) != -1){
            outStream.write(buffer);
        }
        inStream.close();
        outStream.flush();
        outStream.close();
        mDrawable = Drawable.createFromPath(mExternalFile.getAbsolutePath());
    }

    /**
     * Runs on the UI thread after doInBackground completes successfully
     * @param result
     */
    @Override
    protected void onPostExecute(Boolean result) {
        if(mPortraitListener != null) {
            if(result) {
                mPortraitListener.onPortraitReady(mPictureURL, mExternalFile, mDrawable);
            }
            else {
                mPortraitListener.onPortraitNotFound(mPictureURL);
            }
        }
    }

    /**
     * Runs on the UI thread after doInBackground does NOT complete successfully
     */
    @Override
    protected void onCancelled() {
        if(mPortraitListener != null) {
            mPortraitListener.onPortraitNotFound(mPictureURL);
        }
    }
}
