package vanderbilt.vuiis_cest_2017.Data.Portrait;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.content.res.AppCompatResources;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import vanderbilt.vuiis_cest_2017.MainActivity;
import vanderbilt.vuiis_cest_2017.R;
import vanderbilt.vuiis_cest_2017.ThisApplication;

/**
 * Created by matthew on 5/12/17.
 */

public class PortraitManager implements PortraitDrawableListener {
    private final static String CONFIG_NAME = "Pictures";
    private final static String EXTERNAL_PORTRAIT_DIR = "VUIIS_CEST_2017_Portraits";
    private static PortraitManager mPictureManager;

    public interface PortraitLoadObserver {
        void onPortraitLoad(String pictureURL, Drawable portrait);
        void onPortraitLoadFailed(String pictureURL);
    };

    private class PortraitInfo {
        File mExternalFile;
        Drawable mDrawable;
        PortraitInfo(File externalFile, Drawable drawable) {
            mExternalFile = externalFile;
            mDrawable = drawable;
        }
    }

    private Drawable mPlaceholder;
    private Map<String, PortraitInfo> mPortraitInfos = new HashMap<>();
    private Map<String, PortraitRetrievalTask> mPortraitRetrievals = new HashMap<>();
    private Map<String, Set<PortraitLoadObserver>> mPortraitObserverMap = new HashMap<>();

    public PortraitManager(MainActivity activity) {
        mPlaceholder = AppCompatResources.getDrawable(activity, R.drawable.placeholder);
        loadPortraitInfo(activity);
    }

    static public void setInstance(PortraitManager pictureManager) {
        mPictureManager = pictureManager;
    }

    static public PortraitManager getInstance() {
        return mPictureManager;
    }

    static public void deletePortraits(Context context) {
        File externalDir = new File(context.getFilesDir(), EXTERNAL_PORTRAIT_DIR);
        if(externalDir.exists()) {
            for (String filename : externalDir.list()) {
                new File(externalDir, filename).delete();
            }
        }
    }

    static public Drawable getPlaceholderPortrait() {
        final PortraitManager portraitManager = getInstance();
        return portraitManager.mPlaceholder;
    }

    private PortraitInfo retrievePortrait(Context context, @NonNull final String pictureURL) {
        PortraitInfo portraitInfo = null;
        if(mPortraitInfos.containsKey(pictureURL)) {
            portraitInfo = mPortraitInfos.get(pictureURL);
        }
        else {
            File externalDir = new File(context.getFilesDir(), EXTERNAL_PORTRAIT_DIR);
            if(externalDir.mkdirs() || externalDir.isDirectory()) {
                //Directory exists
            }
            File externalFile = new File(externalDir, pictureURL);
            portraitInfo = new PortraitInfo(externalFile, null);
            if(externalFile.exists()) {
                portraitInfo.mDrawable = Drawable.createFromPath(externalFile.getAbsolutePath());
                mPortraitInfos.put(pictureURL, portraitInfo);
            }
            else {
                if(!mPortraitRetrievals.containsKey(pictureURL)) {
                    PortraitRetrievalTask task = new PortraitRetrievalTask(pictureURL,
                            externalFile, this);
                    mPortraitRetrievals.put(pictureURL, task);
                    task.execute(0);
                }
                portraitInfo = null;
            }
        }
        return portraitInfo;
    }

    static public void requestPortrait(final Context context, final String pictureURL,
                                       PortraitLoadObserver observer) {
        if(pictureURL == null || pictureURL.equals("")) {
            observer.onPortraitLoadFailed(pictureURL);
            return;
        }
        final PortraitManager portraitManager = getInstance();
        //Look locally first
        Drawable drawable = portraitManager.findPortraitLocal(context, pictureURL);
        if(drawable != null) {
            observer.onPortraitLoad(pictureURL, drawable);
        }
        else { //Look in the cache, and then request it from the server
            PortraitInfo portraitInfo = portraitManager.retrievePortrait(context, pictureURL);
            if (portraitInfo != null) {
                observer.onPortraitLoad(pictureURL, portraitInfo.mDrawable);
            }
            else {
                if (!portraitManager.mPortraitObserverMap.containsKey(pictureURL)) {
                    portraitManager.mPortraitObserverMap.put(pictureURL,
                            new HashSet<PortraitLoadObserver>());
                }
                portraitManager.mPortraitObserverMap.get(pictureURL).add(observer);
            }
        }
    }

    @Override
    public void onPortraitReady(String pictureURL, File portraitFile, Drawable portrait) {
        PortraitInfo portraitInfo = new PortraitInfo(portraitFile, portrait);
        mPortraitInfos.put(pictureURL, portraitInfo);
        mPortraitRetrievals.remove(pictureURL);
        if(mPortraitObserverMap.containsKey(pictureURL)) {
            Set<PortraitLoadObserver> observers = mPortraitObserverMap.get(pictureURL);
            for (PortraitLoadObserver observer : observers) {
                observer.onPortraitLoad(pictureURL, portrait);
            }
            mPortraitObserverMap.remove(pictureURL);
        }
    }

    @Override
    public void onPortraitNotFound(String pictureURL) {
        mPortraitInfos.remove(pictureURL);
        mPortraitRetrievals.remove(pictureURL);
        if(mPortraitObserverMap.containsKey(pictureURL)) {
            Set<PortraitLoadObserver> observers = mPortraitObserverMap.get(pictureURL);
            for (PortraitLoadObserver observer : observers) {
                observer.onPortraitLoadFailed(pictureURL);
            }
            mPortraitObserverMap.remove(pictureURL);
        }
    }

    /**
     * hasAsset
     * @param pictureURL
     * @return
     */
    public boolean hasAsset(final String pictureURL) {
        return mPortraitInfos.containsKey(pictureURL);
    }

    private Drawable findPortraitLocal(Context context, String pictureURL) {
        String drawableName;
        if (pictureURL == null || pictureURL.equals("")) {
            //drawableName = "placeholder";
            Log.d("PortraitManager", "drawable not found: '" + pictureURL + "'");
            return null;
        }
        else {
            //Convert CamelCase name to the Android snake_case drawable name
            drawableName = "person_" + pictureURL.replaceAll("([A-Z]{2,})([a-z])", "$1_$2")
                    .toLowerCase().replace(".jpg", "");
            Log.d("PortraitManager", pictureURL + " drawableName is: " + drawableName);
        }
        Resources resources = context.getResources();
        String packageName = context.getPackageName();
        int resourceId = resources.getIdentifier(drawableName, "drawable", packageName);

        if(resourceId == 0) {
            Log.d("PortraitManager", "drawable not found: '" + pictureURL + "'");
            return null;
        }
        else {
            return AppCompatResources.getDrawable(context, resourceId);
        }
    }

    private void loadPortraitInfo(Context context) {
        mPortraitInfos.clear();
        File externalDir = new File(context.getFilesDir(), EXTERNAL_PORTRAIT_DIR);
        try {
            JSONArray array = new JSONArray(ThisApplication.getConfigValue(CONFIG_NAME, ""));
            int arrayLength = array.length();
            int i = 0;
            while(i < arrayLength) {
                JSONObject obj = array.getJSONObject(i);
                String abstractURL = obj.getString("pictureURL");
                File externalFile = new File(externalDir, obj.getString("filename"));
                Drawable drawable =
                        Drawable.createFromPath(externalFile.getAbsolutePath());
                mPortraitInfos.put(abstractURL, new PortraitInfo(externalFile, drawable));
                i++;
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void savePortraitInfo() {
        JSONArray array = new JSONArray();
        for(Map.Entry<String, PortraitInfo> entry: mPortraitInfos.entrySet()) {
            JSONObject obj = new JSONObject();
            try {
                PortraitInfo portraitInfo = entry.getValue();
                obj.put("pictureURL", entry.getKey());
                obj.put("filename", portraitInfo.mExternalFile.getName());
                array.put(obj);
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }
        ThisApplication.saveConfigValue(CONFIG_NAME, array.toString());
        mPortraitInfos.clear();
    }
}
