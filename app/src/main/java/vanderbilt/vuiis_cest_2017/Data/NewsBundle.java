package vanderbilt.vuiis_cest_2017.Data;

import java.util.Collection;
import java.util.Collections;

/**
 * Created by Matthew Christian on 3/15/17.
 */

public class NewsBundle {
    private Collection<NewsArticle> mArticles;

    public NewsBundle(Collection<NewsArticle> articles) {
        mArticles = articles;
    }

    public final Collection<NewsArticle> getArticles() {
        return Collections.unmodifiableCollection(mArticles);
    }

    public boolean isEmpty() { return mArticles == null || mArticles.isEmpty(); }

    public int numOfItems() {
        if(mArticles == null) {
            return 0;
        }
        return mArticles.size();
    }
}

