package vanderbilt.vuiis_cest_2017.Data;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.text.ParseException;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Matthew Christian on 3/21/17.
 */

public class DatabaseMetaData {
    static private Map<String, Date> mTableLastModified = new HashMap<>();

    static public final String TABLE_PERSON = "Person";
    static public final String TABLE_SECTION_TYPE = "SectionType";
    static public final String TABLE_SECTION = "Section";
    static public final String TABLE_MODERATOR = "Moderator";
    static public final String TABLE_PRESENTATION = "Presentation";
    static public final String TABLE_PRESENTATION_RATING = "PresentationRating";
    static public final String TABLE_NEWS = "News";

    static public final String[] PUBLIC_TABLES = new String[] { TABLE_PERSON, TABLE_SECTION_TYPE,
            TABLE_SECTION, TABLE_MODERATOR, TABLE_PRESENTATION, TABLE_NEWS };
    static public final String DATABASE_NAME = "vuiis_cest_2017.db";
    static public final int DATABASE_VERSION = 1;

    static public Map<String, Date> getDateTablesLastModified() {
        return Collections.unmodifiableMap(mTableLastModified);
    }

    static public void updateDateTablesLastModified(DatabaseHelper dataHelper) {
        SQLiteDatabase db = dataHelper.getReadableDatabase();
        Cursor cursor;
        for(String tableName: DatabaseMetaData.PUBLIC_TABLES) {
            Date lastModified = null;
            Date rowModified;
            try {
                cursor = db.query(tableName, new String[]{"timeModified"}, null, null, null, null,
                        null);
                cursor.moveToFirst();
                while(!cursor.isAfterLast()) {
                    String rowTimestamp = cursor.getString(0);
                    rowModified = DatabaseHelper.convertTimestampToDate(rowTimestamp);
                    if((lastModified == null) || rowModified.after(lastModified)) {
                        lastModified = rowModified;
                    }
                    cursor.moveToNext();
                }
                cursor.close();
            }
            catch(android.database.sqlite.SQLiteException e) {
                Log.e("DataUpdateTask", "Exception occurred while scanning timeModified: " + e);
            }
            catch (ParseException e) {
                e.printStackTrace();
            }
            mTableLastModified.put(tableName, lastModified);
        }
        db.close();
    }

}
