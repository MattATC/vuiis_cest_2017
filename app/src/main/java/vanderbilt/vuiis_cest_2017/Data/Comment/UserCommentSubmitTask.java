package vanderbilt.vuiis_cest_2017.Data.Comment;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import vanderbilt.vuiis_cest_2017.ThisApplication;
import vanderbilt.vuiis_cest_2017.Utilities.NetworkTaskCommon;

/**
 * Created by Matthew Christian on 4/4/17.
 */

public class UserCommentSubmitTask extends AsyncTask<Integer, Void, Boolean> {
    private static final String COMMENT_SUBMIT_SCRIPT_PATH = "question";

    private UserComment mComment;
    private UserCommentSubmitListener mTaskDelegate;

    public UserCommentSubmitTask(UserComment comment,
                                 UserCommentSubmitListener taskDelegate) {
        mComment = comment;
        mTaskDelegate = taskDelegate;
    }

    @Override
    protected Boolean doInBackground(Integer... params) {
        boolean success = false;
        String body = mComment.toJSON().toString();
        try {
            Map<String, String> header = new HashMap<>();
            header.put("Content-Type", "application/json");
            header.put("x-api-key", ThisApplication.getServerAPIKey());
            String response = new String(NetworkTaskCommon.postServerHTTPData(
                    COMMENT_SUBMIT_SCRIPT_PATH, "", body, header), "UTF-8");
            JSONObject obj = new JSONObject(response);
            success = obj.getBoolean("success");
        }
        catch (ClassCastException e) {
            e.printStackTrace();
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return success;
    }

    /**
     * Runs on the UI thread after doInBackground completes successfully
     * @param result
     */
    @Override
    protected void onPostExecute(Boolean result) {
        if(mTaskDelegate != null) {
            mTaskDelegate.onCommentSubmitted(result, mComment);
        }
    }

    /**
     * Runs on the UI thread after doInBackground does NOT complete successfully
     */
    @Override
    protected void onCancelled() {
        if(mTaskDelegate != null) {
            mTaskDelegate.onCommentSubmissionCancelled(mComment);
        }
    }
}
