package vanderbilt.vuiis_cest_2017.Data.Comment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collection;

import vanderbilt.vuiis_cest_2017.ThisApplication;

/**
 * Created by Matthew Christian on 4/4/17.
 */

public class UserComment {
    private int mPresentationID;
    private String mContent;

    public UserComment(int presentationID, String content) {
        mPresentationID = presentationID;
        mContent = content;
    }

    public int getID() { return mPresentationID; }

    public String getContent() { return mContent; }

    public JSONObject toJSON() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("presentationID", mPresentationID);
            obj.put("uid", ThisApplication.getInstallationID());
            obj.put("question", mContent);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        return obj;
    }

    public static JSONArray saveCommentsToJSON(Collection<UserComment> comments) {
        JSONArray array = new JSONArray();
        for(UserComment comment: comments) {
            array.put(comment.toJSON());
        }
        return array;
    }
}
