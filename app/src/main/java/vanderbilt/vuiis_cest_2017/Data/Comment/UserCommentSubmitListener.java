package vanderbilt.vuiis_cest_2017.Data.Comment;

/**
 * Created by Matthew Christian on 4/4/17.
 */

public interface UserCommentSubmitListener {
    void onCommentSubmitted(boolean result, UserComment comment);
    void onCommentSubmissionCancelled(UserComment comment);
}
