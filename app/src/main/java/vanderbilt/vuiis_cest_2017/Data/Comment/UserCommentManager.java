package vanderbilt.vuiis_cest_2017.Data.Comment;

import android.os.Handler;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import vanderbilt.vuiis_cest_2017.Data.DatabaseHelper;

/**
 * Created by Matthew Christian on 3/28/17.
 */

public class UserCommentManager implements UserCommentSubmitListener {
    static private UserCommentManager mUserCommentManager;
    private static Map<Integer, UserComment> mComments = new HashMap<>();

    private List<UserComment> mQueue = new ArrayList<>();
    private Handler mCommentSubmitHandler = new Handler();
    private int mSubmissionInterval = 60000;  // Retry submission every 1 minute
    private Runnable mCommentSubmitRunnable = new Runnable() {
        @Override
        public void run() {
            startSubmission();
        }
    };

    static public void setInstance(UserCommentManager userCommentManager) {
        mUserCommentManager = userCommentManager;
    }

    static public UserCommentManager getInstance() {
        return mUserCommentManager;
    }

    public UserCommentManager() {
    }

    static public synchronized void submitComment(DatabaseHelper dataHelper, UserComment comment) {
        UserCommentManager userCommentManager = getInstance();
        userCommentManager.saveComment(dataHelper, comment); //Save the comment to the database
        if(userCommentManager.mQueue.isEmpty()) { // If no task is currently running, execute one immediately
            new UserCommentSubmitTask(comment, userCommentManager).execute(0);
        }
        else {  // Else add it to the queue for the next startSubmission call
            userCommentManager.mQueue.add(comment);
        }
    }

    private synchronized void startSubmission() {
        if(!mQueue.isEmpty()) {
            new UserCommentSubmitTask(mQueue.get(0), this).execute(0);
        }
    }

    //@Override
    public synchronized void onCommentSubmitted(boolean result,
                                               UserComment comment) {
        if(result) { // If submit was successful, remove those items from the queue, if present
            mQueue.remove(comment);
        }
        else { // If submit was not successful, try again
            Log.d("onCommentSubmitted", "Submission was not successful, trying again " +
                    comment.toString());
            mQueue.add(comment);
        }
        if(!mQueue.isEmpty()) {
            mCommentSubmitHandler.postDelayed(mCommentSubmitRunnable, mSubmissionInterval);
        }
    }

    //@Override
    public synchronized void onCommentSubmissionCancelled(UserComment comment) {
    }

    static public UserComment getComment(int presentationID) {
        return mComments.get(presentationID);
    }

    /**
     * Saves the comment to the database
     * @param comment
     */
    private void saveComment(DatabaseHelper dataHelper, UserComment comment) {
        final int presentationID = comment.getID();
        mComments.remove(presentationID);
        mComments.put(presentationID, comment);
    }
}
