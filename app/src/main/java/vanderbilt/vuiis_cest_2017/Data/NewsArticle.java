package vanderbilt.vuiis_cest_2017.Data;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Matthew Christian on 3/15/17.
 */

public class NewsArticle {
    private int mID;
    private String mTitle;
    private Date mBeginTime;
    private Date mEndTime;
    private String mContent;
    private boolean mIsRead;

    public NewsArticle(int id, String title, Date beginTime, Date endTime, String content, boolean isRead) {
        mID = id;
        mTitle = title;
        mBeginTime = beginTime;
        mEndTime = endTime;
        mContent = content;
        mIsRead = isRead;
    }

    public int getID() { return mID; }
    public String getTitle() { return mTitle; }
    public Date getBeginTime() { return mBeginTime; }
    public Date getEndTime() { return mEndTime; }
    public String getBeginTimeAsDateString() {
        return new SimpleDateFormat("EEE, MMM dd").format(mBeginTime);
    }
    public String getBeginTimeString() {
        return new SimpleDateFormat("h:mm a").format(mBeginTime);
    }
    public String getContent() { return mContent; }
    public boolean isRead() { return mIsRead; }
    public void setIsRead(boolean isRead) { mIsRead = isRead; }
}
