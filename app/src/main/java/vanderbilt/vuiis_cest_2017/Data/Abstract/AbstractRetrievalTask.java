package vanderbilt.vuiis_cest_2017.Data.Abstract;

import android.os.AsyncTask;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import vanderbilt.vuiis_cest_2017.ThisApplication;
import vanderbilt.vuiis_cest_2017.Utilities.NetworkTaskCommon;

/**
 * Created by Matthew Christian on 4/7/17.
 */

public class AbstractRetrievalTask extends AsyncTask<Integer, Void, Boolean> {
    private String mAbstractURL;
    private File mExternalFile;
    private AbstractDocumentListener mAbstractListener;

    public AbstractRetrievalTask(String abstractURL, File externalFile,
                                 AbstractDocumentListener abstractDocumentListener) {
        mAbstractURL = abstractURL;
        mExternalFile = externalFile;
        mAbstractListener = abstractDocumentListener;
    }

    @Override
    protected Boolean doInBackground(Integer... params) {
        boolean success = false;
        try {
            Map<String, String> header = new HashMap<>();
            header.put("Content-Type", "application/pdf");
            header.put("x-api-key", ThisApplication.getServerAPIKey());
            byte[] data = NetworkTaskCommon.getServerHTTPData(mAbstractURL, "", "", header);
            copyDataToFile(data);
            success = true;
        }
        catch(IllegalArgumentException e) {
            e.printStackTrace();
        }
        catch(ClassCastException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return success;
    }

    private void copyDataToFile(byte[] data) throws IOException {
        InputStream inStream = new ByteArrayInputStream(data);
        OutputStream outStream = new FileOutputStream(mExternalFile.getAbsolutePath());
        //Copy bytes to the file
        byte[] buffer = new byte[4096];
        while(inStream.read(buffer) != -1){
            outStream.write(buffer);
        }
        inStream.close();
        outStream.flush();
        outStream.close();
    }

    /**
     * Runs on the UI thread after doInBackground completes successfully
     * @param result
     */
    @Override
    protected void onPostExecute(Boolean result) {
        if(mAbstractListener != null) {
            if(result) {
                mAbstractListener.onAbstractDocumentReady(mAbstractURL, mExternalFile);
            }
            else {
                mAbstractListener.onAbstractDocumentNotFound(mAbstractURL);
            }
        }
    }

    /**
     * Runs on the UI thread after doInBackground does NOT complete successfully
     */
    @Override
    protected void onCancelled() {
        if(mAbstractListener != null) {
            mAbstractListener.onAbstractDocumentNotFound(mAbstractURL);
        }
    }
}
