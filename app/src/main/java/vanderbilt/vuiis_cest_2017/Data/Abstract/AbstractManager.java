package vanderbilt.vuiis_cest_2017.Data.Abstract;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.pdf.PdfRenderer;
import android.net.Uri;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import vanderbilt.vuiis_cest_2017.MainActivity;
import vanderbilt.vuiis_cest_2017.ThisApplication;

/**
 * Created by Matthew Christian on 4/7/17.
 */

public class AbstractManager implements AbstractDocumentListener {
    private final static String CONFIG_NAME = "Abstracts";
    private final static String EXTERNAL_ABSTRACT_DIR = "VUIIS_CEST_2017_Abstracts";
    private final static int BITMAP_CACHE_LIMIT = 4;
    private final static double ROOT_2 = Math.sqrt(2);
    private static AbstractManager mAbstractManager;

    enum AbstractTaskTag {
        TASK_PREVIEW,
        TASK_OPEN
    }
    private interface AbstractTask {
        void execute();
    }

    private Map<String, File> mAbstractPaths = new HashMap<>();
    private Map<String, AbstractRetrievalTask> mAbstractRetrievals = new HashMap<>();
    private Map<String, Map<AbstractTaskTag, AbstractTask>> mAbstractTaskMap = new HashMap<>();
    private Map<String, Bitmap> mBitmaps = new HashMap<>();

    public AbstractManager(MainActivity activity) {
        loadAbstractInfo(activity);
    }

    static public void setInstance(AbstractManager abstractManager) {
        mAbstractManager = abstractManager;
    }

    static public AbstractManager getInstance() {
        return mAbstractManager;
    }

    static public void deleteAbstracts(Context context) {
        File externalDir = new File(context.getFilesDir(), EXTERNAL_ABSTRACT_DIR);
        if(externalDir.exists()) {
            for (String filename : externalDir.list()) {
                new File(externalDir, filename).delete();
            }
        }
    }

    static public boolean isAbstractPreviewSupported() {
        return Build.VERSION.SDK_INT >= 21;
    }

    /**
     * retrieveAbstract
     * @param context
     * @param abstractURL
     * @return
     */
    private File retrieveAbstract(Context context, @NonNull final String abstractURL) {
        File abstractFile = null;
        if(mAbstractPaths.containsKey(abstractURL)) {
            abstractFile = mAbstractPaths.get(abstractURL);
        }
        else {
            File externalDir = new File(context.getFilesDir(), EXTERNAL_ABSTRACT_DIR);
            if(externalDir.mkdirs() || externalDir.isDirectory()) {
                //Directory exists
            }
            abstractFile = new File(externalDir, abstractURL);
            if(abstractFile.exists()) {
                mAbstractPaths.put(abstractURL, abstractFile);
            }
            else {
                if(!mAbstractRetrievals.containsKey(abstractURL)) {
                    AbstractRetrievalTask task = new AbstractRetrievalTask(abstractURL,
                            abstractFile, this);
                    mAbstractRetrievals.put(abstractURL, task);
                    task.execute(0);
                }
                abstractFile = null;
            }
        }
        return abstractFile;
    }

    /**
     * previewAbstract
     * @param context
     * @param abstractURL
     * @param imageView
     * @return
     */
    static public void previewAbstract(final Context context, final String abstractURL,
                                       final ImageView imageView, final int maxWidth) {
        if(abstractURL == null) {
            return;
        }
        final AbstractManager abstractManager = getInstance();
        final File abstractFile = abstractManager.retrieveAbstract(context, abstractURL);
        if(abstractFile != null) {
            abstractManager.createAbstractPreview(abstractFile, imageView, maxWidth);
        }
        else {
            //Show the loading image
            int height = (int)(maxWidth * ROOT_2);
            Bitmap bitmap = Bitmap.createBitmap(maxWidth, height, Bitmap.Config.ARGB_4444);
            Canvas canvas = new Canvas(bitmap);
            Paint whitePaint = new Paint();
            whitePaint.setColor(Color.WHITE);
            canvas.drawColor(Color.argb(255, 127, 127, 127));
            imageView.setImageBitmap(bitmap);
            imageView.invalidate();

            if(!abstractManager.mAbstractTaskMap.containsKey(abstractURL)) {
                abstractManager.mAbstractTaskMap.put(abstractURL,
                        new HashMap<AbstractTaskTag, AbstractTask>());
            }
            Map<AbstractTaskTag, AbstractTask> taskMap = abstractManager.mAbstractTaskMap.get(
                    abstractURL);
            taskMap.put(AbstractTaskTag.TASK_PREVIEW, new AbstractTask() {
                @Override
                public void execute() {
                    previewAbstract(context, abstractURL, imageView, maxWidth);
                }
            });
        }
    }

    /**
     * openAbstract
     * @param context
     * @param abstractURL
     */
    static public void openAbstract(final Context context, final String abstractURL) {
        if(abstractURL == null) {
            return;
        }
        final AbstractManager abstractManager = getInstance();
        File abstractFile = abstractManager.retrieveAbstract(context, abstractURL);
        if(abstractFile != null) {
            abstractManager.openAbstractFile(context, abstractFile);
        }
        else {
            if(!abstractManager.mAbstractTaskMap.containsKey(abstractURL)) {
                abstractManager.mAbstractTaskMap.put(abstractURL,
                        new HashMap<AbstractTaskTag, AbstractTask>());
            }
            Map<AbstractTaskTag, AbstractTask> taskMap = abstractManager.mAbstractTaskMap.get(
                    abstractURL);
            taskMap.put(AbstractTaskTag.TASK_OPEN, new AbstractTask() {
                @Override
                public void execute() {
                    openAbstract(context, abstractURL);
                }
            });
        }
    }

    @Override
    public void onAbstractDocumentReady(String abstractURL, File abstractFile) {
        mAbstractPaths.put(abstractURL, abstractFile);
        mAbstractRetrievals.remove(abstractURL);
        if(mAbstractTaskMap.containsKey(abstractURL)) {
            Map<AbstractTaskTag, AbstractTask> taskMap = mAbstractTaskMap.get(abstractURL);
            for (AbstractTask task : taskMap.values()) {
                task.execute();
            }
            taskMap.clear();
        }
        mAbstractTaskMap.remove(abstractURL);
    }

    @Override
    public void onAbstractDocumentNotFound(String abstractURL) {
        mAbstractPaths.remove(abstractURL);
        mAbstractRetrievals.remove(abstractURL);
        mAbstractTaskMap.remove(abstractURL);
    }

    /**
     * openAbstractFile
     * @param abstractFile
     */
    private void openAbstractFile(Context context, File abstractFile) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri uri = FileProvider.getUriForFile(context.getApplicationContext(),
                    context.getPackageName() + ".provider", abstractFile);
            intent.setDataAndType(uri, "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            //Grant read permission for all apps that can handle "Read a PDF" intent
            List<ResolveInfo> resInfoList = context.getPackageManager().queryIntentActivities(
                    intent, PackageManager.MATCH_DEFAULT_ONLY);
            for (ResolveInfo resolveInfo : resInfoList) {
                context.grantUriPermission(resolveInfo.activityInfo.packageName, uri,
                        Intent.FLAG_GRANT_READ_URI_PERMISSION);
            }

            context.startActivity(intent);
        }
        catch (ActivityNotFoundException e) {
            Toast.makeText(context, "No application to open PDF",
                    Toast.LENGTH_SHORT).show();
            Log.e("AbstractManager", "No application to open PDF:" + e.getMessage());
        }
    }

    /**
     * createAbstractPreview
     * @param abstractFile
     * @param imageView
     */
    private void createAbstractPreview(final File abstractFile, final ImageView imageView,
                                       final int maxWidth) {
        if(!isAbstractPreviewSupported()) {
            return; //Not supported
        }
        try {
            PdfRenderer renderer = new PdfRenderer(ParcelFileDescriptor.open(abstractFile,
                    ParcelFileDescriptor.MODE_READ_ONLY));
            int currentPage = 0;
            PdfRenderer.Page page = renderer.openPage(currentPage);
            double pageRatio = (double) page.getHeight() / page.getWidth();
            int pageWidth = Math.max(page.getWidth(), maxWidth);
            int pageHeight = (int) (pageWidth * pageRatio);
            int bgWidth = Math.max(maxWidth, pageWidth + 10);
            int bgHeight = (int) (bgWidth * pageRatio);
            int left = (int) Math.max(0, bgWidth / 2.0 - pageWidth / 2.0);
            int top = (int) Math.max(0, bgHeight / 2.0 - pageHeight / 2.0);
            int right = Math.min(left + pageWidth, bgWidth);
            int bottom = Math.min(top + pageHeight, bgHeight);
            Rect rect = new Rect(left, top, right, bottom);
            Bitmap bitmap;
            String bitmapKey = String.format("%s %s", bgWidth, bgHeight);
            if(mBitmaps.containsKey(bitmapKey)) {
                bitmap = mBitmaps.get(bitmapKey);
            }
            else {
                bitmap = Bitmap.createBitmap(bgWidth, bgHeight, Bitmap.Config.ARGB_4444);
                if(mBitmaps.size() > BITMAP_CACHE_LIMIT) {
                    mBitmaps.clear();
                }
                mBitmaps.put(bitmapKey, bitmap);
            }
            Canvas canvas = new Canvas(bitmap);
            Paint whitePaint = new Paint();
            whitePaint.setColor(Color.WHITE);
            canvas.drawColor(Color.argb(255, 127, 127, 127));
            canvas.drawRect(rect, whitePaint);
            Matrix matrix = imageView.getImageMatrix();
            matrix.reset();
            matrix.postScale((float) pageWidth / page.getWidth(),
                    (float) pageHeight / page.getHeight());
            matrix.postTranslate(left, top);
            page.render(bitmap, rect, matrix, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
            imageView.setImageMatrix(matrix);
            imageView.setImageBitmap(bitmap);
            imageView.invalidate();
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * hasAsset
     * @param abstractURL
     * @return
     */
    public boolean hasAsset(final String abstractURL) {
        return mAbstractPaths.containsKey(abstractURL);
    }

    private void loadAbstractInfo(Context context) {
        mAbstractPaths.clear();
        File externalDir = new File(context.getFilesDir(), EXTERNAL_ABSTRACT_DIR);
        try {
            JSONArray array = new JSONArray(ThisApplication.getConfigValue(CONFIG_NAME, ""));
            int arrayLength = array.length();
            int i = 0;
            while(i < arrayLength) {
                JSONObject obj = array.getJSONObject(i);
                String abstractURL = obj.getString("abstractURL");
                File externalFile = new File(externalDir, obj.getString("filename"));
                mAbstractPaths.put(abstractURL, externalFile);
                i++;
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void saveAbstractInfo() {
        JSONArray array = new JSONArray();
        for(Map.Entry<String, File> entry: mAbstractPaths.entrySet()) {
            JSONObject obj = new JSONObject();
            try {
                File externalFile = entry.getValue();
                obj.put("abstractURL", entry.getKey());
                obj.put("filename", externalFile.getName());
                array.put(obj);
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }
        ThisApplication.saveConfigValue(CONFIG_NAME, array.toString());
        mAbstractPaths.clear();
    }
}
