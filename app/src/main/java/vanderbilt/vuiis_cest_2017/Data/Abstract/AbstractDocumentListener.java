package vanderbilt.vuiis_cest_2017.Data.Abstract;

import java.io.File;

/**
 * Created by Matthew Christian on 4/7/17.
 */

public interface AbstractDocumentListener {
    void onAbstractDocumentReady(String abstractURL, File abstractFile);
    void onAbstractDocumentNotFound(String abstractURL);
}
