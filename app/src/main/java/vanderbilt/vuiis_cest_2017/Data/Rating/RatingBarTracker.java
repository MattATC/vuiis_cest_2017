package vanderbilt.vuiis_cest_2017.Data.Rating;

import android.widget.RatingBar;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import vanderbilt.vuiis_cest_2017.Data.Agenda.PresentationData;
import vanderbilt.vuiis_cest_2017.Data.DatabaseHelper;

/**
 * Created by Matthew Christian on 3/29/17.
 */

public class RatingBarTracker implements RatingBarTrackerDelegate {
    static private RatingBarTracker mRatingBarTracker;

    private Map<Integer, Map<Integer, RatingBar>> mRatingBarMap = new HashMap<>();
    private Map<Integer, RatingBar.OnRatingBarChangeListener> mRatingBarListenerMap = new HashMap<>();

    static public void setInstance(RatingBarTracker ratingBarTracker) {
        mRatingBarTracker = ratingBarTracker;
    }

    static public RatingBarTracker getInstance() {
        return mRatingBarTracker;
    }

    public RatingBarTracker() {
    }

    @Override
    public void registerRatingBar(PresentationData presentation, Integer viewTag, RatingBar ratingBar,
                                  final DatabaseHelper dataHelper) {
        final int presentationID = presentation.getID();
        Map<Integer, RatingBar> ratingBarSet = mRatingBarMap.get(presentationID);
        RatingBar.OnRatingBarChangeListener changeListener = mRatingBarListenerMap.get(presentationID);
        if(ratingBarSet == null) {
            ratingBarSet = new HashMap<>();
            changeListener = new RatingBar.OnRatingBarChangeListener() {
                @Override
                public void onRatingChanged(RatingBar ratingBar, float v, boolean fromUser) {
                    if(!fromUser) {
                        return;
                    }
                    final int stars = (int)v;
                    final Date now = new Date();
                    PresentationRating rating = new PresentationRating(presentationID, now, stars);
                    PresentationRatingManager.getInstance().submitRating(dataHelper, rating);
                    //Update all rating bars
                    Map<Integer, RatingBar> myRatingBarSet = mRatingBarMap.get(presentationID);
                    for(RatingBar rb: myRatingBarSet.values()) {
                        //rb.setOnRatingBarChangeListener(null);
                        rb.setRating(stars);
                        //rb.setOnRatingBarChangeListener(changeListener);
                    }
                }
            };
            mRatingBarMap.put(presentationID, ratingBarSet);
            mRatingBarListenerMap.put(presentationID, changeListener);
        }
        ratingBar.setOnRatingBarChangeListener(changeListener);
        ratingBarSet.put(viewTag, ratingBar);
    }

    public void clearRegistrations() {
        for(Map<Integer, RatingBar> ratingBarSet: mRatingBarMap.values()) {
            for(RatingBar rb: ratingBarSet.values()) {
                rb.setOnRatingBarChangeListener(null);
            }
        }
        mRatingBarMap.clear();
        mRatingBarListenerMap.clear();
    }
}
