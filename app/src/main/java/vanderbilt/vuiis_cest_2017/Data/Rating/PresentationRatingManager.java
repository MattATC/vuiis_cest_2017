package vanderbilt.vuiis_cest_2017.Data.Rating;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Handler;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import vanderbilt.vuiis_cest_2017.Data.DatabaseHelper;
import vanderbilt.vuiis_cest_2017.Data.DatabaseMetaData;
import vanderbilt.vuiis_cest_2017.DataManager.DataUpdateInfo;
import vanderbilt.vuiis_cest_2017.ThisApplication;

/**
 * Created by Matthew Christian on 3/28/17.
 */

public class PresentationRatingManager implements PresentationRatingSubmitListener {
    static final private String CONFIG_NAME = "rating_queue";
    static private PresentationRatingManager mPresentationRatingManager;
    private static Map<Integer, PresentationRating> mRatings = new HashMap<>();

    private Collection<PresentationRating> mQueue = new ArrayList<>();
    private Handler mRatingSubmitHandler = new Handler();
    private int mSubmissionInterval = 60000;  // Retry submission every 1 minute
    private Runnable mRatingSubmitRunnable = new Runnable() {
        @Override
        public void run() {
            startSubmission();
        }
    };

    static public void setInstance(PresentationRatingManager presentationRatingManager) {
        mPresentationRatingManager = presentationRatingManager;
    }

    static public PresentationRatingManager getInstance() {
        return mPresentationRatingManager;
    }

    public PresentationRatingManager() {
        loadRatingsQueue();
    }

    public synchronized void submitRating(DatabaseHelper dataHelper, PresentationRating rating) {
        saveRating(dataHelper, rating); //Save the rating to the database
        if(mQueue.isEmpty()) { // If no task is currently running, execute one immediately
            Collection<PresentationRating> queue = new ArrayList<>();
            queue.add(rating);
            new PresentationRatingSubmitTask(queue, this).execute(0);
        }
        else {  // Else add it to the queue for the next startSubmission call
            mQueue.add(rating);
        }
    }

    private synchronized void startSubmission() {
        Collection<PresentationRating> queue = new ArrayList<>(mQueue);
        if(!queue.isEmpty()) {
            new PresentationRatingSubmitTask(queue, this).execute(0);
        }
    }

    @Override
    public synchronized void onRatingSubmitted(boolean result,
                                               Collection<PresentationRating> items) {
        if(result) { // If submit was successful, remove those items from the queue, if present
            mQueue.removeAll(items);
        }
        else { // If submit was not successful, try again
            Log.d("onRatingSubmitted", "Submission was not successful, trying again " +
                    items.toString());
            mQueue.addAll(items);
        }
        if(!mQueue.isEmpty()) {
            mRatingSubmitHandler.postDelayed(mRatingSubmitRunnable, mSubmissionInterval);
        }
    }

    @Override
    public synchronized void onRatingSubmissionCancelled(Collection<PresentationRating> items) {
    }

    public synchronized void saveRatingsQueue() {
        JSONObject inputObject = PresentationRating.saveRatingsToJSON(mQueue);
        mQueue.clear();
        try {
            Log.d("saveRatingsQueue", "Saving " + inputObject.toString(3));
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        ThisApplication.saveConfigValue(CONFIG_NAME, inputObject.toString());
    }

    private synchronized void loadRatingsQueue() {
        try {
            JSONArray array = new JSONArray(ThisApplication.getConfigValue(CONFIG_NAME, ""));
            mQueue.addAll(PresentationRating.loadRatingsFromJSON(array));
            try {
                Log.d("loadRatingsQueue", "Loaded " + array.toString(3));
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        //Write an empty JSON array
        ThisApplication.saveConfigValue(CONFIG_NAME, new JSONArray().toString());

        if(!mQueue.isEmpty()) {
            mRatingSubmitHandler.post(mRatingSubmitRunnable);
        }
    }

    static public PresentationRating getRating(int presentationID) {
        return mRatings.get(presentationID);
    }

    static public void updateRatings(DatabaseHelper dataHelper, DataUpdateInfo dataUpdateInfo) {
        PresentationRatingManager ratingManager = getInstance();
        Set<String> tablesAffected = dataUpdateInfo.getTablesAffected();
        if(tablesAffected.contains(DatabaseMetaData.TABLE_PRESENTATION)) {
            SQLiteDatabase db = dataHelper.getReadableDatabase();
            String query;
            Cursor cursor;
            mRatings.clear();
            query = "select id, timeModified, stars from PresentationRating order by id";
            cursor = db.rawQuery(query, null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                final int presentationID = cursor.getInt(0);
                final String timeModified = cursor.getString(1);
                final int stars = cursor.getInt(2);
                try {
                    PresentationRating rating = new PresentationRating(presentationID,
                            DatabaseHelper.convertTimestampToDate(timeModified), stars);
                    mRatings.put(presentationID, rating);
                }
                catch (ParseException e) {
                    e.printStackTrace();
                }
                cursor.moveToNext();
            }
            cursor.close();
            db.close();
        }
    }

    /**
     * Saves the rating to the database
     * @param rating
     */
    private void saveRating(DatabaseHelper dataHelper, PresentationRating rating) {
        final int presentationID = rating.getID();
        Date timeModified = rating.getTimestamp();
        final int stars = rating.getStars();
        SQLiteDatabase db = dataHelper.getWritableDatabase();
        db.beginTransaction();
        try {
            ArrayList<String> values = new ArrayList<>();
            values.add(Integer.toString(presentationID));
            values.add(DatabaseHelper.convertDateToTimestamp(timeModified));
            values.add(Integer.toString(stars));
            String stmt = "insert or replace into PresentationRating values (?, ?, ?)";
            db.execSQL(stmt, values.toArray(new String[values.size()]));
            db.setTransactionSuccessful();
            mRatings.remove(presentationID);
            mRatings.put(presentationID, rating);
        }
        catch(SQLiteException e) {
            Log.e("saveRating", "Exception occurred while saving Presentation rating " + e);
        }
        finally {
            db.endTransaction();
        }
        db.close();
    }
}
