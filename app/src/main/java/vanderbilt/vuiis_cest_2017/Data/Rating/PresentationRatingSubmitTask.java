package vanderbilt.vuiis_cest_2017.Data.Rating;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import vanderbilt.vuiis_cest_2017.ThisApplication;
import vanderbilt.vuiis_cest_2017.Utilities.NetworkTaskCommon;

/**
 * Created by Matthew Christian on 3/28/17.
 */

public class PresentationRatingSubmitTask extends AsyncTask<Integer, Void, Boolean> {
    private static final String RATING_SUBMIT_SCRIPT_PATH = "rating";

    private Collection<PresentationRating> mRatings;
    private List<PresentationRating> mSuccededRatings = new ArrayList<>();
    private List<PresentationRating> mFailedRatings = new ArrayList<>();
    private PresentationRatingSubmitListener mTaskDelegate;

    public PresentationRatingSubmitTask(Collection<PresentationRating> ratings,
                                        PresentationRatingSubmitListener taskDelegate) {
        mRatings = ratings;
        mTaskDelegate = taskDelegate;
    }

    @Override
    protected Boolean doInBackground(Integer... params) {
        boolean success = false;
        JSONObject inputObject = PresentationRating.saveRatingsToJSON(mRatings);
        String body = inputObject.toString();
        try {
            Map<String, String> header = new HashMap<>();
            header.put("Content-Type", "application/json");
            header.put("x-api-key", ThisApplication.getServerAPIKey());
            String response = new String(NetworkTaskCommon.postServerHTTPData(
                    RATING_SUBMIT_SCRIPT_PATH, "", body, header), "UTF-8");
            JSONArray outputArray = new JSONArray(response);
            List<PresentationRating> ratingList = new ArrayList<>(mRatings);
            int outputLength = Math.min(outputArray.length(), ratingList.size());
            for(int i = 0; i < outputLength; i++) {
                JSONObject obj = outputArray.getJSONObject(i);
                if(obj.getBoolean("success")) {
                    mSuccededRatings.add(ratingList.get(i));
                }
                else {
                    mFailedRatings.add(ratingList.get(i));
                }
            }
            success = mSuccededRatings.size() == mRatings.size() && mFailedRatings.isEmpty();
        }
        catch (JSONException e) {
            e.printStackTrace();
            mFailedRatings.addAll(mRatings);
        }
        catch (IOException e) {
            e.printStackTrace();
            mFailedRatings.addAll(mRatings);
        }
        return success;
    }

    /**
     * Runs on the UI thread after doInBackground completes successfully
     * @param result
     */
    @Override
    protected void onPostExecute(Boolean result) {
        if(mTaskDelegate != null) {
            if(!mSuccededRatings.isEmpty()) {
                mTaskDelegate.onRatingSubmitted(true, mSuccededRatings);
            }
            if(!mFailedRatings.isEmpty()) {
                mTaskDelegate.onRatingSubmitted(false, mFailedRatings);
            }
        }
    }

    /**
     * Runs on the UI thread after doInBackground does NOT complete successfully
     */
    @Override
    protected void onCancelled() {
        if(mTaskDelegate != null) {
            mTaskDelegate.onRatingSubmissionCancelled(mRatings);
        }
    }
}
