package vanderbilt.vuiis_cest_2017.Data.Rating;

import android.widget.RatingBar;

import vanderbilt.vuiis_cest_2017.Data.Agenda.PresentationData;
import vanderbilt.vuiis_cest_2017.Data.DatabaseHelper;

/**
 * Created by Matthew Christian on 3/29/17.
 */

public interface RatingBarTrackerDelegate {
    void registerRatingBar(PresentationData presentation, Integer viewTag, RatingBar ratingBar,
                           DatabaseHelper dataHelper);
}
