package vanderbilt.vuiis_cest_2017.Data.Rating;

import java.util.Collection;

/**
 * Created by Matthew Christian on 3/28/17.
 */

public interface PresentationRatingSubmitListener {
    void onRatingSubmitted(boolean result, Collection<PresentationRating> items);
    void onRatingSubmissionCancelled(Collection<PresentationRating> items);
}
