package vanderbilt.vuiis_cest_2017.Data.Rating;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import vanderbilt.vuiis_cest_2017.Data.DatabaseHelper;
import vanderbilt.vuiis_cest_2017.ThisApplication;

/**
 * Created by Matthew Christian on 3/28/17.
 */

public class PresentationRating {
    private int mPresentationID;
    private Date mTimestamp;
    private int mStars;

    public PresentationRating(int presentationID, Date timestamp, int stars) {
        mPresentationID = presentationID;
        mTimestamp = timestamp;
        mStars = stars;
    }

    public PresentationRating(JSONObject obj) {
        try {
            mPresentationID = Integer.valueOf(obj.getString("presentationID"));
            mTimestamp = DatabaseHelper.convertTimestampToDate(obj.getString("timestamp"));
            mStars = Integer.valueOf(obj.getString("rating"));
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public int getID() { return mPresentationID; }
    public int getStars() { return mStars; }
    public Date getTimestamp() { return mTimestamp; }

    public JSONObject toJSON() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("presentationID", String.valueOf(mPresentationID));
            obj.put("time", DatabaseHelper.convertDateToTimestamp(mTimestamp));
            obj.put("rating", String.valueOf(mStars));
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        return obj;
    }

    public static List<PresentationRating> loadRatingsFromJSON(JSONArray array) {
        final int arrayLength = array.length();
        List<PresentationRating> ratings = new ArrayList<>();
        int i = 0;
        try {
            while (i < arrayLength) {
                ratings.add(new PresentationRating(array.getJSONObject(i)));
                i++;
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        return ratings;
    }

    public static JSONObject saveRatingsToJSON(Collection<PresentationRating> ratings) {
        JSONObject root = new JSONObject();
        JSONArray array = new JSONArray();
        try {
            root.put("uid", ThisApplication.getInstallationID());
            for(PresentationRating rating: ratings) {
                array.put(rating.toJSON());
            }
            root.put("ratings", array);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        return root;
    }
}
