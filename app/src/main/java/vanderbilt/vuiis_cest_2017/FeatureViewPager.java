package vanderbilt.vuiis_cest_2017;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by matthew on 3/14/17.
 */

public class FeatureViewPager extends ViewPager {
    private boolean mSwipeEnabled;

    public FeatureViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        mSwipeEnabled = true;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        // Never allow swiping to switch between pages
        if(mSwipeEnabled) {
            return super.onInterceptTouchEvent(event);
        }
        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // Never allow swiping to switch between pages
        if(mSwipeEnabled) {
            return super.onTouchEvent(event);
        }
        return false;
    }

    public void enableSwipe() {
        mSwipeEnabled = true;
    }

    public void disableSwipe() {
        mSwipeEnabled = false;
    }
}
