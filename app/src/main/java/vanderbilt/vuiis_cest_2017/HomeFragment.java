package vanderbilt.vuiis_cest_2017;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import vanderbilt.vuiis_cest_2017.ViewController.SimpleController.SimpleController;
import vanderbilt.vuiis_cest_2017.ViewController.SimpleController.SimpleControllerListener;
import vanderbilt.vuiis_cest_2017.ViewController.ViewControllerFragment;
import vanderbilt.vuiis_cest_2017.ViewController.ViewControllerHandle;

/**
 * Created by Matthew Christian on 3/3/17.
 */

public class HomeFragment extends ViewControllerFragment
    implements SimpleControllerListener {

    public static final String TAB_TITLE = "Home";

    private ViewControllerHandle mController;
    private LayoutInflater mLayoutInflater;

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static HomeFragment newInstance(int sectionNumber) {
        return new HomeFragment();
    }

    public HomeFragment() {
        mController = new SimpleController(this, null, this);
    }

    @Override
    public ViewControllerHandle getViewController() {
        return mController;
    }

    @Override
    public void doCustomAction(int action, Bundle params) {}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        MainActivity mainActivity = (MainActivity) getActivity();
        mController.setAppActivityListener(mainActivity);
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mController.onConfigurationChanged(newConfig);
    }

    @Override
    public final View onCreateView(LayoutInflater inflater, ViewGroup container,
                                   Bundle savedInstanceState) {
        mLayoutInflater = inflater;
        return mController.onCreateView(inflater, container, savedInstanceState);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        mController.onCreateOptionsMenu(menu, menuInflater);
    }

    @Override
    public View buildView(LayoutInflater inflater, ViewGroup container) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        return view;
    }
}