from __future__ import print_function
import re
import os
import sys
import glob

files = glob.glob("*.jpg")

for f in files:
    b, ext = os.path.splitext(f)
    s1 = re.sub('[0-9]', '', b)
    #s2 = re.sub('(.)([A-Za-z]+)', '\\1_\\2', s1)
    new_name = 'person_' + re.sub('([A-Z]{2,})([a-z])', '\\1_\\2', s1).lower() + '.jpg'
    os.rename(f, new_name)
    print("old name:", f, "new name:", new_name)


